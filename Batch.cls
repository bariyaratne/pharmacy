VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Batch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varBatchID As Long
    Private varBatch As String
    Private varItemID As Long
    Private varDOM As Date
    Private varDOE As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblBatch Where BatchID = " & varBatchID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !Batch = varBatch
        !ItemID = varItemID
        !DOM = varDOM
        !DOE = varDOE
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varBatchID = !NewID
        Else
            varBatchID = !BatchID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBatch WHERE BatchID = " & varBatchID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!BatchID) Then
               varBatchID = !BatchID
            End If
            If Not IsNull(!Batch) Then
               varBatch = !Batch
            End If
            If Not IsNull(!ItemID) Then
               varItemID = !ItemID
            End If
            If Not IsNull(!DOM) Then
               varDOM = !DOM
            End If
            If Not IsNull(!DOE) Then
               varDOE = !DOE
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varBatchID = 0
    varBatch = Empty
    varItemID = 0
    varDOM = Empty
    varDOE = Empty
End Sub

Public Property Let BatchID(ByVal vBatchID As Long)
    Call clearData
    varBatchID = vBatchID
    Call loadData
End Property

Public Property Get BatchID() As Long
    BatchID = varBatchID
End Property

Public Property Let Batch(ByVal vBatch As String)
    varBatch = vBatch
End Property

Public Property Get Batch() As String
    Batch = varBatch
End Property

Public Property Let ItemID(ByVal vItemID As Long)
    varItemID = vItemID
End Property

Public Property Get ItemID() As Long
    ItemID = varItemID
End Property

Public Property Let DOM(ByVal vDOM As Date)
    varDOM = vDOM
End Property

Public Property Get DOM() As Date
    DOM = varDOM
End Property

Public Property Let DOE(ByVal vDOE As Date)
    varDOE = vDOE
End Property

Public Property Get DOE() As Date
    DOE = varDOE
End Property


