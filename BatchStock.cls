VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BatchStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varBatchStockID As Long
    Private varBatchID As Long
    Private varStoreID As Long
    Private varStock As Double
    Private varpprice As Double
    Private varsprice As Double
    Private varwprice As Double
    Private varLocationID As Long
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblBatchStock Where BatchStockID = " & varBatchStockID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !BatchID = varBatchID
        !StoreID = varStoreID
        !Stock = varStock
        !pprice = varpprice
        !sprice = varsprice
        !wprice = varwprice
        !LocationID = varLocationID
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varBatchStockID = !NewID
        Else
            varBatchStockID = !BatchStockID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBatchStock WHERE BatchStockID = " & varBatchStockID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!BatchStockID) Then
               varBatchStockID = !BatchStockID
            End If
            If Not IsNull(!BatchID) Then
               varBatchID = !BatchID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!Stock) Then
               varStock = !Stock
            End If
            If Not IsNull(!pprice) Then
               varpprice = !pprice
            End If
            If Not IsNull(!sprice) Then
               varsprice = !sprice
            End If
            If Not IsNull(!wprice) Then
               varwprice = !wprice
            End If
            If Not IsNull(!LocationID) Then
               varLocationID = !LocationID
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varBatchStockID = 0
    varBatchID = 0
    varStoreID = 0
    varStock = 0
    varpprice = 0
    varsprice = 0
    varwprice = 0
    varLocationID = 0
    varupsize_ts = Empty
End Sub

Public Property Let BatchStockID(ByVal vBatchStockID As Long)
    Call clearData
    varBatchStockID = vBatchStockID
    Call loadData
End Property

Public Property Get BatchStockID() As Long
    BatchStockID = varBatchStockID
End Property

Public Property Let BatchID(ByVal vBatchID As Long)
    varBatchID = vBatchID
End Property

Public Property Get BatchID() As Long
    BatchID = varBatchID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let Stock(ByVal vStock As Double)
    varStock = vStock
End Property

Public Property Get Stock() As Double
    Stock = varStock
End Property

Public Property Let pprice(ByVal vpprice As Double)
    varpprice = vpprice
End Property

Public Property Get pprice() As Double
    pprice = varpprice
End Property

Public Property Let sprice(ByVal vsprice As Double)
    varsprice = vsprice
End Property

Public Property Get sprice() As Double
    sprice = varsprice
End Property

Public Property Let wprice(ByVal vwprice As Double)
    varwprice = vwprice
End Property

Public Property Get wprice() As Double
    wprice = varwprice
End Property

Public Property Let LocationID(ByVal vLocationID As Long)
    varLocationID = vLocationID
End Property

Public Property Get LocationID() As Long
    LocationID = varLocationID
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


