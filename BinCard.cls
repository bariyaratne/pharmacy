VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "BinCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varid As Long
    Private varstoreId As Long
    Private varitemId As Long
    Private varbillType As Long
    Private varbillItemId As Long
    Private varbillId As Long
    Private varinQty As Double
    Private varoutQty As Double
    Private varbeforeStock As Double
    Private varafterStock As Double
    Private varaddedDate As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblBinCard Where id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !storeId = varstoreId
        !itemId = varitemId
        !billType = varbillType
        !billItemId = varbillItemId
        !billId = varbillId
        !inQty = varinQty
        !outQty = varoutQty
        !beforeStock = varbeforeStock
        !afterStock = varafterStock
        !addedDate = varaddedDate
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varid = !NewID
        Else
            varid = !id
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblBinCard WHERE id = " & varid
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!id) Then
               varid = !id
            End If
            If Not IsNull(!storeId) Then
               varstoreId = !storeId
            End If
            If Not IsNull(!itemId) Then
               varitemId = !itemId
            End If
            If Not IsNull(!billType) Then
               varbillType = !billType
            End If
            If Not IsNull(!billItemId) Then
               varbillItemId = !billItemId
            End If
            If Not IsNull(!billId) Then
               varbillId = !billId
            End If
            If Not IsNull(!inQty) Then
               varinQty = !inQty
            End If
            If Not IsNull(!outQty) Then
               varoutQty = !outQty
            End If
            If Not IsNull(!beforeStock) Then
               varbeforeStock = !beforeStock
            End If
            If Not IsNull(!afterStock) Then
               varafterStock = !afterStock
            End If
            If Not IsNull(!addedDate) Then
               varaddedDate = !addedDate
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varid = 0
    varstoreId = 0
    varitemId = 0
    varbillType = 0
    varbillItemId = 0
    varbillId = 0
    varinQty = 0
    varoutQty = 0
    varbeforeStock = 0
    varafterStock = 0
    varaddedDate = Empty
End Sub

Public Property Let id(ByVal vid As Long)
    Call clearData
    varid = vid
    Call loadData
End Property

Public Property Get id() As Long
    id = varid
End Property

Public Property Let storeId(ByVal vstoreId As Long)
    varstoreId = vstoreId
End Property

Public Property Get storeId() As Long
    storeId = varstoreId
End Property

Public Property Let itemId(ByVal vitemId As Long)
    varitemId = vitemId
End Property

Public Property Get itemId() As Long
    itemId = varitemId
End Property

Public Property Let billType(ByVal vbillType As Long)
    varbillType = vbillType
End Property

Public Property Get billType() As Long
    billType = varbillType
End Property

Public Property Let billItemId(ByVal vbillItemId As Long)
    varbillItemId = vbillItemId
End Property

Public Property Get billItemId() As Long
    billItemId = varbillItemId
End Property

Public Property Let billId(ByVal vbillId As Long)
    varbillId = vbillId
End Property

Public Property Get billId() As Long
    billId = varbillId
End Property

Public Property Let inQty(ByVal vinQty As Double)
    varinQty = vinQty
End Property

Public Property Get inQty() As Double
    inQty = varinQty
End Property

Public Property Let outQty(ByVal voutQty As Double)
    varoutQty = voutQty
End Property

Public Property Get outQty() As Double
    outQty = varoutQty
End Property

Public Property Let beforeStock(ByVal vbeforeStock As Double)
    varbeforeStock = vbeforeStock
End Property

Public Property Get beforeStock() As Double
    beforeStock = varbeforeStock
End Property

Public Property Let afterStock(ByVal vafterStock As Double)
    varafterStock = vafterStock
End Property

Public Property Get afterStock() As Double
    afterStock = varafterStock
End Property

Public Property Let addedDate(ByVal vaddedDate As Date)
    varaddedDate = vaddedDate
End Property

Public Property Get addedDate() As Date
    addedDate = varaddedDate
End Property


