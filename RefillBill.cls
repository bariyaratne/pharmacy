VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RefillBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varRefillBillID As Long
    Private varDistributorID As Long
    Private vardeptInvoiceNo As String
    Private varInvoiceNo As String
    Private varInvoiceDate As Date
    Private varDistributorOrderBillID As Long
    Private varStoreID As Long
    Private varStaffID As Long
    Private varSaleId As Long
    Private varCheckedStaffID As Long
    Private varPrice As Double
    Private varDiscount As Double
    Private varTax As Double
    Private varDiscountPercent As Double
    Private varNetPrice As Double
    Private varDate As Date
    Private varTime As Date
    Private varPaymentMethodID As Long
    Private varPaymentMethod As String
    Private varPaidStaffID As Long
    Private varPaidDate As Date
    Private varPaidTime As Date
    Private varPaidPrice As Double
    Private varFullyPaid As Boolean
    Private varIssuedCashID As Long
    Private varIssuedChequeID As Long
    Private varIssuedCreditCardID As Long
    Private varIssuedSlipID As Long
    Private varIssuedCreditID As Long
    Private varOrderBillID As Long
    Private varPurchase As Boolean
    Private varAutoRequest As Boolean
    Private varManualRequest As Boolean
    Private varReceivedIssue As Boolean
    Private varCancelled As Boolean
    Private varCancelledUserID As Long
    Private varCancelledDate As Date
    Private varCancelledTime As Date
    Private varCancelledCUserID As Long
    Private varRepayPaymentMethodID As Long
    Private varReceivedCashID As Long
    Private varReceivedCreditID As Long
    Private varReceivedChequeID As Long
    Private varReturned As Boolean
    Private varReturnedUserID As Long
    Private varReturnedCUserID As Long
    Private varReturnedDate As Date
    Private varReturnedTime As Date
    Private varCancelledValue As Double
    Private varReturnedValue As Double
    Private varSettledValue As Double
    Private varupsize_ts
    Private varbillValueUoToYesterday As Double
    Private varbillValueUoToToday As Double
    Private varbillValueToday As Double
    Private varbillValueUoToYesterdayS As Double
    Private varbillValueUoToTodayS As Double
    Private varbillValueTodayS As Double
    Private varNetSaleValue As Double

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblRefillBill Where RefillBillID = " & varRefillBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !DistributorID = varDistributorID
        !deptInvoiceNo = vardeptInvoiceNo
        !InvoiceNo = varInvoiceNo
        !InvoiceDate = varInvoiceDate
        !DistributorOrderBillID = varDistributorOrderBillID
        !StoreID = varStoreID
        !StaffID = varStaffID
        !SaleId = varSaleId
        !CheckedStaffID = varCheckedStaffID
        !Price = varPrice
        !Discount = varDiscount
        !Tax = varTax
        !DiscountPercent = varDiscountPercent
        !NetPrice = varNetPrice
        !Date = varDate
        !Time = varTime
        !PaymentMethodID = varPaymentMethodID
        !PaymentMethod = varPaymentMethod
        !PaidStaffID = varPaidStaffID
        !PaidDate = varPaidDate
        !PaidTime = varPaidTime
        !PaidPrice = varPaidPrice
        !FullyPaid = varFullyPaid
        !IssuedCashID = varIssuedCashID
        !IssuedChequeID = varIssuedChequeID
        !IssuedCreditCardID = varIssuedCreditCardID
        !IssuedSlipID = varIssuedSlipID
        !IssuedCreditID = varIssuedCreditID
        !OrderBillID = varOrderBillID
        !Purchase = varPurchase
        !AutoRequest = varAutoRequest
        !ManualRequest = varManualRequest
        !ReceivedIssue = varReceivedIssue
        !Cancelled = varCancelled
        !CancelledUserID = varCancelledUserID
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledCUserID = varCancelledCUserID
        !RepayPaymentMethodID = varRepayPaymentMethodID
        !ReceivedCashID = varReceivedCashID
        !ReceivedCreditID = varReceivedCreditID
        !ReceivedChequeID = varReceivedChequeID
        !Returned = varReturned
        !ReturnedUserID = varReturnedUserID
        !ReturnedCUserID = varReturnedCUserID
        !ReturnedDate = varReturnedDate
        !ReturnedTime = varReturnedTime
        !CancelledValue = varCancelledValue
        !ReturnedValue = varReturnedValue
        !SettledValue = varSettledValue
        !billValueUoToYesterday = varbillValueUoToYesterday
        !billValueUoToToday = varbillValueUoToToday
        !billValueToday = varbillValueToday
        !billValueUoToYesterdayS = varbillValueUoToYesterdayS
        !billValueUoToTodayS = varbillValueUoToTodayS
        !billValueTodayS = varbillValueTodayS
        !NetSaleValue = varNetSaleValue
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varRefillBillID = !NewID
        Else
            varRefillBillID = !RefillBillID
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblRefillBill WHERE RefillBillID = " & varRefillBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!RefillBillID) Then
               varRefillBillID = !RefillBillID
            End If
            If Not IsNull(!DistributorID) Then
               varDistributorID = !DistributorID
            End If
            If Not IsNull(!deptInvoiceNo) Then
               vardeptInvoiceNo = !deptInvoiceNo
            End If
            If Not IsNull(!InvoiceNo) Then
               varInvoiceNo = !InvoiceNo
            End If
            If Not IsNull(!InvoiceDate) Then
               varInvoiceDate = !InvoiceDate
            End If
            If Not IsNull(!DistributorOrderBillID) Then
               varDistributorOrderBillID = !DistributorOrderBillID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!SaleId) Then
               varSaleId = !SaleId
            End If
            If Not IsNull(!CheckedStaffID) Then
               varCheckedStaffID = !CheckedStaffID
            End If
            If Not IsNull(!Price) Then
               varPrice = !Price
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!Tax) Then
               varTax = !Tax
            End If
            If Not IsNull(!DiscountPercent) Then
               varDiscountPercent = !DiscountPercent
            End If
            If Not IsNull(!NetPrice) Then
               varNetPrice = !NetPrice
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!PaymentMethod) Then
               varPaymentMethod = !PaymentMethod
            End If
            If Not IsNull(!PaidStaffID) Then
               varPaidStaffID = !PaidStaffID
            End If
            If Not IsNull(!PaidDate) Then
               varPaidDate = !PaidDate
            End If
            If Not IsNull(!PaidTime) Then
               varPaidTime = !PaidTime
            End If
            If Not IsNull(!PaidPrice) Then
               varPaidPrice = !PaidPrice
            End If
            If Not IsNull(!FullyPaid) Then
               varFullyPaid = !FullyPaid
            End If
            If Not IsNull(!IssuedCashID) Then
               varIssuedCashID = !IssuedCashID
            End If
            If Not IsNull(!IssuedChequeID) Then
               varIssuedChequeID = !IssuedChequeID
            End If
            If Not IsNull(!IssuedCreditCardID) Then
               varIssuedCreditCardID = !IssuedCreditCardID
            End If
            If Not IsNull(!IssuedSlipID) Then
               varIssuedSlipID = !IssuedSlipID
            End If
            If Not IsNull(!IssuedCreditID) Then
               varIssuedCreditID = !IssuedCreditID
            End If
            If Not IsNull(!OrderBillID) Then
               varOrderBillID = !OrderBillID
            End If
            If Not IsNull(!Purchase) Then
               varPurchase = !Purchase
            End If
            If Not IsNull(!AutoRequest) Then
               varAutoRequest = !AutoRequest
            End If
            If Not IsNull(!ManualRequest) Then
               varManualRequest = !ManualRequest
            End If
            If Not IsNull(!ReceivedIssue) Then
               varReceivedIssue = !ReceivedIssue
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledCUserID) Then
               varCancelledCUserID = !CancelledCUserID
            End If
            If Not IsNull(!RepayPaymentMethodID) Then
               varRepayPaymentMethodID = !RepayPaymentMethodID
            End If
            If Not IsNull(!ReceivedCashID) Then
               varReceivedCashID = !ReceivedCashID
            End If
            If Not IsNull(!ReceivedCreditID) Then
               varReceivedCreditID = !ReceivedCreditID
            End If
            If Not IsNull(!ReceivedChequeID) Then
               varReceivedChequeID = !ReceivedChequeID
            End If
            If Not IsNull(!Returned) Then
               varReturned = !Returned
            End If
            If Not IsNull(!ReturnedUserID) Then
               varReturnedUserID = !ReturnedUserID
            End If
            If Not IsNull(!ReturnedCUserID) Then
               varReturnedCUserID = !ReturnedCUserID
            End If
            If Not IsNull(!ReturnedDate) Then
               varReturnedDate = !ReturnedDate
            End If
            If Not IsNull(!ReturnedTime) Then
               varReturnedTime = !ReturnedTime
            End If
            If Not IsNull(!CancelledValue) Then
               varCancelledValue = !CancelledValue
            End If
            If Not IsNull(!ReturnedValue) Then
               varReturnedValue = !ReturnedValue
            End If
            If Not IsNull(!SettledValue) Then
               varSettledValue = !SettledValue
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
            If Not IsNull(!billValueUoToYesterday) Then
               varbillValueUoToYesterday = !billValueUoToYesterday
            End If
            If Not IsNull(!billValueUoToToday) Then
               varbillValueUoToToday = !billValueUoToToday
            End If
            If Not IsNull(!billValueToday) Then
               varbillValueToday = !billValueToday
            End If
            If Not IsNull(!billValueUoToYesterdayS) Then
               varbillValueUoToYesterdayS = !billValueUoToYesterdayS
            End If
            If Not IsNull(!billValueUoToTodayS) Then
               varbillValueUoToTodayS = !billValueUoToTodayS
            End If
            If Not IsNull(!billValueTodayS) Then
               varbillValueTodayS = !billValueTodayS
            End If
            If Not IsNull(!NetSaleValue) Then
               varNetSaleValue = !NetSaleValue
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varRefillBillID = 0
    varDistributorID = 0
    vardeptInvoiceNo = Empty
    varInvoiceNo = Empty
    varInvoiceDate = Empty
    varDistributorOrderBillID = 0
    varStoreID = 0
    varStaffID = 0
    varSaleId = 0
    varCheckedStaffID = 0
    varPrice = 0
    varDiscount = 0
    varTax = 0
    varDiscountPercent = 0
    varNetPrice = 0
    varDate = Empty
    varTime = Empty
    varPaymentMethodID = 0
    varPaymentMethod = Empty
    varPaidStaffID = 0
    varPaidDate = Empty
    varPaidTime = Empty
    varPaidPrice = 0
    varFullyPaid = False
    varIssuedCashID = 0
    varIssuedChequeID = 0
    varIssuedCreditCardID = 0
    varIssuedSlipID = 0
    varIssuedCreditID = 0
    varOrderBillID = 0
    varPurchase = False
    varAutoRequest = False
    varManualRequest = False
    varReceivedIssue = False
    varCancelled = False
    varCancelledUserID = 0
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledCUserID = 0
    varRepayPaymentMethodID = 0
    varReceivedCashID = 0
    varReceivedCreditID = 0
    varReceivedChequeID = 0
    varReturned = False
    varReturnedUserID = 0
    varReturnedCUserID = 0
    varReturnedDate = Empty
    varReturnedTime = Empty
    varCancelledValue = 0
    varReturnedValue = 0
    varSettledValue = 0
    varupsize_ts = Empty
    varbillValueUoToYesterday = 0
    varbillValueUoToToday = 0
    varbillValueToday = 0
    varbillValueUoToYesterdayS = 0
    varbillValueUoToTodayS = 0
    varbillValueTodayS = 0
    varNetSaleValue = 0
End Sub

Public Property Let RefillBillID(ByVal vRefillBillID As Long)
    Call clearData
    varRefillBillID = vRefillBillID
    Call loadData
End Property

Public Property Get RefillBillID() As Long
    RefillBillID = varRefillBillID
End Property

Public Property Let DistributorID(ByVal vDistributorID As Long)
    varDistributorID = vDistributorID
End Property

Public Property Get DistributorID() As Long
    DistributorID = varDistributorID
End Property

Public Property Let deptInvoiceNo(ByVal vdeptInvoiceNo As String)
    vardeptInvoiceNo = vdeptInvoiceNo
End Property

Public Property Get deptInvoiceNo() As String
    deptInvoiceNo = vardeptInvoiceNo
End Property

Public Property Let InvoiceNo(ByVal vInvoiceNo As String)
    varInvoiceNo = vInvoiceNo
End Property

Public Property Get InvoiceNo() As String
    InvoiceNo = varInvoiceNo
End Property

Public Property Let InvoiceDate(ByVal vInvoiceDate As Date)
    varInvoiceDate = vInvoiceDate
End Property

Public Property Get InvoiceDate() As Date
    InvoiceDate = varInvoiceDate
End Property

Public Property Let DistributorOrderBillID(ByVal vDistributorOrderBillID As Long)
    varDistributorOrderBillID = vDistributorOrderBillID
End Property

Public Property Get DistributorOrderBillID() As Long
    DistributorOrderBillID = varDistributorOrderBillID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let SaleId(ByVal vSaleId As Long)
    varSaleId = vSaleId
End Property

Public Property Get SaleId() As Long
    SaleId = varSaleId
End Property

Public Property Let CheckedStaffID(ByVal vCheckedStaffID As Long)
    varCheckedStaffID = vCheckedStaffID
End Property

Public Property Get CheckedStaffID() As Long
    CheckedStaffID = varCheckedStaffID
End Property

Public Property Let Price(ByVal vPrice As Double)
    varPrice = vPrice
End Property

Public Property Get Price() As Double
    Price = varPrice
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let Tax(ByVal vTax As Double)
    varTax = vTax
End Property

Public Property Get Tax() As Double
    Tax = varTax
End Property

Public Property Let DiscountPercent(ByVal vDiscountPercent As Double)
    varDiscountPercent = vDiscountPercent
End Property

Public Property Get DiscountPercent() As Double
    DiscountPercent = varDiscountPercent
End Property

Public Property Let NetPrice(ByVal vNetPrice As Double)
    varNetPrice = vNetPrice
End Property

Public Property Get NetPrice() As Double
    NetPrice = varNetPrice
End Property

Public Property Let BillDate(ByVal vDate As Date)
    varDate = vDate
End Property

Public Property Get BillDate() As Date
    BillDate = varDate
End Property

Public Property Let Time(ByVal vTime As Date)
    varTime = vTime
End Property

Public Property Get Time() As Date
    Time = varTime
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let PaymentMethod(ByVal vPaymentMethod As String)
    varPaymentMethod = vPaymentMethod
End Property

Public Property Get PaymentMethod() As String
    PaymentMethod = varPaymentMethod
End Property

Public Property Let PaidStaffID(ByVal vPaidStaffID As Long)
    varPaidStaffID = vPaidStaffID
End Property

Public Property Get PaidStaffID() As Long
    PaidStaffID = varPaidStaffID
End Property

Public Property Let PaidDate(ByVal vPaidDate As Date)
    varPaidDate = vPaidDate
End Property

Public Property Get PaidDate() As Date
    PaidDate = varPaidDate
End Property

Public Property Let PaidTime(ByVal vPaidTime As Date)
    varPaidTime = vPaidTime
End Property

Public Property Get PaidTime() As Date
    PaidTime = varPaidTime
End Property

Public Property Let PaidPrice(ByVal vPaidPrice As Double)
    varPaidPrice = vPaidPrice
End Property

Public Property Get PaidPrice() As Double
    PaidPrice = varPaidPrice
End Property

Public Property Let FullyPaid(ByVal vFullyPaid As Boolean)
    varFullyPaid = vFullyPaid
End Property

Public Property Get FullyPaid() As Boolean
    FullyPaid = varFullyPaid
End Property

Public Property Let IssuedCashID(ByVal vIssuedCashID As Long)
    varIssuedCashID = vIssuedCashID
End Property

Public Property Get IssuedCashID() As Long
    IssuedCashID = varIssuedCashID
End Property

Public Property Let IssuedChequeID(ByVal vIssuedChequeID As Long)
    varIssuedChequeID = vIssuedChequeID
End Property

Public Property Get IssuedChequeID() As Long
    IssuedChequeID = varIssuedChequeID
End Property

Public Property Let IssuedCreditCardID(ByVal vIssuedCreditCardID As Long)
    varIssuedCreditCardID = vIssuedCreditCardID
End Property

Public Property Get IssuedCreditCardID() As Long
    IssuedCreditCardID = varIssuedCreditCardID
End Property

Public Property Let IssuedSlipID(ByVal vIssuedSlipID As Long)
    varIssuedSlipID = vIssuedSlipID
End Property

Public Property Get IssuedSlipID() As Long
    IssuedSlipID = varIssuedSlipID
End Property

Public Property Let IssuedCreditID(ByVal vIssuedCreditID As Long)
    varIssuedCreditID = vIssuedCreditID
End Property

Public Property Get IssuedCreditID() As Long
    IssuedCreditID = varIssuedCreditID
End Property

Public Property Let OrderBillID(ByVal vOrderBillID As Long)
    varOrderBillID = vOrderBillID
End Property

Public Property Get OrderBillID() As Long
    OrderBillID = varOrderBillID
End Property

Public Property Let Purchase(ByVal vPurchase As Boolean)
    varPurchase = vPurchase
End Property

Public Property Get Purchase() As Boolean
    Purchase = varPurchase
End Property

Public Property Let AutoRequest(ByVal vAutoRequest As Boolean)
    varAutoRequest = vAutoRequest
End Property

Public Property Get AutoRequest() As Boolean
    AutoRequest = varAutoRequest
End Property

Public Property Let ManualRequest(ByVal vManualRequest As Boolean)
    varManualRequest = vManualRequest
End Property

Public Property Get ManualRequest() As Boolean
    ManualRequest = varManualRequest
End Property

Public Property Let ReceivedIssue(ByVal vReceivedIssue As Boolean)
    varReceivedIssue = vReceivedIssue
End Property

Public Property Get ReceivedIssue() As Boolean
    ReceivedIssue = varReceivedIssue
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Long)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Long
    CancelledUserID = varCancelledUserID
End Property

Public Property Let CancelledDate(ByVal vCancelledDate As Date)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate() As Date
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime As Date)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime() As Date
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledCUserID(ByVal vCancelledCUserID As Long)
    varCancelledCUserID = vCancelledCUserID
End Property

Public Property Get CancelledCUserID() As Long
    CancelledCUserID = varCancelledCUserID
End Property

Public Property Let RepayPaymentMethodID(ByVal vRepayPaymentMethodID As Long)
    varRepayPaymentMethodID = vRepayPaymentMethodID
End Property

Public Property Get RepayPaymentMethodID() As Long
    RepayPaymentMethodID = varRepayPaymentMethodID
End Property

Public Property Let ReceivedCashID(ByVal vReceivedCashID As Long)
    varReceivedCashID = vReceivedCashID
End Property

Public Property Get ReceivedCashID() As Long
    ReceivedCashID = varReceivedCashID
End Property

Public Property Let ReceivedCreditID(ByVal vReceivedCreditID As Long)
    varReceivedCreditID = vReceivedCreditID
End Property

Public Property Get ReceivedCreditID() As Long
    ReceivedCreditID = varReceivedCreditID
End Property

Public Property Let ReceivedChequeID(ByVal vReceivedChequeID As Long)
    varReceivedChequeID = vReceivedChequeID
End Property

Public Property Get ReceivedChequeID() As Long
    ReceivedChequeID = varReceivedChequeID
End Property

Public Property Let Returned(ByVal vReturned As Boolean)
    varReturned = vReturned
End Property

Public Property Get Returned() As Boolean
    Returned = varReturned
End Property

Public Property Let ReturnedUserID(ByVal vReturnedUserID As Long)
    varReturnedUserID = vReturnedUserID
End Property

Public Property Get ReturnedUserID() As Long
    ReturnedUserID = varReturnedUserID
End Property

Public Property Let ReturnedCUserID(ByVal vReturnedCUserID As Long)
    varReturnedCUserID = vReturnedCUserID
End Property

Public Property Get ReturnedCUserID() As Long
    ReturnedCUserID = varReturnedCUserID
End Property

Public Property Let ReturnedDate(ByVal vReturnedDate As Date)
    varReturnedDate = vReturnedDate
End Property

Public Property Get ReturnedDate() As Date
    ReturnedDate = varReturnedDate
End Property

Public Property Let ReturnedTime(ByVal vReturnedTime As Date)
    varReturnedTime = vReturnedTime
End Property

Public Property Get ReturnedTime() As Date
    ReturnedTime = varReturnedTime
End Property

Public Property Let CancelledValue(ByVal vCancelledValue As Double)
    varCancelledValue = vCancelledValue
End Property

Public Property Get CancelledValue() As Double
    CancelledValue = varCancelledValue
End Property

Public Property Let ReturnedValue(ByVal vReturnedValue As Double)
    varReturnedValue = vReturnedValue
End Property

Public Property Get ReturnedValue() As Double
    ReturnedValue = varReturnedValue
End Property

Public Property Let SettledValue(ByVal vSettledValue As Double)
    varSettledValue = vSettledValue
End Property

Public Property Get SettledValue() As Double
    SettledValue = varSettledValue
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property

Public Property Let billValueUoToYesterday(ByVal vbillValueUoToYesterday As Double)
    varbillValueUoToYesterday = vbillValueUoToYesterday
End Property

Public Property Get billValueUoToYesterday() As Double
    billValueUoToYesterday = varbillValueUoToYesterday
End Property

Public Property Let billValueUoToToday(ByVal vbillValueUoToToday As Double)
    varbillValueUoToToday = vbillValueUoToToday
End Property

Public Property Get billValueUoToToday() As Double
    billValueUoToToday = varbillValueUoToToday
End Property

Public Property Let billValueToday(ByVal vbillValueToday As Double)
    varbillValueToday = vbillValueToday
End Property

Public Property Get billValueToday() As Double
    billValueToday = varbillValueToday
End Property

Public Property Let billValueUoToYesterdayS(ByVal vbillValueUoToYesterdayS As Double)
    varbillValueUoToYesterdayS = vbillValueUoToYesterdayS
End Property

Public Property Get billValueUoToYesterdayS() As Double
    billValueUoToYesterdayS = varbillValueUoToYesterdayS
End Property

Public Property Let billValueUoToTodayS(ByVal vbillValueUoToTodayS As Double)
    varbillValueUoToTodayS = vbillValueUoToTodayS
End Property

Public Property Get billValueUoToTodayS() As Double
    billValueUoToTodayS = varbillValueUoToTodayS
End Property

Public Property Let billValueTodayS(ByVal vbillValueTodayS As Double)
    varbillValueTodayS = vbillValueTodayS
End Property

Public Property Get billValueTodayS() As Double
    billValueTodayS = varbillValueTodayS
End Property

Public Property Let NetSaleValue(ByVal vNetSaleValue As Double)
    varNetSaleValue = vNetSaleValue
End Property

Public Property Get NetSaleValue() As Double
    NetSaleValue = varNetSaleValue
End Property


