VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SaleItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varSaleId As Long
    Private varSaleBillID As Long
    Private varCategoryID As Long
    Private varItemID As Long
    Private varBatchID As Long
    Private varStoreID As Long
    Private varDate As Date
    Private varTime As Date
    Private varStaffID As Long
    Private varCheckedStaffID As Long
    Private varRate As Double
    Private varAmount As Double
    Private varGrossPrice As Double
    Private varDiscount As Double
    Private varDiscountPercent
    Private varPrice As Double
    Private varCost As Double
    Private varBilledOutPatientID As Long
    Private varBilledInPatientID As Long
    Private varBilledBHTID As Long
    Private varBilledStaffID As Long
    Private varPaymentMethodID As Long
    Private varPaymentMethod As String
    Private varComments As String
    Private varDuration As Long
    Private varDurationID As Long
    Private varFrequencyID As Long
    Private varPMessageID As Long
    Private varDose As Double
    Private varDoseUnitID As Long
    Private varBilledUnitID As Long
    Private varReceivedOtherID As Long
    Private varupsize_ts

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblSale Where SaleID = " & varSaleId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !SaleBillID = varSaleBillID
        !CategoryID = varCategoryID
        !ItemID = varItemID
        !BatchID = varBatchID
        !StoreID = varStoreID
        !Date = varDate
        !Time = varTime
        !StaffID = varStaffID
        !CheckedStaffID = varCheckedStaffID
        !Rate = varRate
        !Amount = varAmount
        !GrossPrice = varGrossPrice
        !Discount = varDiscount
        !DiscountPercent = varDiscountPercent
        !Price = varPrice
        !Cost = varCost
        !BilledOutPatientID = varBilledOutPatientID
        !BilledInPatientID = varBilledInPatientID
        !BilledBHTID = varBilledBHTID
        !BilledStaffID = varBilledStaffID
        !PaymentMethodID = varPaymentMethodID
        !PaymentMethod = varPaymentMethod
        !Comments = varComments
        !Duration = varDuration
        !DurationID = varDurationID
        !FrequencyID = varFrequencyID
        !PMessageID = varPMessageID
        !Dose = varDose
        !DoseUnitID = varDoseUnitID
        !BilledUnitID = varBilledUnitID
        !ReceivedOtherID = varReceivedOtherID
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varSaleId = !NewID
        Else
            varSaleId = !SaleId
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSale WHERE SaleID = " & varSaleId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!SaleId) Then
               varSaleId = !SaleId
            End If
            If Not IsNull(!SaleBillID) Then
               varSaleBillID = !SaleBillID
            End If
            If Not IsNull(!CategoryID) Then
               varCategoryID = !CategoryID
            End If
            If Not IsNull(!ItemID) Then
               varItemID = !ItemID
            End If
            If Not IsNull(!BatchID) Then
               varBatchID = !BatchID
            End If
            If Not IsNull(!StoreID) Then
               varStoreID = !StoreID
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!CheckedStaffID) Then
               varCheckedStaffID = !CheckedStaffID
            End If
            If Not IsNull(!Rate) Then
               varRate = !Rate
            End If
            If Not IsNull(!Amount) Then
               varAmount = !Amount
            End If
            If Not IsNull(!GrossPrice) Then
               varGrossPrice = !GrossPrice
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!DiscountPercent) Then
               varDiscountPercent = !DiscountPercent
            End If
            If Not IsNull(!Price) Then
               varPrice = !Price
            End If
            If Not IsNull(!Cost) Then
               varCost = !Cost
            End If
            If Not IsNull(!BilledOutPatientID) Then
               varBilledOutPatientID = !BilledOutPatientID
            End If
            If Not IsNull(!BilledInPatientID) Then
               varBilledInPatientID = !BilledInPatientID
            End If
            If Not IsNull(!BilledBHTID) Then
               varBilledBHTID = !BilledBHTID
            End If
            If Not IsNull(!BilledStaffID) Then
               varBilledStaffID = !BilledStaffID
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!PaymentMethod) Then
               varPaymentMethod = !PaymentMethod
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!Duration) Then
               varDuration = !Duration
            End If
            If Not IsNull(!DurationID) Then
               varDurationID = !DurationID
            End If
            If Not IsNull(!FrequencyID) Then
               varFrequencyID = !FrequencyID
            End If
            If Not IsNull(!PMessageID) Then
               varPMessageID = !PMessageID
            End If
            If Not IsNull(!Dose) Then
               varDose = !Dose
            End If
            If Not IsNull(!DoseUnitID) Then
               varDoseUnitID = !DoseUnitID
            End If
            If Not IsNull(!BilledUnitID) Then
               varBilledUnitID = !BilledUnitID
            End If
            If Not IsNull(!ReceivedOtherID) Then
               varReceivedOtherID = !ReceivedOtherID
            End If
            If Not IsNull(!upsize_ts) Then
               varupsize_ts = !upsize_ts
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varSaleId = 0
    varSaleBillID = 0
    varCategoryID = 0
    varItemID = 0
    varBatchID = 0
    varStoreID = 0
    varDate = Empty
    varTime = Empty
    varStaffID = 0
    varCheckedStaffID = 0
    varRate = 0
    varAmount = 0
    varGrossPrice = 0
    varDiscount = 0
    varDiscountPercent = Empty
    varPrice = 0
    varCost = 0
    varBilledOutPatientID = 0
    varBilledInPatientID = 0
    varBilledBHTID = 0
    varBilledStaffID = 0
    varPaymentMethodID = 0
    varPaymentMethod = Empty
    varComments = Empty
    varDuration = 0
    varDurationID = 0
    varFrequencyID = 0
    varPMessageID = 0
    varDose = 0
    varDoseUnitID = 0
    varBilledUnitID = 0
    varReceivedOtherID = 0
    varupsize_ts = Empty
End Sub

Public Property Let SaleId(ByVal vSaleId As Long)
    Call clearData
    varSaleId = vSaleId
    Call loadData
End Property

Public Property Get SaleId() As Long
    SaleId = varSaleId
End Property

Public Property Let SaleBillID(ByVal vSaleBillID As Long)
    varSaleBillID = vSaleBillID
End Property

Public Property Get SaleBillID() As Long
    SaleBillID = varSaleBillID
End Property

Public Property Let CategoryID(ByVal vCategoryID As Long)
    varCategoryID = vCategoryID
End Property

Public Property Get CategoryID() As Long
    CategoryID = varCategoryID
End Property

Public Property Let ItemID(ByVal vItemID As Long)
    varItemID = vItemID
End Property

Public Property Get ItemID() As Long
    ItemID = varItemID
End Property

Public Property Let BatchID(ByVal vBatchID As Long)
    varBatchID = vBatchID
End Property

Public Property Get BatchID() As Long
    BatchID = varBatchID
End Property

Public Property Let StoreID(ByVal vStoreID As Long)
    varStoreID = vStoreID
End Property

Public Property Get StoreID() As Long
    StoreID = varStoreID
End Property

Public Property Let ItemDate(ByVal vDate As Date)
    varDate = vDate
End Property

Public Property Get ItemDate() As Date
    Date = varDate
End Property

Public Property Let Time(ByVal vTime As Date)
    varTime = vTime
End Property

Public Property Get Time() As Date
    Time = varTime
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let CheckedStaffID(ByVal vCheckedStaffID As Long)
    varCheckedStaffID = vCheckedStaffID
End Property

Public Property Get CheckedStaffID() As Long
    CheckedStaffID = varCheckedStaffID
End Property

Public Property Let Rate(ByVal vRate As Double)
    varRate = vRate
End Property

Public Property Get Rate() As Double
    Rate = varRate
End Property

Public Property Let Amount(ByVal vAmount As Double)
    varAmount = vAmount
End Property

Public Property Get Amount() As Double
    Amount = varAmount
End Property

Public Property Let GrossPrice(ByVal vGrossPrice As Double)
    varGrossPrice = vGrossPrice
End Property

Public Property Get GrossPrice() As Double
    GrossPrice = varGrossPrice
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let DiscountPercent(ByVal vDiscountPercent)
    varDiscountPercent = vDiscountPercent
End Property

Public Property Get DiscountPercent()
    DiscountPercent = varDiscountPercent
End Property

Public Property Let Price(ByVal vPrice As Double)
    varPrice = vPrice
End Property

Public Property Get Price() As Double
    Price = varPrice
End Property

Public Property Let Cost(ByVal vCost As Double)
    varCost = vCost
End Property

Public Property Get Cost() As Double
    Cost = varCost
End Property

Public Property Let BilledOutPatientID(ByVal vBilledOutPatientID As Long)
    varBilledOutPatientID = vBilledOutPatientID
End Property

Public Property Get BilledOutPatientID() As Long
    BilledOutPatientID = varBilledOutPatientID
End Property

Public Property Let BilledInPatientID(ByVal vBilledInPatientID As Long)
    varBilledInPatientID = vBilledInPatientID
End Property

Public Property Get BilledInPatientID() As Long
    BilledInPatientID = varBilledInPatientID
End Property

Public Property Let BilledBHTID(ByVal vBilledBHTID As Long)
    varBilledBHTID = vBilledBHTID
End Property

Public Property Get BilledBHTID() As Long
    BilledBHTID = varBilledBHTID
End Property

Public Property Let BilledStaffID(ByVal vBilledStaffID As Long)
    varBilledStaffID = vBilledStaffID
End Property

Public Property Get BilledStaffID() As Long
    BilledStaffID = varBilledStaffID
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Long)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Long
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let PaymentMethod(ByVal vPaymentMethod As String)
    varPaymentMethod = vPaymentMethod
End Property

Public Property Get PaymentMethod() As String
    PaymentMethod = varPaymentMethod
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let Duration(ByVal vDuration As Long)
    varDuration = vDuration
End Property

Public Property Get Duration() As Long
    Duration = varDuration
End Property

Public Property Let DurationID(ByVal vDurationID As Long)
    varDurationID = vDurationID
End Property

Public Property Get DurationID() As Long
    DurationID = varDurationID
End Property

Public Property Let FrequencyID(ByVal vFrequencyID As Long)
    varFrequencyID = vFrequencyID
End Property

Public Property Get FrequencyID() As Long
    FrequencyID = varFrequencyID
End Property

Public Property Let PMessageID(ByVal vPMessageID As Long)
    varPMessageID = vPMessageID
End Property

Public Property Get PMessageID() As Long
    PMessageID = varPMessageID
End Property

Public Property Let Dose(ByVal vDose As Double)
    varDose = vDose
End Property

Public Property Get Dose() As Double
    Dose = varDose
End Property

Public Property Let DoseUnitID(ByVal vDoseUnitID As Long)
    varDoseUnitID = vDoseUnitID
End Property

Public Property Get DoseUnitID() As Long
    DoseUnitID = varDoseUnitID
End Property

Public Property Let BilledUnitID(ByVal vBilledUnitID As Long)
    varBilledUnitID = vBilledUnitID
End Property

Public Property Get BilledUnitID() As Long
    BilledUnitID = varBilledUnitID
End Property

Public Property Let ReceivedOtherID(ByVal vReceivedOtherID As Long)
    varReceivedOtherID = vReceivedOtherID
End Property

Public Property Get ReceivedOtherID() As Long
    ReceivedOtherID = varReceivedOtherID
End Property

Public Property Let upsize_ts(ByVal vupsize_ts)
    varupsize_ts = vupsize_ts
End Property

Public Property Get upsize_ts()
    upsize_ts = varupsize_ts
End Property


