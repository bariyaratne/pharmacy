VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UserStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varUserStoreId As Long
    Private varUserId As Long
    Private varStoreId As Long
    Private vardeleted As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    Dim newEntry As Boolean
    With rsTem
        temSQL = "SELECT * FROM tblUserStore Where UserStoreId = " & varUserStoreId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then
            .AddNew
            newEntry = True
        Else
            newEntry = False
        End If
        !UserId = varUserId
        !StoreId = varStoreId
        !deleted = vardeleted
        .Update
        If newEntry = True Then
            .Close
            temSQL = "SELECT @@IDENTITY AS NewID"
           .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            varUserStoreId = !NewID
        Else
            varUserStoreId = !UserStoreId
        End If
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblUserStore WHERE UserStoreId = " & varUserStoreId
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!UserStoreId) Then
               varUserStoreId = !UserStoreId
            End If
            If Not IsNull(!UserId) Then
               varUserId = !UserId
            End If
            If Not IsNull(!StoreId) Then
               varStoreId = !StoreId
            End If
            If Not IsNull(!deleted) Then
               vardeleted = !deleted
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varUserStoreId = 0
    varUserId = 0
    varStoreId = 0
    vardeleted = False
End Sub

Public Property Let UserStoreId(ByVal vUserStoreId As Long)
    Call clearData
    varUserStoreId = vUserStoreId
    Call loadData
End Property

Public Property Get UserStoreId() As Long
    UserStoreId = varUserStoreId
End Property

Public Property Let UserId(ByVal vUserId As Long)
    varUserId = vUserId
End Property

Public Property Get UserId() As Long
    UserId = varUserId
End Property

Public Property Let StoreId(ByVal vStoreId As Long)
    varStoreId = vStoreId
End Property

Public Property Get StoreId() As Long
    StoreId = varStoreId
End Property

Public Property Let deleted(ByVal vdeleted As Boolean)
    vardeleted = vdeleted
End Property

Public Property Get deleted() As Boolean
    deleted = vardeleted
End Property


