VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmAllItemBHTIssue 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "All Item Issue to BHT"
   ClientHeight    =   12180
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15705
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   12180
   ScaleWidth      =   15705
   Begin TabDlg.SSTab SSTab1 
      Height          =   7935
      Left            =   0
      TabIndex        =   18
      Top             =   2280
      Width           =   15405
      _ExtentX        =   27173
      _ExtentY        =   13996
      _Version        =   393216
      Tab             =   2
      TabHeight       =   520
      TabCaption(0)   =   "Item Summery"
      TabPicture(0)   =   "frmAllItemBHTIssue.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "GridIssue"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Sale Bills"
      TabPicture(1)   =   "frmAllItemBHTIssue.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "gridSaleBills"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Cancelled Bills"
      TabPicture(2)   =   "frmAllItemBHTIssue.frx":0038
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "gridCancelBills"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid GridIssue 
         Height          =   7335
         Left            =   -74880
         TabIndex        =   19
         Top             =   480
         Width           =   15135
         _ExtentX        =   26696
         _ExtentY        =   12938
         _Version        =   393216
      End
      Begin MSFlexGridLib.MSFlexGrid gridSaleBills 
         Height          =   7335
         Left            =   -74880
         TabIndex        =   20
         Top             =   480
         Width           =   15135
         _ExtentX        =   26696
         _ExtentY        =   12938
         _Version        =   393216
      End
      Begin MSFlexGridLib.MSFlexGrid gridCancelBills 
         Height          =   7335
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   15135
         _ExtentX        =   26696
         _ExtentY        =   12938
         _Version        =   393216
      End
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   10800
      Width           =   3255
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   4200
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   11400
      Width           =   3255
   End
   Begin btButtonEx.ButtonEx btnProcess 
      Height          =   375
      Left            =   7440
      TabIndex        =   15
      Top             =   120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Process"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcUnit 
      Height          =   1620
      Left            =   1320
      TabIndex        =   9
      Top             =   600
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   2858
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   1
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   12240
      TabIndex        =   5
      Top             =   11520
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   10920
      TabIndex        =   4
      Top             =   11520
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFromDate 
      Height          =   375
      Left            =   1320
      TabIndex        =   0
      Top             =   120
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   111869955
      CurrentDate     =   29224
   End
   Begin MSComCtl2.DTPicker dtpToDate 
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   120
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   111869955
      CurrentDate     =   29224
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   9600
      TabIndex        =   14
      Top             =   11520
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label8 
      Caption         =   "Net Value"
      Height          =   255
      Left            =   8640
      TabIndex        =   13
      Top             =   11040
      Width           =   2655
   End
   Begin VB.Label lblNetValue 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   11520
      TabIndex        =   12
      Top             =   11040
      Width           =   1935
   End
   Begin VB.Label Label6 
      Caption         =   "Total Return"
      Height          =   255
      Left            =   8640
      TabIndex        =   11
      Top             =   10680
      Width           =   2655
   End
   Begin VB.Label lblTotalReturn 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   11520
      TabIndex        =   10
      Top             =   10680
      Width           =   1935
   End
   Begin VB.Label Label4 
      Caption         =   "BHT"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label lblTotalValue 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   11520
      TabIndex        =   7
      Top             =   10320
      Width           =   1935
   End
   Begin VB.Label Label3 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   8640
      TabIndex        =   6
      Top             =   10320
      Width           =   2655
   End
   Begin VB.Label Label2 
      Caption         =   "To"
      Height          =   255
      Left            =   4200
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "From"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmAllItemBHTIssue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temGroupBy As String
    Dim temOrderBY As String
    Dim i As Long
    Dim TotalValue As Double
    Dim TotalReturn As Double
    Dim temTopic As String
    Dim temSubTopic As String
    Dim CsetPrinter As New cSetDfltPrinter
    
    Dim rsItemIssie As New ADODB.Recordset
    Dim rsViewUnit As New ADODB.Recordset
    
    
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    
Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel GridIssue, "Issue of Medicins to BHT - " & dtcUnit.text
End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    If SelectForm(cmbPaper.text, Me.hdc) = 1 Then
        GridPrint GridIssue, ThisReportFormat, "All Item Issue - " & dtcUnit.text, Format(Date, LongDateFormat)
        Printer.EndDoc
    Else
        MsgBox "Printer select error"
        Printer.KillDoc
        Exit Sub
    End If
End Sub

'Private Sub btnPrint_Click()
'    printBhtItems Val(cmbBht.BoundText), Me.hwnd, dtpFrom.Value, dtpTo.Value, cmbPrinter.text, cmbPaper.text
'End Sub

Private Sub btnProcess_Click()
    Call FormatGrid
    Call fillGrid
End Sub

Private Sub Form_Load()
    dtpFromDate.Value = Date
    dtpToDate.Value = Date
    Call fillCombos
    Call FormatGrid
    
    getPrinterSettings Me
    
    
    
    
    GetCommonSettings Me
End Sub

Private Sub fillCombos()
    With rsViewUnit
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblBHT Order by BHT"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcUnit
        Set .RowSource = rsViewUnit
        .ListField = "BHT"
        .BoundColumn = "BHTID"
    End With
End Sub

Private Sub FormatGrid()
    With GridIssue
        .Clear
        
        .Rows = 1
        .Cols = 5
        
        .FixedCols = 0
        
        .Row = 0
        
        .Col = 0
        .CellAlignment = 4
        .text = "Item"
        
        .Col = 1
        .CellAlignment = 4
        .text = "Qty"
        
        .Col = 2
        .CellAlignment = 4
        .text = "Val"
        
        .Col = 3
        .CellAlignment = 4
        .text = "Rtn. Qty"
        
        .Col = 4
        .CellAlignment = 4
        .text = "Rtn. Val"
        
    End With
End Sub

Private Sub fillGrid()
   ' On Error Resume Next
    Dim MyItemSaleReturn As ItemSaleAndReturn
    If IsNumeric(dtcUnit.BoundText) = False Then Exit Sub
    Screen.MousePointer = vbHourglass
    DoEvents
    With rsItemIssie
        temSQL = "SELECT DISTINCT dbo.tblItem.ItemID, dbo.tblItem.Display " & _
                    "FROM dbo.tblReturn LEFT OUTER JOIN " & _
                        "dbo.tblSale RIGHT OUTER JOIN " & _
                        "dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID LEFT OUTER JOIN " & _
                        "dbo.tblItem ON dbo.tblSale.ItemID = dbo.tblItem.ItemID ON dbo.tblReturn.ItemID = dbo.tblItem.ItemID RIGHT OUTER JOIN " & _
                        "dbo.tblReturnBill ON dbo.tblReturn.ReturnBillID = dbo.tblReturnBill.ReturnBillID " & _
                        "WHERE     (dbo.tblReturn.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFromDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpToDate.Value, "dd MMMM yyyy") & "', 102)) OR " & _
                        "(dbo.tblSale.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFromDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpToDate.Value, "dd MMMM yyyy") & "', 102)) " & _
                        "ORDER BY dbo.tblItem.Display"
                        
                        
        temSQL = "SELECT DISTINCT TOP 100 PERCENT dbo.tblItem.ItemID, dbo.tblItem.Display " & _
                    "FROM         dbo.tblSale RIGHT OUTER JOIN " & _
                      "dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID LEFT OUTER JOIN " & _
                      "dbo.tblItem ON dbo.tblSale.ItemID = dbo.tblItem.ItemID " & _
                        "WHERE  tblSaleBill.BilledBhtId = " & Val(dtcUnit.BoundText) & "   AND " & _
                        "tblSale.Date BETWEEN '" & Format(dtpFromDate.Value, "dd MMMM yyyy") & "' AND '" & Format(dtpToDate.Value, "dd MMMM yyyy") & "' " & _
                        "ORDER BY dbo.tblItem.Display"
                        
                        
                        
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        TotalValue = 0
        TotalReturn = 0
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            GridIssue.Rows = .RecordCount + 1
            i = 1
            While .EOF = False
                If IsNull(!ItemID) = False Then
                    MyItemSaleReturn = PeriodSale(dtpFromDate.Value, dtpToDate.Value, !ItemID, Val(dtcUnit.BoundText))
                    If MyItemSaleReturn.SaleValue <> 0 Or MyItemSaleReturn.ReturnValue <> 0 Then
                        If Not IsNull(!Display) Then GridIssue.TextMatrix(i, 0) = !Display
                        GridIssue.TextMatrix(i, 1) = MyItemSaleReturn.SaleQuentity
                        GridIssue.TextMatrix(i, 2) = Format(MyItemSaleReturn.SaleValue, "#,##0.00")
                        TotalValue = TotalValue + MyItemSaleReturn.SaleValue
                        GridIssue.TextMatrix(i, 3) = MyItemSaleReturn.ReturnQuentity
                        GridIssue.TextMatrix(i, 4) = Format(MyItemSaleReturn.ReturnValue, "#,##0.00")
                        TotalReturn = TotalReturn + MyItemSaleReturn.ReturnValue
                        i = i + 1
                    End If
                End If
                .MoveNext
            Wend
        End If
        GridIssue.Rows = i
        lblTotalValue.Caption = Format(TotalValue, "#,##0.00")
        lblTotalReturn.Caption = Format(TotalReturn, "#,##0.00")
        lblNetValue.Caption = Format(TotalValue - TotalReturn, "#,##0.00")
    End With
    With GridIssue
        .Rows = .Rows + 4
        
        .Row = i + 1
        .Col = 0
        .text = "Total Value"
        .Col = 2
        .text = Format(TotalValue, "#,##0.00")
        
        .Row = i + 2
        .Col = 0
        .text = "Total Return"
        .Col = 2
        .text = Format(TotalReturn, "#,##0.00")
        
        .Row = i + 3
        .Col = 0
        .text = "Net Value"
        .Col = 2
        .text = Format(TotalValue - TotalReturn, "#,##0.00")
        
    
    End With
    
    
    Screen.MousePointer = vbDefault
    DoEvents
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
    setPrinterSettings Me
    
End Sub
