VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmBhtItems 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Items Issued to BHTs"
   ClientHeight    =   8445
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   12210
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8445
   ScaleWidth      =   12210
   Begin VB.ComboBox cmbPaper 
      Height          =   315
      Left            =   3480
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   8040
      Width           =   3255
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   8040
      Width           =   3255
   End
   Begin VB.TextBox txtDetails 
      Height          =   1335
      Left            =   7800
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   120
      Width           =   4215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   4560
      TabIndex        =   4
      Top             =   1080
      Width           =   3135
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   9600
      TabIndex        =   3
      Top             =   7800
      Width           =   1215
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   8280
      TabIndex        =   2
      Top             =   7800
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   10920
      TabIndex        =   1
      Top             =   7800
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridBills 
      Height          =   5895
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   10398
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   5280
      TabIndex        =   5
      Top             =   120
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   100794371
      CurrentDate     =   41272
   End
   Begin MSDataListLib.DataCombo cmbBht 
      Height          =   1545
      Left            =   840
      TabIndex        =   6
      Top             =   120
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   2725
      _Version        =   393216
      Style           =   1
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5280
      TabIndex        =   7
      Top             =   600
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   100794371
      CurrentDate     =   41272
   End
   Begin VB.Label Label2 
      Caption         =   "BHT"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "From"
      Height          =   255
      Left            =   4560
      TabIndex        =   9
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "To"
      Height          =   255
      Left            =   4560
      TabIndex        =   8
      Top             =   480
      Width           =   1215
   End
End
Attribute VB_Name = "frmBhtItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim printerName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Private CsetPrinter As New cSetDfltPrinter


Private Sub Form_Resize()
    gridBills.Width = Me.Width - 500
    gridBills.Height = Me.Height - 3000
    btnClose.Top = Me.Height - 1250
    btnPrint.Top = btnClose.Top
    btnExcel.Top = btnClose.Top
End Sub

Private Sub btnPrint_Click()
    printBhtItems Val(cmbBht.BoundText), Me.hwnd, dtpFrom.Value, dtpTo.Value, cmbPrinter.text, cmbPaper.text
End Sub

Private Sub btnBillPrint_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            printSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            printSaleBillA4 id, Me.hwnd
        End If
    End If
    
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()
    fillBhts cmbBht
End Sub

Private Sub fillGrid()
    Dim tPt As New Patient
    Dim tBht As New BHT
    Dim sql As String
    
    tBht.BHTID = Val(cmbBht.BoundText)
    tPt.PatientID = tBht.PatientID
    
    
    sql = "SELECT  dbo.tblItem.Display, dbo.tblSale.Amount as [Quatity], dbo.tblSale.Price,  Cashier.Name AS [Billed User]" & _
            "FROM dbo.tblItem RIGHT OUTER JOIN dbo.tblSale LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID LEFT OUTER JOIN dbo.tblStaff Cashier ON dbo.tblSaleBill.StaffID = Cashier.StaffID ON dbo.tblItem.ItemID = dbo.tblSale.ItemID " & _
            "WHERE (dbo.tblSaleBill.BilledBHTID = " & Val(cmbBht.BoundText) & ") AND (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFrom.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTo.Value, "dd MMMM yyyy") & "', 102)) "
    sql = sql & " ORDER BY dbo.tblSaleBill.SaleBillID"
    
    txtDetails.text = "Patient " & tPt.FirstName
    
    
    FillAnyGrid sql, gridBills
    
    txtDetails.text = txtDetails.text & vbNewLine & rowTotal(gridBills, 2, 1, gridBills.Rows - 1, False)
    
    
    formatGridString gridBills, 2, "#,##0.00"
    
End Sub

Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    dtpFrom.Value = Date
    dtpTo.Value = Date
End Sub

Private Sub btnExcel_Click()
    GridToExcel1 gridBills, "All Items Issued for BHT - " & cmbBht.text, Format(dtpFrom.Value, LongDateFormat) & " - " & Format(dtpTo.Value, LongDateFormat)
End Sub

Private Sub btnPreview_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            previewSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            previewSaleBillA4 id, Me.hwnd
        End If
    End If

End Sub

Private Sub btnProcess_Click()
    Call fillGrid
End Sub

Private Sub Form_Load()
    fillCombos
    getSettings
    btnProcess_Click
    getPrinterSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'    saveSettings
    setPrinterSettings Me
End Sub

