VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Begin VB.Form frmCreditSettle 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Credit Settle"
   ClientHeight    =   4170
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8985
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4170
   ScaleWidth      =   8985
   Begin VB.TextBox txtComments 
      Height          =   1815
      Left            =   2160
      TabIndex        =   8
      Top             =   1680
      Width           =   4575
   End
   Begin VB.TextBox txtBalance 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   375
      Left            =   6840
      TabIndex        =   7
      Top             =   120
      Width           =   1935
   End
   Begin VB.CommandButton btnSettle 
      Caption         =   "&Settle"
      Height          =   495
      Left            =   5160
      TabIndex        =   6
      Top             =   3600
      Width           =   1575
   End
   Begin VB.TextBox txtAmount 
      Alignment       =   1  'Right Justify
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      Top             =   720
      Width           =   1935
   End
   Begin MSDataListLib.DataCombo cmbName 
      Height          =   360
      Left            =   2160
      TabIndex        =   1
      Top             =   120
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbPaymentMethod 
      Height          =   360
      Left            =   2160
      TabIndex        =   5
      Top             =   1200
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "Payment Method"
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Amount"
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Name"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmCreditSettle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsTem As New ADODB.Recordset
    Dim temSQL As String

Private Sub fillCombos()
    Dim Staff As New clsFillCombos
    Staff.FillSpecificField cmbName, "Staff", "Name"
    Dim PM As New clsFillCombos
    PM.FillSpecificFieldBoolCombo cmbPaymentMethod, "PaymentMethod", "PaymentMethod", "PaymentMethod", "FinalPay"
End Sub

Private Sub btnSettle_Click()
    Dim rsTem As New ADODB.Recordset
    Dim temID As Long
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * from  tblIncomeBill where IncomeBillID = 0"
        
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        .AddNew
        !Date = Date
        !Time = Time
        !UserID = UserID
        !StoreID = UserStoreID
        !GrossTotal = Val(txtAmount.Text)
        !NetTotal = Val(txtAmount.Text)
        !Discount = 0
        !IsOtherBill = True
        !Completed = True
        !CompletedUserID = UserID
        !CompletedDate = Date
        !CompletedTime = Time
        !BillSettled = True
        !PaymentMethodID = Val(cmbPaymentMethod.BoundText)
        !PaymentComments = txtComments.Text
        !OtherBillID = Val(cmbName.BoundText)
        .Update
        temID = !IncomeBillID
        .Close
'        temSQL = "Select * from tblIncome where IncomeID = 0"
'        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
'        .AddNew
'        !Time = Time
'        !Date = Date
'        !StaffID = UserID
'        !StoreID = UserStoreID
'        !Price = Val(txtAmount.Text)
'        !PaymentMethodID = Val(cmbPaymentMethod.BoundText)
'        !BilledStaffID = Val(cmbName.BoundText)
'        !PaymentMethod = cmbPaymentMethod.Text
'        !IsIncome = True
''        !IncomeBillID = temID
'        .Update
'        .Close
        temSQL = "Select * from tblStaff where staffID = " & Val(cmbName.BoundText)
        If .State = 1 Then .Close
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Credit = !Credit + Val(txtAmount.Text)
            .Update
        End If
        .Close
    End With
    txtAmount.Text = ""
    txtComments.Text = ""
    txtBalance.Text = ""
    temID = Val(cmbName.BoundText)
    cmbName.Text = Empty
    cmbName.BoundText = temID
End Sub

Private Sub cmbName_Change()
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblStaff where StaffID = " & Val(cmbName.BoundText)
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            txtBalance.Text = Format(Abs(!Credit), "0.00")
            .Close
        End If
    End With
End Sub

Private Sub cmbName_Click(Area As Integer)
    cmbName_Change
End Sub

Private Sub Form_Load()
    Call fillCombos
End Sub
