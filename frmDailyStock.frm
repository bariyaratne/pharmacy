VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmDailyStock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Stock"
   ClientHeight    =   8775
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13425
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8775
   ScaleWidth      =   13425
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   12000
      TabIndex        =   6
      Top             =   7920
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   7920
      Width           =   1215
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   4
      Top             =   7920
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridStock 
      Height          =   6735
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   11880
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "P&rocess"
      Height          =   495
      Left            =   6840
      TabIndex        =   2
      Top             =   360
      Width           =   2415
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   2160
      TabIndex        =   1
      Top             =   360
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   97648643
      CurrentDate     =   41383
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   1215
   End
End
Attribute VB_Name = "frmDailyStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim sql As String
    

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridStock, "Stock Value", Format(dtpDate.Value, "dd MMMM yyyy")
End Sub

Private Sub btnProcess_Click()
    fillGrid
End Sub

Private Sub fillGrid()
    sql = "SELECT TOP 100 PERCENT dbo.tblStore.Store as [Store] , avg(dbo.tblStockValueHx.stockValue) as [Stock Value at Sale Rate], avg(dbo.tblStockValueHx.stockPurchaseValue) as [Stock Value at Purchase Rate] " & _
            "FROM dbo.tblStockValueHx LEFT OUTER JOIN dbo.tblStore ON dbo.tblStockValueHx.storeId = dbo.tblStore.StoreID " & _
            "WHERE (dbo.tblStockValueHx.valueDate = CONVERT(DATETIME, '" & Format(dtpDate.Value, "dd MMMM yyyy") & "', 102)) " & _
            "GROUP BY dbo.tblStore.Store"
    FillAnyGrid sql, gridStock
    Dim totS As Double
    Dim totP As Double
    
    totS = rowTotal(gridStock, 1, 1, gridStock.Rows - 1, False)
    totP = rowTotal(gridStock, 2, 1, gridStock.Rows - 1, False)
    
    With gridStock
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .text = "Total"
        .Col = 1
        .text = totS
        .Col = 2
        .text = totP
        formatGridString gridStock, 1, "#,##0.00"
        formatGridString gridStock, 2, "#,##0.00"
    
        .ColWidth(0) = 2000
        .ColWidth(1) = 2900
        .ColWidth(2) = 2900
    
    End With
    
End Sub

