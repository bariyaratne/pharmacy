VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmIssueDailySummery 
   Caption         =   "Issue Daily Summery"
   ClientHeight    =   6630
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6630
   ScaleWidth      =   13695
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   5400
      TabIndex        =   3
      Top             =   120
      Width           =   1335
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   5400
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8760
      TabIndex        =   1
      Top             =   5400
      Width           =   1215
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   0
      Top             =   5400
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridSum 
      Height          =   4695
      Left            =   240
      TabIndex        =   4
      Top             =   600
      Width           =   14295
      _ExtentX        =   25215
      _ExtentY        =   8281
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1200
      TabIndex        =   5
      Top             =   120
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   105906179
      CurrentDate     =   41234
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "frmIssueDailySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim saleCats As New Collection


Private Sub btnExcel_Click()
    GridToExcel gridSum, "Daily Issue Summery - " & UserStore, Format(dtpDate.Value, LongDateFormat)
End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    ThisReportFormat.ReportPrintOrientation = Landscape
    GridPrint gridSum, ThisReportFormat, "Daily Issue Summery - " & UserStore, Format(dtpDate.Value, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    DisplayDetails
End Sub

Private Sub Form_Load()
    getSettings
    dtpDate.Value = Date
    DisplayDetails
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    Set saleCats = findAllStores
End Sub

Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    saveSettings
End Sub

Private Sub DisplayDetails()
    Dim temCat As clsStore
    Dim Col As Integer
    Dim Row As Integer
    
    Dim cashSum1 As Double
    Dim nonCashSum1 As Double
    Dim unitSum1 As Double
    
    Dim cashSum2 As Double
    Dim nonCashSum2 As Double
    Dim unitSum2 As Double
    
    Dim sum1 As Double
    Dim sum2 As Double
    
    With gridSum
        Col = 1
        .Rows = 5
        .Cols = 1
        
        .TextMatrix(0, 0) = "Date"
        .TextMatrix(1, 0) = Format(dtpDate.Value, "dd MMMM yyyy")
        .TextMatrix(2, 0) = Format(dtpDate.Value - 1, "dd MMMM yyyy")
        .TextMatrix(3, 0) = "Total"

        Set saleCats = findAllStores
        
        For Each temCat In saleCats
                sum1 = findNetIssue(dtpDate.Value, dtpDate.Value, UserStoreID, temCat.StoreID)
                sum2 = findNetIssue(DateSerial(Year(dtpDate.Value), Month(dtpDate.Value), 1), dtpDate.Value - 1, UserStoreID, temCat.StoreID)
                
                If sum1 <> 0 Or sum2 <> 0 Then
                     .Cols = .Cols + 1
                     .TextMatrix(0, Col) = temCat.Store
                     .TextMatrix(1, Col) = Format(sum1, "#,##0.00")
                     .TextMatrix(2, Col) = Format(sum2, "#,##0.00")
                     unitSum1 = unitSum1 + sum1
                     unitSum2 = unitSum2 + sum2
                     .TextMatrix(3, Col) = Format(sum1 + sum2, "#,##0.00")
                     Col = Col + 1
            
                End If
        Next
        
        Col = Col - 1
       .Cols = .Cols + 2

        
        .TextMatrix(0, Col + 1) = "Total"
        .TextMatrix(1, Col + 1) = Format(unitSum1, "#,##0.00")
        .TextMatrix(2, Col + 1) = Format(unitSum2, "#,##0.00")
        .TextMatrix(3, Col + 1) = Format(unitSum1 + unitSum2, "#,##0.00")
        
        .TextMatrix(0, Col + 2) = "Current Stock Value"
        .TextMatrix(1, Col + 2) = Format(findStockValue(UserStoreID, 0), "#,##0.00")
        
    End With
End Sub

