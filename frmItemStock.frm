VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmItemStock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Stock"
   ClientHeight    =   12165
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14820
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   12165
   ScaleWidth      =   14820
   Begin MSDataListLib.DataCombo cmbItem 
      Height          =   1605
      Left            =   2400
      TabIndex        =   11
      Top             =   1560
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   2831
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   1
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.OptionButton optItem 
      Caption         =   "Selected Item"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   1560
      Width           =   2895
   End
   Begin MSDataListLib.DataCombo cmbCategory 
      Height          =   360
      Left            =   2400
      TabIndex        =   3
      Top             =   1080
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.OptionButton optCat 
      Caption         =   "&All Items of category"
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   960
      Width           =   2895
   End
   Begin VB.OptionButton optAll 
      Caption         =   "&All Items"
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   480
      Value           =   -1  'True
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   7440
      TabIndex        =   7
      Top             =   1680
      Width           =   1935
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   13320
      TabIndex        =   2
      Top             =   11640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   495
      Left            =   12000
      TabIndex        =   1
      Top             =   11640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid GridStock 
      Height          =   8175
      Left            =   120
      TabIndex        =   0
      Top             =   3360
      Width           =   14535
      _ExtentX        =   25638
      _ExtentY        =   14420
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   10680
      TabIndex        =   4
      Top             =   11640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbStores 
      Height          =   360
      Left            =   2400
      TabIndex        =   5
      Top             =   120
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "Unit"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmItemStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temGroupBy As String
    Dim temOrderBY As String
    Dim rsBatchStock As New ADODB.Recordset
    Dim temTopic As String
    Dim temSubTopic As String
    Dim i As Integer
    Dim TotalValue As Double
    Dim rsViewCategory As New ADODB.Recordset
    Dim rsStores As New ADODB.Recordset
    Dim rsItem As New ADODB.Recordset
    
Private Sub FormatGrid()
    With GridStock
        .Clear
        .Rows = 1
        .Cols = 6
        .Row = 0
        
        .Col = 0
        .text = "Item"
        .CellAlignment = 4
        
        .Col = 1
        .text = "Stock"
        .CellAlignment = 4
        
        .Col = 2
        .text = "Sale Rate"
        .CellAlignment = 4
        
        .Col = 3
        .text = "Sale Value"
        .CellAlignment = 4
        
        .Col = 4
        .text = "Purchase Rate"
        .CellAlignment = 4
        
        .Col = 5
        .text = "Purchase Value"
        .CellAlignment = 4
        
        .ColWidth(0) = 4500
        .ColWidth(1) = 1800
        .ColWidth(2) = 1800
        .ColWidth(3) = 2100
        .ColWidth(4) = 1800
        .ColWidth(5) = 2100
    
    End With
End Sub
    
Private Sub FormatGridOld()
    With GridStock
        .Clear
        .Rows = 1
        .Cols = 2
        .Row = 0
        .Col = 0
        .text = "Item"
        .CellAlignment = 4
        .Col = 1
        .text = "Stock"
        .CellAlignment = 4
        .CellAlignment = 4
        .ColWidth(0) = 7500
        .ColWidth(1) = 2100
    End With
End Sub

Private Sub fillGrid()
    Screen.MousePointer = vbHourglass
    DoEvents
    With rsBatchStock
        If .State = 1 Then .Close
        temSelect = "SELECT tblItem.ItemId, tblItem.Display, tblItem.Code, sum(tblBatchStock.Stock) as ItemStock "
        temFrom = "FROM ((tblBatch RIGHT JOIN tblBatchStock ON tblBatch.BatchID = tblBatchStock.BatchID) LEFT JOIN tblItem ON tblBatch.ItemID = tblItem.ItemID) LEFT JOIN tblCurrentPurchasePrice ON tblItem.ItemID = tblCurrentPurchasePrice.ItemID  "
        
        If optCat.Value = True Then
            If Val(cmbCategory.BoundText) <= 0 Then
                MsgBox "Please select a category"
                cmbCategory.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            ElseIf IsNumeric(cmbStores.BoundText) = False Then
                temWhere = "WHERE (((tblBatchStock.Stock)>0) And (tblItem.ItemCategoryID = " & Val(cmbCategory.BoundText) & " ) )"
            Else
                temWhere = "WHERE (((tblBatchStock.Stock)>0) And ((tblBatchStock.StoreID)=" & Val(cmbStores.BoundText) & ") And (tblItem.ItemCategoryID = " & Val(cmbCategory.BoundText) & " ) )"
            End If
        ElseIf optItem.Value = True Then
            If Val(cmbItem.BoundText) <= 0 Then
                MsgBox "Please select an Item"
                cmbItem.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            ElseIf IsNumeric(cmbStores.BoundText) = True Then
                temWhere = "WHERE (((tblBatchStock.Stock)>0)  And ((tblBatchStock.StoreID)=" & Val(cmbStores.BoundText) & ") And  (tblItem.ItemID = " & Val(cmbItem.BoundText) & " ) )"
            ElseIf IsNumeric(cmbStores.BoundText) = False Then
                temWhere = "WHERE (((tblBatchStock.Stock)>0) And ((tblBatchStock.ItemID)=" & Val(cmbItem.BoundText) & ") ) "
            Else
                MsgBox "Error"
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        ElseIf optAll.Value = True Then
            If IsNumeric(cmbStores.BoundText) = False Then
               temWhere = "WHERE (((tblBatchStock.Stock)>0))"
            Else
                temWhere = "WHERE (((tblBatchStock.Stock)>0) And ((tblBatchStock.StoreID)=" & Val(cmbStores.BoundText) & "))"
            End If
        Else

            Exit Sub
        End If
        
        temOrderBY = "GROUP BY  tblItem.ItemId, tblItem.Display, tblItem.Code ORDER BY tblItem.Display"
        
        temSQL = temSelect & " " & temFrom & " " & temWhere & " " & temOrderBY
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        i = 0
        TotalValue = 0
        
        Dim pr As Double
        Dim pv As Double
        Dim sr As Double
        Dim sv As Double
        Dim tpv As Double
        Dim tsv As Double
        Dim s As Double
        
        If .RecordCount > 0 Then
            .MoveLast
            GridStock.Rows = .RecordCount + 1
            .MoveFirst
            While .EOF = False
                i = i + 1
                If Not IsNull(!Display) Then GridStock.TextMatrix(i, 0) = !Display
                If Not IsNull(!ItemStock) Then
                    s = !ItemStock
                    GridStock.TextMatrix(i, 1) = s
                Else
                    s = 0
                End If
                If Not IsNull(!ItemID) Then
                    sr = findSalePrice(!ItemID)
                    pr = findPurchasePrice(!ItemID)
                    sv = s * sr
                    pv = s * pr
                    tpv = tpv + pv
                    tsv = tsv + sv
                    GridStock.TextMatrix(i, 2) = Format(sr, "#,##0.00")
                    GridStock.TextMatrix(i, 3) = Format(sv, "#,##0.00")
                    GridStock.TextMatrix(i, 4) = Format(pr, "#,##0.00")
                    GridStock.TextMatrix(i, 5) = Format(pv, "#,##0.00")
                End If
                .MoveNext
            Wend
        End If
        
    End With
    
    With GridStock
        .Rows = .Rows + 1
        .Row = .Rows - 1
        
        .Col = 3
        .text = Format(tsv, "#,##0.00")
    
        .Col = 5
        .text = Format(tpv, "#,##0.00")
    
    End With
    
    
    Screen.MousePointer = vbDefault
    DoEvents
End Sub

Private Sub fillGridOld()
'    Screen.MousePointer = vbHourglass
'    DoEvents
'    With rsBatchStock
'        If .State = 1 Then .Close
'        temSelect = "SELECT tblItem.Display, tblItem.Code, sum(tblBatchStock.Stock) as ItemStock "
'        temFrom = "FROM ((tblBatch RIGHT JOIN tblBatchStock ON tblBatch.BatchID = tblBatchStock.BatchID) LEFT JOIN tblItem ON tblBatch.ItemID = tblItem.ItemID) LEFT JOIN tblCurrentPurchasePrice ON tblItem.ItemID = tblCurrentPurchasePrice.ItemID  "
'        If IsNumeric(cmbCategory.BoundText) = False And IsNumeric(cmbStores.BoundText) = False Then
'            temWhere = "WHERE (((tblBatchStock.Stock)>0))"
'        ElseIf IsNumeric(cmbCategory.BoundText) = True And IsNumeric(cmbStores.BoundText) = False Then
'            temWhere = "WHERE (((tblBatchStock.Stock)>0) And (tblItem.ItemCategoryID = " & Val(cmbCategory.BoundText) & " ) )"
'        ElseIf IsNumeric(cmbCategory.BoundText) = False And IsNumeric(cmbStores.BoundText) = True Then
'            temWhere = "WHERE (((tblBatchStock.Stock)>0) And ((tblBatchStock.StoreID)=" & Val(cmbStores.BoundText) & ") ) "
'        Else
'            temWhere = "WHERE (((tblBatchStock.Stock)>0) And ((tblBatchStock.StoreID)=" & Val(cmbStores.BoundText) & ") And (tblItem.ItemCategoryID = " & Val(cmbCategory.BoundText) & " ) )"
'        End If
'        If optItem.Value = True Then
'            temOrderBY = "GROUP BY tblItem.Display, tblItem.Code ORDER BY tblItem.Display"
'        ElseIf optValue.Value = True Then
'            temOrderBY = "GROUP BY tblItem.Display, tblItem.Code ORDER BY tblCurrentPurchasePrice.PPrice*tblBatchStock.Stock"
'        ElseIf optQuentity.Value = True Then
'            temOrderBY = "GROUP BY tblItem.Display, tblItem.Code ORDER BY tblBatchStock.Stock"
'        ElseIf optExpiary.Value = True Then
'            temOrderBY = "GROUP BY tblItem.Display, tblItem.Code ORDER BY tblBatch.DOE"
'        End If
'        If optDescending.Value = True Then temOrderBY = temOrderBY & " DESC"
'        temSQL = temSelect & " " & temFrom & " " & temWhere & " " & temOrderBY
'        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
'        i = 0
'        TotalValue = 0
'        If .RecordCount > 0 Then
'            .MoveLast
'            GridStock.Rows = .RecordCount + 1
'            .MoveFirst
'            While .EOF = False
'                i = i + 1
'                If Not IsNull(!Display) Then GridStock.TextMatrix(i, 0) = !Display
'
'                If Not IsNull(!ItemStock) Then GridStock.TextMatrix(i, 1) = !ItemStock
'
'                .MoveNext
'            Wend
'        End If
'    End With
'    lblValue.Caption = Format(TotalValue, "#,##0.00")
'    Screen.MousePointer = vbDefault
'    DoEvents
End Sub

Private Sub btnExcel_Click()
    GridToExcel GridStock, "Item Stock Report - " & cmbStores.text & " - " & cmbCategory.text, Format(Date, LongDateFormat)
End Sub

Private Sub btnProcess_Click()
    Call FormatGrid
    Call fillGrid

End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrintOld_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    RetVal = SelectForm(ReportPaperName, Me.hwnd)
    Select Case RetVal
        Case FORM_NOT_SELECTED   ' 0
            TemResponce = MsgBox("You have not selected a printer form to print, Please goto Preferances and Printing preferances to set a valid printer form.", vbExclamation, "Bill Not Printed")
        Case FORM_SELECTED   ' 1
            With dtrBatchStock
                Set .DataSource = rsBatchStock
                .Sections("Section4").Controls.Item("lblNaME").Caption = HospitalName
                .Sections("Section4").Controls.Item("lblContact").Caption = HospitalAddress
                temTopic = "Batch-vice Item Stocks"
                temSubTopic = "On " & Format(Date, LongDateFormat)
                .Sections("Section4").Controls.Item("lblTopic").Caption = temTopic
                .Sections("Section4").Controls.Item("lblSubTopic").Caption = temSubTopic
                .Caption = temTopic & " - " & temSubTopic
                .Sections("Section1").Controls.Item("txtItem").DataField = "Display"
                .Sections("Section1").Controls.Item("txtQuentity").DataField = "Stock"
                .Sections("Section1").Controls.Item("txtValue").DataField = "StockValue"
                .Sections("Section1").Controls.Item("txtDOE").DataField = "DOE"
                .Sections("Section1").Controls.Item("txtBatch").DataField = "Batch"
                
                .Sections("Section5").Controls.Item("funValue").DataField = "StockValue"
                .Show
            End With
        Case FORM_ADDED   ' 2
            TemResponce = MsgBox("New paper size added.", vbExclamation, "New Paper size")
    End Select
End Sub

Private Sub bttnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    GridPrint GridStock, ThisReportFormat, "Item Stock Report - " & cmbStores.text & " - " & cmbCategory.text, Format(Date, LongDateFormat)
    Printer.EndDoc
End Sub


Private Sub cmbItem_KeyDown(KeyCode As Integer, Shift As Integer)
        If KeyCode = vbKeyReturn Then
            btnProcess_Click
        ElseIf KeyCode = vbKeyEscape Then
            cmbItem.text = Empty
            searchItems
        Else
            searchItems
        End If
End Sub

Private Sub cmbStores_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbStores.text = Empty
    End If
End Sub

Private Sub Form_Load()
    Call fillCombos
    Call FormatGrid
    Call fillGrid
End Sub


Private Sub searchItems()
    With rsItem
        If .State = 1 Then .Close
        If Trim(cmbItem.text) = "" Then
            temSQL = "select tblItem.ItemID, tblItem.Display from tblItem order by display"
        Else
            temSQL = "select tblItem.ItemID, tblItem.Display from tblItem where display like '%" & cmbItem.text & "%' order by display"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    Set cmbItem.RowSource = rsItem
    cmbItem.BoundColumn = "ItemId"
    cmbItem.ListField = "Display"
End Sub

Private Sub fillCombos()
    With rsViewCategory
        If .State = 1 Then .Close
        temSQL = "Select * from tblItemCategory order by ItemCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbCategory
        Set .RowSource = rsViewCategory
        .ListField = "ItemCategory"
        .BoundColumn = "ItemCategoryID"
    End With
    With rsStores
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblStore order by store"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbStores
        Set .RowSource = rsStores
        .ListField = "Store"
        .BoundColumn = "StoreID"
        .BoundText = UserStoreId
    End With
    FillItems cmbItem
End Sub

Private Sub prepareCombos()
    If optAll.Value = True Then
        cmbCategory.text = Empty
        cmbCategory.Enabled = False
        cmbItem.text = Empty
        cmbItem.Enabled = False
    ElseIf optCat.Value = True Then
        cmbCategory.Enabled = True
        cmbItem.text = Empty
        cmbItem.Enabled = False
    ElseIf optItem.Value = True Then
        cmbCategory.text = Empty
        cmbCategory.Enabled = False
        cmbItem.Enabled = True
    End If
End Sub

Private Sub optAscending_Click()
    Call FormatGrid
    Call fillGrid
End Sub

Private Sub optDescending_Click()
    Call FormatGrid
    Call fillGrid
End Sub

Private Sub optExpiary_Click()
    Call FormatGrid
    Call fillGrid
End Sub

Private Sub optAll_Click()
    prepareCombos
End Sub

Private Sub optCat_Click()
    prepareCombos
End Sub

Private Sub optItem_Click()
    prepareCombos
End Sub

