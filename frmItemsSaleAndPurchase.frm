VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmItemSaleAndPurchase 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Sale And Purchase"
   ClientHeight    =   8790
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15270
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8790
   ScaleWidth      =   15270
   Begin btButtonEx.ButtonEx btnFill 
      Height          =   375
      Left            =   6840
      TabIndex        =   6
      Top             =   1080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Fill"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   14160
      TabIndex        =   7
      Top             =   8280
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcItem 
      Height          =   360
      Left            =   1560
      TabIndex        =   1
      Top             =   600
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFDate 
      Height          =   375
      Left            =   1560
      TabIndex        =   3
      Top             =   1080
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   22478851
      CurrentDate     =   39540
   End
   Begin MSComCtl2.DTPicker dtpTDate 
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   1080
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   22478851
      CurrentDate     =   39540
   End
   Begin MSDataListLib.DataCombo cmbStore 
      Height          =   360
      Left            =   1560
      TabIndex        =   8
      Top             =   120
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridDetail 
      Height          =   6495
      Left            =   120
      TabIndex        =   10
      Top             =   1680
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   11456
      _Version        =   393216
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   8160
      TabIndex        =   11
      Top             =   1080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label15 
      Caption         =   "&Store"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label3 
      Caption         =   "From :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   2175
   End
   Begin VB.Label Label7 
      Caption         =   "To :"
      Height          =   255
      Left            =   4080
      TabIndex        =   4
      Top             =   1080
      Width           =   2175
   End
   Begin VB.Label Label2 
      Caption         =   "&Item"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   1095
   End
End
Attribute VB_Name = "frmItemSaleAndPurchase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    
    Dim CsetPrinter As New cSetDfltPrinter
    
    Dim NewItem As New Item
    
    Dim rsSPrice As New ADODB.Recordset
    Dim rsPPrice As New ADODB.Recordset
    Dim rsCC As New ADODB.Recordset
    Dim rsItem As New ADODB.Recordset
    Dim rsItemCategory As New ADODB.Recordset
    Dim rsCode As New ADODB.Recordset
    Dim rsDistributor As New ADODB.Recordset
    
    Dim rsTemOrder As New ADODB.Recordset
    Dim rsTemPrice As New ADODB.Recordset
    Dim rsTemDistributor As New ADODB.Recordset
    Dim rsTemStore As New ADODB.Recordset
    Dim rsTemOrderBill As New ADODB.Recordset
    Dim rsTemDistributorOrder As New ADODB.Recordset
    Dim rsTemRefill As New ADODB.Recordset
    Dim rsTemRefillBill As New ADODB.Recordset
    Dim rsStore As New ADODB.Recordset

    Dim qty As Double
    


Private Sub ListAllItems()
    With rsItem
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblitem order by display"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcItem
        Set .RowSource = rsItem
        .ListField = "display"
        .BoundColumn = "ItemID"
    End With
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridDetail, "Purchase, Sale and Adjustment Report of " & dtcItem.text, "From " & Format(dtpFDate.Value, "dd MMMM yyyy") & " to " & Format(dtpTDate.Value, "dd MMMM yyyy")
End Sub

Private Sub btnFill_Click()
    If IsNumeric(cmbStore.BoundText) = False Then
        MsgBox "Please select a store"
        Exit Sub
    End If
    If IsNumeric(dtcItem.BoundText) = False Then
        MsgBox "Please select an Item"
        Exit Sub
    End If
    
    Call FillDetails
    
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()
    With rsItem
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblitem order by display"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcItem
        Set .RowSource = rsItem
        .ListField = "display"
        .BoundColumn = "ItemID"
    End With
    With rsStore
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblStore order by Store"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With cmbStore
        Set .RowSource = rsStore
        .ListField = "Store"
        .BoundColumn = "StoreId"
    End With
    
End Sub
    

    
Private Sub FillDetails()
    If Not IsNumeric(dtcItem.BoundText) Then Exit Sub
    Call FormatGrids
    qty = 0
    Call fillPurchase
    Call fillPurchaseReturn
    Call fillSale
    Call fillSaleReturn
    With gridDetail
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 1
        .text = "Net Amount"
        .Col = 4
        .text = qty
    End With
End Sub



Private Sub fillSale()

    With gridDetail
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 1
        .text = "Sale/Issue"
    End With

    With rsTemStore
        If .State = 1 Then .Close

        temSQL = "SELECT      dbo.tblSaleBill.SaleBillID, dbo.tblSale.SaleID, dbo.tblSaleBill.Date, dbo.tblSale.Amount, dbo.tblSaleBill.Time " & _
                    "FROM          dbo.tblSale LEFT OUTER JOIN " & _
                    "dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID " & _
                    "WHERE (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTDate.Value, "dd MMMM yyyy") & "', 102)) AND (dbo.tblSaleBill.StoreID = " & Val(cmbStore.BoundText) & ")  AND (dbo.tblSale.ItemID = " & Val(dtcItem.BoundText) & ")" & _
                    "ORDER BY dbo.tblSaleBill.SaleBillID"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            With gridDetail
            
            
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .Col = 0
                .CellAlignment = 1
                .text = rsTemStore!SaleId
                .Col = 1
                .text = rsTemStore!SaleBillID
                .Col = 2
                .text = Format(rsTemStore!Date, "dd MMMM yyyy")
                .Col = 3
                .text = Format(rsTemStore!Time, "hh mm ampm")
                .Col = 4
                .text = rsTemStore!Amount
                .Col = 5
                .text = ""
                .Col = 6
                .text = transactionType.SaleTransaction
                qty = qty - rsTemStore!Amount
            End With
            .MoveNext
        Wend
        .Close
    End With
    
End Sub

Private Sub fillSaleReturn()
    With gridDetail
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 1
        .text = "Sale/Issue Returns and Cancellations"
    End With

    With rsTemStore
        If .State = 1 Then .Close
        temSQL = "SELECT TOP 100 PERCENT dbo.tblReturn.ReturnID, dbo.tblReturnBill.ReturnBillID, dbo.tblReturn.ItemID, dbo.tblReturn.StoreID, dbo.tblReturnBill.Date, dbo.tblReturnBill.Time, dbo.tblReturn.Amount " & _
                    "FROM dbo.tblReturn LEFT OUTER JOIN dbo.tblReturnBill ON dbo.tblReturn.ReturnBillID = dbo.tblReturnBill.ReturnBillID " & _
                    "WHERE (dbo.tblReturnBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTDate.Value, "dd MMMM yyyy") & "', 102)) AND  (dbo.tblReturnBill.StoreID = " & Val(cmbStore.BoundText) & ")  AND  (dbo.tblReturn.ItemID = " & Val(dtcItem.BoundText) & ") " & _
                    "ORDER BY dbo.tblReturnBill.ReturnBillID"

        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            With gridDetail
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .Col = 0
                .CellAlignment = 1
                .text = rsTemStore!ReturnID
                .Col = 1
                .text = rsTemStore!ReturnBillID
                .Col = 2
                .text = Format(rsTemStore!Date, "dd MMMM yyyy")
                .Col = 3
                .text = Format(rsTemStore!Time, "hh mm ampm")
                .Col = 4
                If IsNull(rsTemStore!Amount) = False Then
                    .text = rsTemStore!Amount
                    qty = qty + rsTemStore!Amount
                End If
                .Col = 5
                .text = ""
                .Col = 6
                .text = transactionType.saleReturnTransaction
                
            End With
            .MoveNext
        Wend
        .Close
    End With
    
End Sub



Private Sub fillPurchase()

    With gridDetail
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 1
        .text = "Purchase"
    End With

    With rsTemStore
        If .State = 1 Then .Close

        temSQL = "SELECT      dbo.tblSaleBill.SaleBillID, dbo.tblSale.SaleID, dbo.tblSaleBill.Date, dbo.tblSale.Amount, dbo.tblSaleBill.Time " & _
                    "FROM          dbo.tblSale LEFT OUTER JOIN " & _
                    "dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID " & _
                    "WHERE (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTDate.Value, "dd MMMM yyyy") & "', 102)) AND (dbo.tblSaleBill.StoreID = " & Val(cmbStore.BoundText) & ")  AND (dbo.tblSale.ItemID = " & Val(dtcItem.BoundText) & ")" & _
                    "ORDER BY dbo.tblSaleBill.SaleBillID"
                    
        temSQL = "SELECT dbo.tblRefill.RefillID, dbo.tblRefillBill.RefillBillID, dbo.tblRefillBill.StoreID, dbo.tblRefillBill.InvoiceNo, dbo.tblRefillBill.Date, dbo.tblRefillBill.Time, dbo.tblRefill.ItemID, dbo.tblRefill.Amount " & _
                    "FROM dbo.tblRefill LEFT OUTER JOIN dbo.tblRefillBill ON dbo.tblRefill.RefillBillID = dbo.tblRefillBill.RefillBillID " & _
                    "WHERE (dbo.tblRefillBill.StoreID = " & Val(cmbStore.BoundText) & ") AND (dbo.tblRefillBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTDate.Value, "dd MMMM yyyy") & "', 102)) AND (dbo.tblRefill.ItemID = " & Val(dtcItem.BoundText) & ") " & _
                    "ORDER BY dbo.tblRefill.RefillID"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            With gridDetail
            
            
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .Col = 0
                .CellAlignment = 1
                .text = rsTemStore!RefillID
                .Col = 1
                .text = rsTemStore!RefillBillID
                .Col = 2
                .text = Format(rsTemStore!Date, "dd MMMM yyyy")
                .Col = 3
                .text = Format(rsTemStore!Time, "hh mm ampm")
                .Col = 4
                .text = rsTemStore!Amount
                .Col = 5
                If IsNull(rsTemStore!InvoiceNo) = False Then
                    .text = rsTemStore!InvoiceNo
                End If
                .Col = 6
                .text = transactionType.PurchaseTransaction
                qty = qty + rsTemStore!Amount
            End With
            .MoveNext
        Wend
        .Close
    End With
    
End Sub

Private Sub fillPurchaseReturn()
    With gridDetail
        .Rows = .Rows + 2
        .Row = .Rows - 1
        .Col = 1
        .text = "Purchase Returns and Cancellations"
    End With

    With rsTemStore
        If .State = 1 Then .Close
        temSQL = "SELECT dbo.tblRefill.RefillID, dbo.tblRefillBill.RefillBillID, dbo.tblRefillBill.StoreID, dbo.tblRefillBill.InvoiceNo, dbo.tblRefill.ReturnedDate, dbo.tblRefill.ReturnedTime, dbo.tblRefill.ItemID, dbo.tblRefill.ReturnedAmount, dbo.tblRefill.Returned " & _
                "FROM dbo.tblRefill LEFT OUTER JOIN dbo.tblRefillBill ON dbo.tblRefill.RefillBillID = dbo.tblRefillBill.RefillBillID " & _
                "WHERE (dbo.tblRefillBill.StoreID = " & Val(cmbStore.BoundText) & ") AND (dbo.tblRefill.ItemID = " & Val(dtcItem.BoundText) & ") AND (dbo.tblRefill.Returned = 1) AND (dbo.tblRefill.ReturnedDate BETWEEN CONVERT(DATETIME, '" & Format(dtpFDate.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTDate.Value, "dd MMMM yyyy") & "', 102)) " & _
                "ORDER BY dbo.tblRefill.ReturnedTime"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            With gridDetail
                .Rows = .Rows + 1
                .Row = .Rows - 1
                .Col = 0
                .CellAlignment = 1
                .text = rsTemStore!RefillID
                .Col = 1
                .text = rsTemStore!RefillBillID
                .Col = 2
                .text = Format(rsTemStore!ReturnedDate, "dd MMMM yyyy")
                .Col = 3
                .text = Format(rsTemStore!ReturnedTime, "hh mm ampm")
                .Col = 4
                If IsNull(rsTemStore!ReturnedAmount) = False Then
                    .text = rsTemStore!ReturnedAmount
                    qty = qty - rsTemStore!ReturnedAmount
                End If
                .Col = 5
                .text = ""
                .Col = 6
                .text = transactionType.PurchaseReturnTransaction
                
            End With
            .MoveNext
        Wend
        .Close
    End With
    
End Sub


Private Sub Form_Load()
    dtpFDate.Value = DateSerial(Year(Date), Month(Date), 1)
    dtpTDate.Value = Date
    fillCombos
    FormatGrids
    GetCommonSettings Me
    cmbStore.BoundText = UserStoreId
End Sub

Private Sub FormatGrids()
    
    With gridDetail
        .Cols = 7
        .Rows = 1
        .FixedCols = 0
        .ColWidth(0) = 0
        .ColWidth(1) = 1800
        .ColWidth(2) = 3200
        .ColWidth(3) = 1800
        .ColWidth(4) = 2200
        .ColWidth(5) = 3000
        .ColWidth(6) = 0
        Dim i As Long
        For i = 0 To .Cols - 1
            .Col = i
            .CellAlignment = 4
            Select Case i
                Case 0: .text = "Bill Item Id"
                Case 1: .text = "Bill Id"
                Case 2: .text = "Date"
                Case 3: .text = "Time"
                Case 4: .text = "Quentity"
                Case 5: .text = "Comments"
                Case 6: .text = "Type"
            End Select
        Next i
    End With
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveCommonSettings Me
End Sub
