VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Divudi Software Solutions - Pharmacy Management Information System"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9975
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmLogin.frx":29C12
   ScaleHeight     =   4455
   ScaleWidth      =   9975
   StartUpPosition =   2  'CenterScreen
   Begin btButtonEx.ButtonEx cmdCancel 
      Height          =   375
      Left            =   8040
      TabIndex        =   3
      Top             =   3960
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Cancel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcDepartment 
      Height          =   360
      Left            =   6120
      TabIndex        =   4
      Top             =   2520
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtTemUsername 
      Height          =   360
      Left            =   8400
      TabIndex        =   7
      Top             =   3960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtUserName 
      Height          =   375
      Left            =   6120
      TabIndex        =   0
      Top             =   3000
      Width           =   3615
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   6120
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   3480
      Width           =   3645
   End
   Begin btButtonEx.ButtonEx cmdOK 
      Height          =   375
      Left            =   6120
      TabIndex        =   2
      Top             =   3960
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Login"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnServer 
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   3960
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Server"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "&Department"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   8
      Top             =   2520
      Width           =   1815
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "&Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   6
      Top             =   3480
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "&User Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   5
      Top             =   3000
      Width           =   1815
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    Dim SuppliedWord As String
    
    Dim rsStaff As New ADODB.Recordset
    Dim rsStore As New ADODB.Recordset
    Dim rsAds As New ADODB.Recordset
    Dim rsHospital As New ADODB.Recordset
    Dim temsql As String
    Dim constr As String
    Dim TemUserPassward As String
    
Private Sub btnServer_Click()
    frmServer.Show 1
End Sub

Private Sub cmdCancel_Click()
    End
End Sub

Private Sub cmdOK_Click()
    Dim TemResponce As Byte
    Dim UserNameFound As Boolean
    UserNameFound = False
    
    TemUserPassward = txtPassword.text
    If Trim(txtUserName.text) = "" Then
        TemResponce = MsgBox("You have not entered a username", vbCritical, "Username")
        txtUserName.SetFocus
        Exit Sub
    End If
    If Trim(txtPassword.text) = "" Then
        TemResponce = MsgBox("You have not entered a password", vbCritical, "Password")
        txtPassword.SetFocus
        Exit Sub
    End If
    If Not IsNumeric(dtcDepartment.BoundText) Then
        TemResponce = MsgBox("You have not selected a department", vbCritical, "Department")
        dtcDepartment.SetFocus
        Exit Sub
    End If
    With rsStaff
        If .State = 1 Then .Close
        temsql = "Select tblstaff.* from tblstaff where tblstaff.IsAUser = 1"
        .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount < 1 Then Exit Sub
        .MoveFirst
        Do While Not .EOF
            txtTemUsername.text = DecreptedWord(!UserName)
            If UCase(txtUserName.text) = UCase(txtTemUsername.text) Then
                UserNameFound = True
                If txtPassword.text = DecreptedWord(!Password) Then
'                If CheckLogin = True Then
'                        If !logged = True Then
'                            TemResponce = MsgBox("There is a user currently logged using the same username. Please check on that. If no one is logged from the user name, it may be due to a sudden breakdown of the program, for example, after a power failure. In this case, advice the owner to and Reset All Users Login", vbCritical, "User Logged")
'                            txtUserName.SetFocus
'                            SendKeys "{home}+{end}"
'                            Exit Sub
'                        Else
'                            !logged = True
'                            .Update
'                        End If
'                End If
                    UserName = UCase(txtUserName.text)
                    userid = !StaffID

                    If Not IsNull(!AuthorityID) Then
                        UserAuthority = !AuthorityID
                    Else
                        UserAuthority = 0
                    End If
                    Exit Do
                Else
                    TemResponce = MsgBox("The username and password you entered are not matching. Please try again", vbCritical, "Wrong Username and Password")
                    txtUserName.SetFocus
                    On Error Resume Next: SendKeys "{home}+{end}"
                    Exit Sub
                End If
            Else
            End If
            .MoveNext
        Loop
        .Close
        If UserNameFound = False Then
            TemResponce = MsgBox("There is no such  a username, Please try again", vbCritical, "Username")
            txtUserName.SetFocus
            SendKeys "{home}+{end}"
            Exit Sub
        End If
        End With
        
        UserStore = dtcDepartment.text
        UserStoreId = dtcDepartment.BoundText
        
        If userCanLogToStore(userid, UserStoreId) = False Then
            MsgBox "You can not log to " & UserStore & ". Please contact the administrator to add you to log on to this store under Menu>Edit>Hospital>Staff>Edit>"
            On Error Resume Next
            dtcDepartment.SetFocus
            Exit Sub
        End If
        
        Unload Me
        MDIMain.Show
        MDIMain.Caption = MDIMain.Caption & " - " & UserStore & " - " & UserName

End Sub


Private Sub setSecKey()
    Dim mySec As New clsSecurity
    ProgramVariable.SecurityKey = mySec.Decode(")?C{.�", "Buddhika")
End Sub


Private Sub Form_Load()

    setSecKey

    On Error GoTo eh
    Dim TemResponce As Byte
    Dim WillExpire As Boolean
    Dim ExpiaryDate As Date
    WillExpire = False
    ExpiaryDate = #3/31/2033#
    If WillExpire = True And ExpiaryDate < Date Then
        TemResponce = MsgBox("The Program has expiared. Please contact www.divudi.com for Assistant", vbCritical, "Expired")
        End
    End If
    Dim tempath As String
    Call LoadPreferances
    
    While connectToDatabase = False
        MsgBox "Please select the correct settings for the SQL Server 2005"
        frmServer.Show 1
    Wend
    
    Call LoadInstitutionDetails
    Call WriteDailyDetails
    Call FillCombo
    Call GetAds
    dtcDepartment.text = GetSetting(App.EXEName, "Options", "dtcDepartment", "")
    
    Dim myAct As New clsActivation
    
    
    If myAct.isActivated = False Then
        frmActivate.Show
        frmActivate.ZOrder 0
        Unload Me
        Exit Sub
    End If

    
    Exit Sub
    
eh:

    MsgBox "Error. Unable to connect to server." & vbNewLine & Err.Description
    End
End Sub

Private Function connectToDatabase() As Boolean
    On Error GoTo eh:
    connectToDatabase = False
    constr = "Provider=MSDataShape.1;Persist Security Info=True;Data Source=" & Server & "" & SQLServer & ";User ID=sa;Password=" & ServerPassword & ";Initial Catalog=" & ServerDatabase & ";Data Provider=SQLOLEDB.1"
             'Provider=MSDataShape.1;Persist Security Info=True;Data Source=SVR                            ;User ID=sa;Password=coop                  ;Initial Catalog=coop;                  Data Provider=SQLOLEDB.1
    
    If cnnStores.State = 1 Then cnnStores.Close
    cnnStores.Open constr
    Dataenvironment1.Connection1.ConnectionString = constr
    connectToDatabase = True
    Exit Function
eh:
    connectToDatabase = connectToDatabaseByDe
    
End Function


Private Function connectToDatabaseByDe() As Boolean
    On Error GoTo eh:
    connectToDatabaseByDe = False
    constr = Dataenvironment1.Connection1.ConnectionString
    If cnnStores.State = 1 Then cnnStores.Close
    cnnStores.Open constr
    Dataenvironment1.Connection1.ConnectionString = constr
    connectToDatabaseByDe = True
    Exit Function
eh:
    connectToDatabaseByDe = False
    
End Function


Private Sub FillCombo()
    With rsStore
        If .State = 1 Then .Close
        temsql = "SELECT * from tblStore order by store"
        .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcDepartment
        Set .RowSource = rsStore
        .ListField = "Store"
        .BoundColumn = "StoreID"
    End With
End Sub

Private Sub WriteDailyDetails()
Dim rsDailyIssue As New ADODB.Recordset
Dim rsIssue As New ADODB.Recordset


End Sub

Private Sub GetAds()
With rsAds
    
    If .State = 1 Then .Close
    temsql = "select * from tblAds"
    .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
    If .RecordCount > 0 Then
        Dim TemNum As Long
        Randomize
        TemNum = Round((Rnd * .RecordCount - 1), 0) + 1
        If TemNum < 1 Then TemNum = 0
        If TemNum > .RecordCount Then TemNum = .RecordCount
        If .State = 1 Then .Close
        temsql = "select * from tblAds where ID = " & TemNum
        .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount >= 1 Then
            LongAd = !AdLong
            ShortAd = !AdShort
        End If
    End If
    .Close
End With
End Sub


Private Sub Form_Unload(Cancel As Integer)
    SaveSetting App.EXEName, "Options", "dtcDepartment", dtcDepartment.text
End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And txtUserName.text <> "" Then cmdOK_Click: Exit Sub
    If KeyAscii = 13 And txtUserName.text = "" Then txtUserName.SetFocus: Exit Sub
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then txtPassword.SetFocus
End Sub

Private Sub LoadPreferances()


    Server = DecreptedWord(GetSetting(App.EXEName, Me.Name, "Server", ""))
    SQLServer = DecreptedWord(GetSetting(App.EXEName, Me.Name, "SQLServer", ""))
    ServerDatabase = DecreptedWord(GetSetting(App.EXEName, Me.Name, "ServerDatabase", ""))
    ServerUserName = DecreptedWord(GetSetting(App.EXEName, Me.Name, "ServerUserName", ""))
    ServerPassword = DecreptedWord(GetSetting(App.EXEName, Me.Name, "ServerPassword", ""))


    Database = GetSetting(App.EXEName, "Options", "Database", App.Path & "\eStore.mdb")
    DoNotAllowExpireConsumption = GetSetting(App.EXEName, "Options", "DoNotAllowExpireConsumption", True)
    DoNotAllowExpireSale = GetSetting(App.EXEName, "Options", "DoNotAllowExpireSale", True)
    DoNotAllowExpireTransfer = GetSetting(App.EXEName, "Options", "DoNotAllowExpireTransfer", True)
    LongDateFormat = GetSetting(App.EXEName, "Options", "LongDateFormat", "dd MM yyyy")
    ShortDateFormat = GetSetting(App.EXEName, "Options", "ShortDateFormat", "dd MM yy")
    BillPrinterName = GetSetting(App.EXEName, "Options", "BillPrinterName", "")
    BillPaperName = GetSetting(App.EXEName, "Options", "BillPaperName", "")
    PrescreptionPrinterName = GetSetting(App.EXEName, "Options", "PrescreptionPrinterName", "")
    ReportPrinterName = GetSetting(App.EXEName, "Options", "ReportPrinterName", "")
    ReportPaperName = GetSetting(App.EXEName, "Options", "ReportPaperName", "")
    PrescreptionPaperName = GetSetting(App.EXEName, "Options", "PrescreptionPaperName", "")
    PrintingOnBlankPaper = GetSetting(App.EXEName, "Options", "PrintingOnBlankPaper", True)
    PrintingOnPrintedPaper = GetSetting(App.EXEName, "Options", "PrintingOnPrintedPaper", False)
    HighRate = GetSetting(App.EXEName, "Options", "HighRate", 1)


'    IncomeDeflation = Val(GetSetting(App.EXEName, "Options", "IncomeDeflation", "3"))
'    ColourScheme = Val(GetSetting(App.EXEName, "Options", "ColourScheme", "1"))
'    AdvanceBookingDays = Val(GetSetting(App.EXEName, "Options", "AdvanceBookingDays", "3"))
'    PrintingOnBlankPaper = GetSetting(App.EXEName, "Options", "PrintingOnBlankPaper", False)
'    PrintingOnPrintedPaper = GetSetting(App.EXEName, "Options", "PrintingOnPrintedPaper", True)
'    AskBeforeAdding = GetSetting(App.EXEName, "Options", "AskBeforeAdding", True)
'    AgentEssential = GetSetting(App.EXEName, "Options", "agentessential", True)
'    AllowNameChange = GetSetting(App.EXEName, "Options", "AllowNameChange", True)
'    AddForeignerSuffix = GetSetting(App.EXEName, "Options", "AddForeignerSuffix", False)
'    AutomaticCapitalization = GetSetting(App.EXEName, "Options", "AutomaticCapitalization", True)
'    AgentNameForCreditBookings = GetSetting(App.EXEName, "Options", "AgentNameForCreditBookings", True)
'    NoAllNames = GetSetting(App.EXEName, "Options", "NoAllNames", False)
'    SurnameFirst = GetSetting(App.EXEName, "Options", "SurnameFirst", True)
'    CanSelectAgent = GetSetting(App.EXEName, "Options", "CanSelectAgent", True)
'    ChangeToCash = GetSetting(App.EXEName, "Options", "ChangeToCash", False)
'    AllowReprint = GetSetting(App.EXEName, "Options", "AllowReprint", True)
'    BackUpPath = GetSetting(App.EXEName, "Options", "BackUpPath", App.Path)
'    PayToDoctor = GetSetting(App.EXEName, "Options", "PayToDoctor", True)
'    AllowAbsent = GetSetting(App.EXEName, "Options", "AllowAbsent", True)
'    AfterAddSpeciality = GetSetting(App.EXEName, "Options", "AfterAddSpeciality", False)
'    AfterAddConsultant = GetSetting(App.EXEName, "Options", "AfterAddConsultant", False)
'    AfterAddDates = GetSetting(App.EXEName, "Options", "AfterAddDates", True)
'    AfterAddPatient = GetSetting(App.EXEName, "Options", "AfterAddPatient", False)
'    HospitalDetails = GetSetting(App.EXEName, "Options", "HospitalDetails", True)
'    DisplayPrintChkBox = GetSetting(App.EXEName, "Options", "DisplayPrintChkBox", True)
'    PaymentAgent = Val(GetSetting(App.EXEName, "Options", "PaymentAgent", 1))
'    PaymentCredit = Val(GetSetting(App.EXEName, "Options", "PaymentCredit", 1))
'    PaymentCash = Val(GetSetting(App.EXEName, "Options", "PaymentCash", 1))
'    AgentCashOnly = GetSetting(App.EXEName, "Options", "AgentCashOnly", True)
'    EnglishDateFormat = GetSetting(App.EXEName, "Options", "EnglishDateFormat", True)
'    PartialRepayments = GetSetting(App.EXEName, "Options", "PartialRepayments", False)
'    DetailedCount = GetSetting(App.EXEName, "Options", "DetailedCount", True)
'    OnePrintForAgents = GetSetting(App.EXEName, "Options", "OnePrintForAgents", False)
'    CheckLogin = GetSetting(App.EXEName, "Options", "CheckLogin", False)
'    If EnglishDateFormat = True Then
'        DefaultLongDate = "DD MMMM YYYY"
'        DefaultShortDate = "dd mm yy"
'    Else
'        DefaultLongDate = "YYYY MMMM DD"
'        DefaultShortDate = "yy mm dd"
'    End If
End Sub

Private Sub LoadInstitutionDetails()
    With rsHospital
        If .State = 1 Then .Close
        temsql = "SELECT * from tblInstitutionDetail"
        .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount < 1 Then Exit Sub
        SuppliedWord = !InstitutionName
        HospitalName = DecreptedWord(SuppliedWord)
        SuppliedWord = !InstitutionDescription
        HospitalDescreption = DecreptedWord(SuppliedWord)
        SuppliedWord = !institutionAddress
        HospitalAddress = DecreptedWord(SuppliedWord)
        If .State = 1 Then .Close
    End With
End Sub
