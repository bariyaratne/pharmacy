VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmPurchaseAndSaleDailySummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "F15 - Daily Summery Report - Purchase & Sale"
   ClientHeight    =   10125
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   16110
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10125
   ScaleWidth      =   16110
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   495
      Left            =   7680
      TabIndex        =   6
      Top             =   120
      Width           =   2415
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   1920
      TabIndex        =   5
      Top             =   120
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   113115139
      CurrentDate     =   41386
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   3
      Top             =   9480
      Width           =   1215
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   14640
      TabIndex        =   2
      Top             =   9480
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   9480
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridSum 
      Height          =   8295
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   15615
      _ExtentX        =   27543
      _ExtentY        =   14631
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   2775
   End
End
Attribute VB_Name = "frmPurchaseAndSaleDailySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim saleCats As New Collection


Private Sub btnExcel_Click()
    GridToExcel gridSum, "Daily Purchase and Sale Summery - " & UserStore, Format(Date, LongDateFormat)
End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    GridPrint gridSum, ThisReportFormat, "F15 - " & UserStore, Format(Date, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    DisplayDetails
End Sub

Private Sub Form_Load()
    getSettings
    dtpDate.Value = Date
    DisplayDetails
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    Set saleCats = findAllSaleCategories
End Sub



Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    saveSettings
End Sub

Private Sub DisplayDetails()
    Dim temCat As SaleCategory
    Dim Col As Integer
    Dim Row As Integer
    Dim temRow As Integer
    
    
    Dim todayTotalSale As Double
    Dim todayTotalPurchase As Double
    Dim ydayTotalSale As Double
    Dim ydayTotalPurchase As Double
    
    
    With gridSum
        Col = 1
        .Rows = 18 + saleCats.Count
        .Cols = 4
        
        .TextMatrix(0, 1) = "Upto Day Before"
        .TextMatrix(0, 2) = "This Date"
        .TextMatrix(0, 3) = "Upto today"

        .TextMatrix(1, 0) = "Received From Units"
        .TextMatrix(2, 0) = "Purchases"
        .TextMatrix(3, 0) = "Total"
        
        .TextMatrix(4, 0) = "Price Change"
        .TextMatrix(5, 0) = "Transfer"
        
        .TextMatrix(6, 0) = "Total"
        
        .TextMatrix(7, 0) = "Opening Balance"
        .TextMatrix(8, 0) = "Grand Total"
        
        .TextMatrix(9, 0) = "Less"

        temRow = 11
        For Each temCat In saleCats
            .TextMatrix(temRow, 0) = temCat.SaleCategory
            temRow = temRow + 1
        Next
        
        .TextMatrix(temRow, 0) = "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Returns"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Discard"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 0) = "Total"


    Dim calDate As Date
    Dim yearStart As Date
    
    yearStart = DateSerial(Year(dtpDate.Value), 1, 1)
    
    calDate = dtpDate.Value


'    *********************************************************

        .TextMatrix(1, 1) = Format(findGrossPurchase(yearStart, calDate - 1, UserStoreId, False, True) - findPurchaseReturnCancellation(calDate - 1, calDate - 1, UserStoreId, False, True), "0.00")
        .TextMatrix(2, 1) = Format(findGrossPurchase(yearStart, calDate - 1, UserStoreId, True, False) - findPurchaseReturnCancellation(calDate - 1, calDate - 1, UserStoreId, True, False), "0.00")
        ydayTotalPurchase = Val(.TextMatrix(1, 2)) + Val(.TextMatrix(2, 2))
        .TextMatrix(3, 1) = Format(ydayTotalPurchase, "0.00")
        
        .TextMatrix(4, 1) = ""
        .TextMatrix(5, 1) = ""
        
        .TextMatrix(6, 1) = "" '
        
        .TextMatrix(7, 1) = "" ' "Opening Balance"
        .TextMatrix(8, 1) = Format(ydayTotalPurchase, "0.00")
        
        .TextMatrix(9, 1) = "Less"

        temRow = 11
        
        Dim temTot As Double
        Dim temVal As Double
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(yearStart, calDate - 1, temCat.SaleCategoryID, UserStoreId)
            temTot = temTot + temVal
            .TextMatrix(temRow, 1) = Format(temVal, "0.00")
            temRow = temRow + 1
        Next
        
        ydayTotalSale = temTot
        
        .TextMatrix(temRow, 1) = Format(temTot, "0.00")
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = ""
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" ' "Remainng Stock Value"
        temRow = temRow + 1
        .TextMatrix(temRow, 1) = "" '"Total"

    ' **********************************************


'    *********************************************************

        .TextMatrix(1, 2) = Format(findGrossPurchase(calDate, calDate, UserStoreId, False, True) - findPurchaseReturnCancellation(calDate, calDate, UserStoreId, False, True), "0.00")
        .TextMatrix(2, 2) = Format(findGrossPurchase(calDate, calDate, UserStoreId, True, False) - findPurchaseReturnCancellation(calDate, calDate, UserStoreId, False, True), "0.00")
        .TextMatrix(3, 2) = Format(Val(.TextMatrix(1, 1)) + Val(.TextMatrix(2, 1)), "0.00")
        
        .TextMatrix(4, 2) = ""
        .TextMatrix(5, 2) = ""
        
        .TextMatrix(6, 2) = "" '
        
        .TextMatrix(7, 2) = "" ' "Opening Balance"
        .TextMatrix(8, 2) = "" ' "Grand Total"
        
        .TextMatrix(9, 2) = "Less"

        temRow = 11
        
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(calDate, calDate, temCat.SaleCategoryID, UserStoreId)
            temTot = temTot + temVal
            .TextMatrix(temRow, 2) = Format(temVal, "0.00")
            temRow = temRow + 1
        Next
        
        
        .TextMatrix(temRow, 2) = Format(temTot, "0.00")
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = "" 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = ""
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = "" ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = Format(findStockValue(UserStoreId, 0), "0.00")  ' "Remainng Stock Value", "0.00")
        temRow = temRow + 1
        .TextMatrix(temRow, 2) = "" '"Total"

    ' **********************************************
    
    
    
    
    
'    *********************************************************

        Dim startDate As Date

        startDate = DateSerial(Year(calDate), 1, 1)

        .TextMatrix(1, 3) = Format(findGrossPurchase(startDate, calDate, UserStoreId, False, True) - findPurchaseReturnCancellation(startDate, calDate, UserStoreId, False, True), "0.00")
        .TextMatrix(2, 3) = Format(findGrossPurchase(startDate, calDate, UserStoreId, True, False) - findPurchaseReturnCancellation(startDate, calDate, UserStoreId, True, False), "0.00")
        .TextMatrix(3, 3) = Format(Val(.TextMatrix(1, 1)) + Val(.TextMatrix(2, 1)), "0.00")
        
        .TextMatrix(4, 3) = ""
        .TextMatrix(5, 3) = ""
        
        .TextMatrix(6, 3) = "" '
        
        .TextMatrix(7, 3) = "" ' "Opening Balance"
        .TextMatrix(8, 3) = "" ' "Grand Total"
        
        .TextMatrix(9, 3) = "Less"

        temRow = 11
        
        
        temTot = 0
        For Each temCat In saleCats
            temVal = findNetSale(startDate, calDate, temCat.SaleCategoryID, UserStoreId)
            temTot = temTot + temVal
            .TextMatrix(temRow, 3) = Format(temVal, "0.00")
            temRow = temRow + 1
        Next
        
        
        .TextMatrix(temRow, 3) = Format(temTot, "0.00")
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = "" 'to do
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = ""
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = "" ' "Total"
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = Format(findStockValue(UserStoreId, 0), "0.00")  ' "Remainng Stock Value", "0.00")
        temRow = temRow + 1
        .TextMatrix(temRow, 3) = "" '"Total"

    ' **********************************************
    
    End With
End Sub

