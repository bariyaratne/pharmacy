VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmSaleBillsForPrint 
   Caption         =   "Sale Bills"
   ClientHeight    =   7470
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   10125
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7470
   ScaleWidth      =   10125
   Begin VB.CommandButton btnBillPrint 
      Caption         =   "Print"
      Height          =   375
      Left            =   8280
      TabIndex        =   14
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton btnPreview 
      Caption         =   "Preview"
      Height          =   375
      Left            =   6960
      TabIndex        =   13
      Top             =   1200
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid gridBills 
      Height          =   5055
      Left            =   120
      TabIndex        =   12
      Top             =   1680
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   8916
      _Version        =   393216
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "C&lose"
      Height          =   495
      Left            =   8760
      TabIndex        =   11
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnExcel 
      Caption         =   "&Excel"
      Height          =   495
      Left            =   1560
      TabIndex        =   10
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnPrint 
      Caption         =   "&Print"
      Height          =   495
      Left            =   240
      TabIndex        =   9
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton btnProcess 
      Caption         =   "Process"
      Height          =   375
      Left            =   6960
      TabIndex        =   8
      Top             =   240
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   840
      TabIndex        =   6
      Top             =   1200
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   108068867
      CurrentDate     =   41272
   End
   Begin MSDataListLib.DataCombo cmbDept 
      Height          =   360
      Left            =   2160
      TabIndex        =   1
      Top             =   240
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCat 
      Height          =   360
      Left            =   2160
      TabIndex        =   3
      Top             =   720
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   4440
      TabIndex        =   7
      Top             =   1200
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   108068867
      CurrentDate     =   41272
   End
   Begin VB.Label Label4 
      Caption         =   "To"
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "From"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Sale Category"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Department"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "frmSaleBillsForPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnBillPrint_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            printSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            printSaleBillA4 id, Me.hwnd
        End If
    End If
    
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub fillCombos()
    fillSaleCats cmbCat
    fillDepts cmbDept
End Sub

Private Sub fillGrid()
    Dim sql As String
    Dim totalCols(2) As Integer
    Dim omitRepeatCols(0) As Integer
    totalCols(0) = 4
    totalCols(1) = 7
    
    sql = "SELECT dbo.tblSaleBill.Date AS [Bill Date],  dbo.tblSaleBill.SaleBillID as [Bill No], dbo.tblSaleBill.Price AS [Gross Total], dbo.tblSaleBill.Discount, dbo.tblSaleBill.NetPrice AS [Net Total], dbo.tblSaleBill.Cancelled as [Returns or Cancellations] ,  dbo.tblSaleBill.Returned , (dbo.tblSaleBill.CancelledValue) as [Cancelled Value] , (dbo.tblSaleBill.ReturnedValue ) as [Returned Value] , OutCustomer.FirstName , dbo.tblBHT.BHT , BilledStaff.Name , BhtPatient.FirstName , IssuedTo.Store AS [Issued To] " & _
            "FROM dbo.tblSaleCategory RIGHT OUTER JOIN  dbo.tblPatientMainDetails OutCustomer RIGHT OUTER JOIN  dbo.tblStore IssuedFrom RIGHT OUTER JOIN dbo.tblStaff BilledStaff RIGHT OUTER JOIN  dbo.tblSaleBill ON BilledStaff.StaffID = dbo.tblSaleBill.BilledStaffId LEFT OUTER JOIN   dbo.tblStaff Cashier ON dbo.tblSaleBill.StaffID = Cashier.StaffID LEFT OUTER JOIN dbo.tblStore IssuedTo ON dbo.tblSaleBill.BilledUnitID = IssuedTo.StoreID ON IssuedFrom.StoreID = dbo.tblSaleBill.StoreID LEFT OUTER JOIN dbo.tblBHT LEFT OUTER JOIN dbo.tblPatientMainDetails BhtPatient ON dbo.tblBHT.PatientID = BhtPatient.PatientID ON dbo.tblSaleBill.BilledBHTID = dbo.tblBHT.BHTID ON OutCustomer.PatientID = dbo.tblSaleBill.BilledOutPatientID ON dbo.tblSaleCategory.SaleCategoryID = dbo.tblSaleBill.SaleCategoryID " & _
            "WHERE (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(dtpFrom.Value, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(dtpTo.Value, "dd MMMM yyyy") & "', 102)) "
    If IsNumeric(cmbCat.BoundText) = True Then
        sql = sql & "AND (dbo.tblSaleBill.SaleCategoryID = " & Val(cmbCat.BoundText) & ") "
    End If
    If IsNumeric(cmbDept.BoundText) = True Then
        sql = sql & "AND (dbo.tblSaleBill.StoreID = " & Val(cmbDept.BoundText) & ")"
    End If
    sql = sql & " ORDER BY dbo.tblSaleBill.SaleBillID"
    'FillAnyGrid sql, gridBills
    FillDetailGrid sql, gridBills, 1, totalCols, omitRepeatCols
    replaceGridString gridBills, 5, "True", "Cancelled Value - "
    replaceGridString gridBills, 5, "False", ""
    replaceGridString gridBills, 6, "True", "Returned Value - "
    replaceGridString gridBills, 6, "False", ""
    
    joinGridColString gridBills, 5, 6
    joinGridColDouble gridBills, 7, 8
    
    formatGridString gridBills, 7, "0.00"
    formatGridString gridBills, 2, "0.00"
    formatGridString gridBills, 3, "0.00"
    formatGridString gridBills, 4, "0.00"
    
    replaceGridString gridBills, 7, "0.00", ""
    replaceGridString gridBills, 2, "0.00", ""
    replaceGridString gridBills, 3, "0.00", ""
    replaceGridString gridBills, 4, "0.00", ""
    
    joinGridColString gridBills, 5, 7
    
    gridBills.Cols = 6
    
End Sub

Private Sub saveSettings()
    SaveCommonSettings Me
End Sub

Private Sub getSettings()
    GetCommonSettings Me
    dtpFrom.Value = Date
    dtpTo.Value = Date
End Sub

Private Sub btnExcel_Click()
    GridToExcel1 gridBills, "Bills Report " & cmbCat.text & " " & cmbDept.text, Format(dtpFrom.Value, LongDateFormat) & " - " & Format(dtpTo.Value, LongDateFormat)
End Sub

Private Sub btnPreview_Click()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            previewSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            previewSaleBillA4 id, Me.hwnd
        End If
    End If

End Sub

Private Sub btnPrint_Click()
    Dim RetVal As Integer
    Dim TemResponce As Integer
    Dim CsetPrinter As New cSetDfltPrinter
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    Dim ThisReportFormat As PrintReport
    GetPrintDefaults ThisReportFormat
    GridPrint gridBills, ThisReportFormat, "All Bills - " & cmbCat.text & vbTab & cmbDept.text, Format(dtpFrom.Value, LongDateFormat) & " - " & Format(dtpTo.Value, LongDateFormat)
    Printer.EndDoc
End Sub

Private Sub btnProcess_Click()
    Call fillGrid
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnProcess_Click
    ElseIf KeyCode = vbKeyEscape Then
        cmbDept.text = Empty
    End If
End Sub

Private Sub cmbCat_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnProcess_Click
    ElseIf KeyCode = vbKeyEscape Then
        cmbCat.text = Empty
    End If
End Sub

Private Sub Form_Load()
    fillCombos
    getSettings
    cmbDept.BoundText = UserStoreId
    btnProcess_Click
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    saveSettings
End Sub

Private Sub Form_Resize()
    gridBills.Width = Me.Width - 500
    gridBills.Height = Me.Height - 3000
    btnClose.Top = Me.Height - 1250
    btnPrint.Top = btnClose.Top
    btnExcel.Top = btnClose.Top
End Sub

Private Sub gridBills_DblClick()
    Dim id As Long
    id = Val(gridBills.TextMatrix(gridBills.Row, 0))
    If id <> 0 Then
        Dim temBill As New SaleBill
        temBill.SaleBillID = id
        Dim sc As New SaleCategory
        sc.SaleCategoryID = temBill.SaleCategoryID
        If sc.billPrint = True Then
            previewSaleBillA5 id, Me.hwnd
        ElseIf sc.reportPrint = True Then
            previewSaleBillA4 id, Me.hwnd
        End If
    End If
End Sub
