VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmSaleReceive 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Receive Items"
   ClientHeight    =   8985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8985
   ScaleWidth      =   13995
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtCell 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   4680
      TabIndex        =   44
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox txtComments 
      Height          =   375
      Left            =   3720
      TabIndex        =   42
      Top             =   8400
      Width           =   5295
   End
   Begin MSFlexGridLib.MSFlexGrid GridItem 
      Height          =   5415
      Left            =   240
      TabIndex        =   0
      Top             =   2760
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   9551
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcRepaymentMethod 
      Height          =   360
      Left            =   6120
      TabIndex        =   39
      Top             =   7200
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Enabled         =   0   'False
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtCustomerID 
      Height          =   375
      Left            =   11280
      TabIndex        =   38
      Top             =   7200
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CheckBox chkPrint 
      Caption         =   "Print"
      Height          =   495
      Left            =   9120
      TabIndex        =   33
      Top             =   8280
      Value           =   1  'Checked
      Width           =   735
   End
   Begin VB.TextBox txtNetReturn 
      Height          =   375
      Left            =   1920
      TabIndex        =   31
      Top             =   7560
      Width           =   2295
   End
   Begin VB.TextBox txtReturnDiscount 
      Height          =   375
      Left            =   1320
      TabIndex        =   29
      Top             =   7320
      Width           =   2295
   End
   Begin VB.TextBox txtReturnValue 
      Height          =   375
      Left            =   2040
      TabIndex        =   23
      Top             =   7320
      Width           =   2295
   End
   Begin btButtonEx.ButtonEx bttnUpdate 
      Height          =   495
      Left            =   10080
      TabIndex        =   21
      Top             =   8280
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   873
      Appearance      =   3
      BorderColor     =   255
      Caption         =   "&Receive"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   12480
      TabIndex        =   20
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      BorderColor     =   255
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcCheckedStaff 
      Height          =   360
      Left            =   11040
      TabIndex        =   34
      Top             =   2160
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      Enabled         =   0   'False
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo dtcIssueStaff 
      Height          =   360
      Left            =   11040
      TabIndex        =   35
      Top             =   1680
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   635
      _Version        =   393216
      Enabled         =   0   'False
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx ButtonEx1 
      Height          =   495
      Left            =   240
      TabIndex        =   41
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      BorderColor     =   255
      Caption         =   "Re&fuse"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label11 
      Caption         =   "COmments"
      Height          =   375
      Left            =   2160
      TabIndex        =   43
      Top             =   8400
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "Repayment Method"
      Height          =   255
      Left            =   4200
      TabIndex        =   40
      Top             =   7200
      Width           =   1815
   End
   Begin VB.Label Label20 
      Caption         =   "Issued By"
      Height          =   255
      Left            =   9360
      TabIndex        =   37
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label Label21 
      Caption         =   "Received By"
      Height          =   255
      Left            =   9360
      TabIndex        =   36
      Top             =   2160
      Width           =   1695
   End
   Begin VB.Label Label17 
      Caption         =   "Net Return"
      Height          =   255
      Left            =   1200
      TabIndex        =   32
      Top             =   7080
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Discount"
      Height          =   255
      Left            =   240
      TabIndex        =   30
      Top             =   7200
      Width           =   1575
   End
   Begin VB.Label Label23 
      Caption         =   "Sending Unit"
      Height          =   255
      Left            =   9360
      TabIndex        =   28
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label22 
      Caption         =   "Receiving Unit"
      Height          =   255
      Left            =   9360
      TabIndex        =   27
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label lblCustomer 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   11040
      TabIndex        =   26
      Top             =   240
      Width           =   2535
   End
   Begin VB.Label lblName 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   11040
      TabIndex        =   25
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label Label16 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   480
      TabIndex        =   24
      Top             =   7680
      Width           =   1575
   End
   Begin VB.Label Label15 
      Caption         =   "Discount %"
      Height          =   255
      Left            =   4800
      TabIndex        =   22
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label lblDiscountPercent 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   6480
      TabIndex        =   19
      Top             =   1200
      Width           =   2535
   End
   Begin VB.Label lblPaymentMethod 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   6480
      TabIndex        =   18
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Label lblNetTotal 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   6480
      TabIndex        =   17
      Top             =   1680
      Width           =   2535
   End
   Begin VB.Label lblDiscount 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   6480
      TabIndex        =   16
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label lblGTotal 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   6480
      TabIndex        =   15
      Top             =   240
      Width           =   2535
   End
   Begin VB.Label lblBillID 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   1800
      TabIndex        =   14
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Label lblCheckedBy 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   1800
      TabIndex        =   13
      Top             =   1680
      Width           =   2535
   End
   Begin VB.Label lblUser 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   1800
      TabIndex        =   12
      Top             =   1200
      Width           =   2535
   End
   Begin VB.Label lblTime 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   1800
      TabIndex        =   11
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label lblDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   1800
      TabIndex        =   10
      Top             =   240
      Width           =   2535
   End
   Begin VB.Label Label9 
      Caption         =   "Payment Method"
      Height          =   255
      Left            =   4800
      TabIndex        =   9
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Net Total"
      Height          =   255
      Left            =   4800
      TabIndex        =   8
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "Discount"
      Height          =   255
      Left            =   4800
      TabIndex        =   7
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Gross Total"
      Height          =   255
      Left            =   4800
      TabIndex        =   6
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label5 
      Caption         =   "Bill ID"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2160
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Checked by"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "User"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Time"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Date"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   1575
   End
End
Attribute VB_Name = "frmSaleReceive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsSaleBill As New ADODB.Recordset
    Dim rsSale As New ADODB.Recordset
    Dim rsReturnBill As New ADODB.Recordset
    Dim rsReturn As New ADODB.Recordset
    Dim rsBatchStock As New ADODB.Recordset
    Dim rsIssuedCash As New ADODB.Recordset
    Dim rsIssuedCredit As New ADODB.Recordset
    Dim rsIssuedWoucher As New ADODB.Recordset
    Dim rsIssuedOther As New ADODB.Recordset
    Dim rsStaff As New ADODB.Recordset
    Dim rsViewPaymentMethod As New ADODB.Recordset
    Dim temIssueCashID As Long
    Dim temIssueCreditID As Long
    Dim temIssueWoucherID As Long
    Dim temIssueCreditCardID As Long
    Dim temIssueOtherID As Long
    Dim temSQL As String
    Dim temReturnBillID As Long
    Dim CsetPrinter As New cSetDfltPrinter
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim rsViewStaff As New ADODB.Recordset
    Dim rsCredit As New ADODB.Recordset
    Dim rsTemCustomer As New ADODB.Recordset

Private Sub fillCombos()
    With rsViewStaff
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblstaff order by listedname"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcIssueStaff
        Set .RowSource = rsViewStaff
        .ListField = "ListedName"
        .BoundColumn = "StaffID"
    End With
    With dtcCheckedStaff
        Set .RowSource = rsViewStaff
        .ListField = "ListedName"
        .BoundColumn = "StaffID"
    End With
    With rsViewPaymentMethod
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblPaymentMethod order by PaymentMethod"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcRepaymentMethod
        Set .RowSource = rsViewPaymentMethod
        .ListField = "PaymentMethod"
        .BoundColumn = "PaymentMethodID"
    End With
    
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnUpdate_Click()
    
 '   On Error Resume Next
    
    Dim tr As Integer
    Dim TemCost As Double
    
    Dim temBilledOutPatientID As Long
    Dim temBilledBHTID As Long
    Dim temBilledStaffID As Long
    Dim temBilledUnitID As Long
    
    Dim newRefillBill As RefillBill
    Dim newRefill As Refill
    Dim newsale As SaleBill
    Dim newSaleItem As SaleItem
    Dim ba As Batch
    Dim bs As BatchStock
    
    recordDailyStock
    
    tr = MsgBox("Are you sure you want to accept this bill?", vbYesNo, "Accept")
    
    If tr = vbNo Then
        Exit Sub
    End If
    
    If Val(txtReturnValue.text) <= 0 Then
        tr = MsgBox("There are no items to return", vbCritical, "No Items")
        Exit Sub
    End If
    
    Set newsale = New SaleBill
    
    newsale.SaleBillID = Val(lblBillID.Caption)
    
    If (newsale.Received = True) Then
        MsgBox "Already Received"
        Exit Sub
    End If
    
    
    newsale.Received = True
    newsale.ReceivedDate = Date
    newsale.ReceivedTime = Now
    newsale.ReceivedUserID = UserId
    newsale.ReceivedComments = txtComments.text
    newsale.saveData
    
    Set newRefillBill = New RefillBill
    
    Dim myDeptId As String
    myDeptId = getDeptTransferBillId(newsale.StoreId, UserStoreId)
    
    With newRefillBill
        .deptInvoiceNo = myDeptId
        .AutoRequest = False
        .Cancelled = False
        .CheckedStaffID = Val(dtcCheckedStaff.BoundText)
        .Discount = newsale.Discount
        .DiscountPercent = newsale.DiscountPercent
        .DistributorID = 0
        .DistributorOrderBillID = 0
        .FullyPaid = 0
        .InvoiceDate = Date
        .InvoiceNo = newsale.SaleBillID
        .ManualRequest = False
        .NetPrice = newsale.NetPrice
        .OrderBillID = newsale.SaleBillID
        .BillDate = Date
        .PaidDate = Date
        .PaidPrice = 0
        .PaidStaffID = UserId
        .PaidTime = Now
        .PaymentMethod = "Transfer"
        .PaymentMethodID = 11
        .Price = newsale.Price
        .Purchase = False
        .Returned = False
        .SettledValue = .NetPrice
        .StaffID = UserId
        .StoreId = UserStoreId
        .SaleId = newsale.SaleBillID
        .ReceivedIssue = True
        .saveData
    End With
    
    
    With GridItem
        Dim i As Integer
        For i = 1 To .Rows - 2
            Set newSaleItem = New SaleItem
            Set newRefill = New Refill
            Set ba = New Batch
            Set bs = New BatchStock
            
            newSaleItem.SaleId = Val(.TextMatrix(i, 0))
        
            bs.BatchID = newSaleItem.BatchID
            
        
            With newRefill
                .Amount = newSaleItem.Amount
                .AutoRequest = False
                .BatchID = newSaleItem.BatchID
                .CatogeryID = newSaleItem.CategoryID
                .CheckedStaffID = Val(dtcCheckedStaff.BoundText)
                .Comments = txtComments.text + " "
                .Discount = newSaleItem.Discount
                .DiscountPercent = newSaleItem.DiscountPercent
                .DistributorID = 0
                .DOE = ba.DOE
                .DOM = ba.DOM
                .FreeAmount = 0
                .IssueReceive = True
                .ItemID = newSaleItem.ItemID
                .LastPPrice = findPurchasePrice(.ItemID)
                .LastSPrice = findSalePrice(.ItemID)
                .LastWPrice = findWholeSalePrice(.ItemID)
                .ManualRequest = False
                .NetPrice = newSaleItem.Price
                .OrderBillID = 0
                .OrderID = 0
                .pprice = findPurchasePrice(.ItemID, .BatchID)
                .Price = newSaleItem.Rate * .Amount
                .Purchase = False
                .RefillBillID = newRefillBill.RefillBillID
                .Returned = False
                .SaleBillID = newsale.SaleBillID
                .SaleId = newSaleItem.SaleId
                .sprice = findSalePrice(.ItemID, .BatchID)
                .StaffID = UserId
                .StoreId = UserStoreId
                .Time = Now
                .wprice = findWholeSalePrice(.ItemID, .BatchID)
                .saveData
            End With
        
        
            If rsBatchStock.State = 1 Then rsBatchStock.Close
            temSQL = "SELECT * from tblBatchStock where BatchID = " & Val(.TextMatrix(i, 2)) & " AND StoreID = " & UserStoreId
            rsBatchStock.Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
            If rsBatchStock.RecordCount < 1 Then
                rsBatchStock.AddNew
                rsBatchStock!BatchID = Val(.TextMatrix(i, 2))
                rsBatchStock!Stock = Val(.TextMatrix(i, 8))
                rsBatchStock!StoreId = UserStoreId
            Else
                rsBatchStock!Stock = rsBatchStock!Stock + Val(.TextMatrix(i, 8))
            End If
            rsBatchStock!sprice = findSalePrice(newRefill.ItemID, newRefill.BatchID)
            rsBatchStock!pprice = findPurchasePrice(newRefill.ItemID, newRefill.BatchID)
            rsBatchStock!wprice = findWholeSalePrice(newRefill.ItemID, newRefill.BatchID)
            
            rsBatchStock.Update
        Next
    End With
    MsgBox "Received"
    Unload Me

'   0   SaleId
'   1   ItemId
'   2   BatchId
'   3   Issue Unit Id
'   4   No
'   5   Item
'   6   Batch
'   7   Quentity
'   8   Accepting
'   9   Returning
'   10  Purchase Rate
'   11   Retail Rate
'   12   Wholesale Rate
'   13   ReceivingValueAtSalePrice
'   14   AcceptingValueAtSalePrice
'   15   RturningValueAtSalePrice
'   16   ReceivingValueAtPurchasePrice
'   17   AcceptingValueAtPurchasePrice
'   18   ReturningValueAtPurchasePrice
'   0   No
'   1   Item
'   2   Batch
'   3   Rate
'   4   Amount
'   5   Price
'   6   ItemID
'   7   BatchID
'   8   AMount
'   9   Rate
'   10  IUnit
'   11  Return Qty
'   12  Return Value
'   13  SaleID


End Sub

Private Sub SetBillPrinter()
    
    
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)

    PrinterName = Printer.DeviceName
    
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    
    
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    
        
    Dim MyPrinter As VB.Printer
    For Each MyPrinter In VB.Printers
        If MyPrinter.DeviceName = BillPrinterName Then
            Set Printer = MyPrinter
        End If
    Next
    
End Sub

Private Sub Form_Load()
    Call fillCombos
    dtcIssueStaff.BoundText = UserId
    Dim tr As Integer
    If TxSaleBillID = 0 Then
        Unload Me
        Exit Sub
    End If
    With rsSaleBill
        If .State = 1 Then .Close
'        temSql = "SELECT tblSaleBill.*, tblPatientMainDetailsOutdoor.FirstName, tblPatientMainDetailsIndoor.FirstName, tblBHT.BHT, tblStaffCustomer.Name, tblPaymentMethod.PaymentMethod, tblStaff.Name, tblCStaff.Name, tblStore.Store " & _
'                    "FROM ((((((tblStaff AS tblStaffCustomer RIGHT JOIN (tblSaleBill INNER JOIN tblStaff ON tblSaleBill.StaffID = tblStaff.StaffID) ON tblStaffCustomer.StaffID = tblSaleBill.BilledStaffID) LEFT JOIN tblBHT ON tblSaleBill.BilledBHTID = tblBHT.BHTID) LEFT JOIN tblPatientMainDetails AS tblPatientMainDetailsIndoor ON tblBHT.PatientID = tblPatientMainDetailsIndoor.PatientID) LEFT JOIN tblPatientMainDetails AS tblPatientMainDetailsOutdoor ON tblSaleBill.BilledOutPatientID = tblPatientMainDetailsOutdoor.PatientID) LEFT JOIN tblStaff AS tblCStaff ON tblSaleBill.CheckedStaffID = tblCStaff.StaffID) LEFT JOIN tblPaymentMethod ON tblSaleBill.PaymentMethodID = tblPaymentMethod.PaymentMethodID) LEFT JOIN tblStore ON tblSaleBill.BilledUnitID = tblStore.StoreID " & _
'                    "WHERE (((tblSaleBill.SaleBillID)=" & TxSaleBillID & ")) "
        temSQL = "SELECT tblSaleBill.*, tblPatientMainDetailsOutdoor.FirstName as ODFirstName, tblPatientMainDetailsIndoor.FirstName as IDFirstName, tblBHT.BHT, tblStaffCustomer.Name as SCName, tblPaymentMethod.PaymentMethod, tblStaff.Name as StaffName, tblCStaff.Name as CName, tblStore.Store " & _
                    "FROM ((((((tblStaff AS tblStaffCustomer RIGHT JOIN (tblSaleBill INNER JOIN tblStaff ON tblSaleBill.StaffID = tblStaff.StaffID) ON tblStaffCustomer.StaffID = tblSaleBill.BilledStaffID) LEFT JOIN tblBHT ON tblSaleBill.BilledBHTID = tblBHT.BHTID) LEFT JOIN tblPatientMainDetails AS tblPatientMainDetailsIndoor ON tblBHT.PatientID = tblPatientMainDetailsIndoor.PatientID) LEFT JOIN tblPatientMainDetails AS tblPatientMainDetailsOutdoor ON tblSaleBill.BilledOutPatientID = tblPatientMainDetailsOutdoor.PatientID) LEFT JOIN tblStaff AS tblCStaff ON tblSaleBill.CheckedStaffID = tblCStaff.StaffID) LEFT JOIN tblPaymentMethod ON tblSaleBill.PaymentMethodID = tblPaymentMethod.PaymentMethodID) LEFT JOIN tblStore ON tblSaleBill.BilledUnitID = tblStore.StoreID " & _
                    "WHERE (((tblSaleBill.SaleBillID)=" & TxSaleBillID & ")) "
        
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If !Cancelled = True Then
                tr = MsgBox("This bill is already cancelled.", vbCritical, "Cancelled")
                Unload Me
                Exit Sub
            ElseIf !Returned = True Then
                tr = MsgBox("This items in this bill is already Canceled.", vbCritical, "Cancelled")
                Unload Me
                Exit Sub
            Else
                lblDate.Caption = Format(!Date, LongDateFormat)
                lblTime.Caption = !Time
                lblUser.Caption = ![StaffName]
                If Not IsNull(![CName]) Then
                    lblCheckedBy.Caption = ![CName]
                End If
                lblBillID.Caption = !SaleBillID
                lblGTotal.Caption = ![Price]
                txtReturnValue.text = Format(!Price, "0.00")
                lblDiscount.Caption = ![Discount]
                txtReturnDiscount.text = !Discount
                lblDiscountPercent.Caption = ![DiscountPercent]
                lblNetTotal.Caption = ![NetPrice]
                txtNetReturn.text = !NetPrice
                lblPaymentMethod.Caption = Format(![PaymentMethod], "")
                dtcRepaymentMethod.BoundText = !PaymentMethodID
                lblBillID.Caption = ![SaleBillID]
                If Not IsNull(![ODFirstName]) Then
                    lblName.Caption = ![ODFirstName]
                    lblCustomer.Caption = "Outdoor Customer"
                    txtCustomerID.text = !BilledOutPatientID
                ElseIf Not IsNull(![IDFirstName]) Then
                   
                ElseIf Not IsNull(![SCName]) Then
                    lblCustomer.Caption = "Staff Customer"
                    lblName.Caption = ![SCName]
                    txtCustomerID.text = !BilledStaffID
                ElseIf Not IsNull(!Store) Then
                    Dim myStore As New clsStore
                    myStore.StoreId = !StoreId
                    lblCustomer.Caption = myStore.Store
                    lblName.Caption = ![Store]
                    txtCustomerID.text = ![BilledUnitID]
                End If
            End If
        Else
            tr = MsgBox("There is no such Bill ID ", vbCritical, "Error")
            Unload Me
            Exit Sub
        End If
        .Close
    End With
    dtcCheckedStaff.BoundText = UserId
    
    Call FormatItemGrid
    Call FillItemGrid
End Sub


Private Sub FormatItemGrid()
    With GridItem
        .Cols = 19
        .Rows = 1
        Dim i As Integer
        For i = 0 To .Cols - 1
            .Col = i
            .CellAlignment = 4
            Select Case i
                Case 4: .text = "No."
                        .ColWidth(i) = 400
                Case 5: .text = "Item"
                        .ColWidth(i) = 3600
                Case 6: .text = "Batch"
                        .ColWidth(i) = 1000
                Case 7: .text = "Received Quentity"
                        .ColWidth(i) = 1200
'                Case 8: .text = "Accepting Quentity"
'                        .ColWidth(i) = 1200
'                Case 9: .ColWidth(i) = 1200
'                        .text = "Returning Quentity"
                Case 10: .text = "Purchase Rate"
                        .ColWidth(i) = 1200
                Case 11: .text = "Retail Rate"
                        .ColWidth(i) = 1200
                Case 12: .ColWidth(i) = 1200
                        .text = "Wholesale Rate"
                Case 13:    .ColWidth(i) = 1200
                            .text = "Received Value at Sale Rate"
'                Case 14:    .ColWidth(i) = 1200
'                            .text = "Accepted Value at Sale Rate"
'                Case 15:    .ColWidth(i) = 1200
'                            .text = "Returned Value at Sale Rate"
                Case 16:    .ColWidth(i) = 1200
                            .text = "Received Value at Purchase Rate"
'                Case 17:    .ColWidth(i) = 1200
'                            .text = "Accepted Value at Purchase Rate"
'                Case 18:    .ColWidth(i) = 1200
'                            .text = "Returned Value at Purchase Rate"
                            
                Case Else
                        .ColWidth(i) = 1
            End Select
        Next
        
'   0   SaleId
'   1   ItemId
'   2   BatchId
'   3   Issue Unit Id
'   4   No
'   5   Item
'   6   Batch
'   7   Quentity
'   8   Accepting
'   9   Returning
'   10  Purchase Rate
'   11   Retail Rate
'   12   Wholesale Rate
'   13   ReceivingValueAtSalePrice
'   14   AcceptingValueAtSalePrice
'   15   RturningValueAtSalePrice
'   16   ReceivingValueAtPurchasePrice
'   17   AcceptingValueAtPurchasePrice
'   18   ReturningValueAtPurchasePrice
        
        
        
        
        
'   0   No
'   1   Item
'   2   Batch
'   3   Rate
'   4   Amount
'   5   Price
'   6   ItemID
'   7   BatchID
'   8   AMount
'   9   Rate
'   10  IUnit
'   11  Return Qty
'   12  Return Value
'   13  SaleID
'   15  Cost
    End With
End Sub

Private Sub FillItemGrid()
    Dim totalSaleVal As Double
    Dim totalPurchaseVal As Double
    
    With rsSale
        If .State = 1 Then .Close
        temSQL = "SELECT tblSale.*, tblItem.Display, tblIssueUnit.IssueUnit, tblSaleBill.SaleBillID, tblBatch.Batch " & _
                    "FROM (tblIssueUnit RIGHT JOIN ((tblSaleBill LEFT JOIN tblSale ON tblSaleBill.SaleBillID = tblSale.SaleBillID) LEFT JOIN tblItem ON tblSale.ItemID = tblItem.ItemID) ON tblIssueUnit.IssueUnitID = tblItem.IssueUnitID) LEFT JOIN tblBatch ON tblSale.BatchID = tblBatch.BatchID " & _
                    "WHERE (((tblSaleBill.SaleBillID)=" & TxSaleBillID & "))"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            Dim i As Integer
            .MoveLast
            GridItem.Rows = .RecordCount + 1
            .MoveFirst
            i = 0
'On Error GoTo er
            While .EOF = False
                i = i + 1
                
                Dim temSaleItem As New SaleItem
                Dim temitem As New clsItem
                Dim pp As Double
                Dim sp As Double
                Dim wp As Double
                
                GridItem.TextMatrix(i, 0) = ![SaleId]
                GridItem.TextMatrix(i, 1) = ![ItemID]
                GridItem.TextMatrix(i, 2) = ![BatchID]
                'GridItem.TextMatrix(i, 3) = ![IssueUnitID]
                temSaleItem.SaleId = !SaleId
                temitem.ItemID = temSaleItem.ItemID
                                
                GridItem.TextMatrix(i, 4) = i
                GridItem.TextMatrix(i, 5) = temitem.Display
                If Not IsNull(!Batch) Then
                    GridItem.TextMatrix(i, 6) = !Batch
                End If
                 GridItem.TextMatrix(i, 7) = !Amount
                GridItem.TextMatrix(i, 8) = !Amount
                GridItem.TextMatrix(i, 9) = 0
                
                pp = findPurchasePrice(temitem.ItemID)
                sp = findSalePrice(temitem.ItemID)
                wp = findWholeSalePrice(temitem.ItemID)
                
                GridItem.TextMatrix(i, 10) = Format(pp, "0.00")
                GridItem.TextMatrix(i, 11) = Format(sp, "0.00")
                GridItem.TextMatrix(i, 12) = Format(wp, "0.00")
                GridItem.TextMatrix(i, 13) = Format(sp * !Amount, "0.00")
                GridItem.TextMatrix(i, 14) = Format(sp * !Amount, "0.00")
                GridItem.TextMatrix(i, 15) = "0.00"
                GridItem.TextMatrix(i, 16) = Format(pp * !Amount, "0.00")
                GridItem.TextMatrix(i, 17) = Format(pp * !Amount, "0.00")
                GridItem.TextMatrix(i, 18) = "0.00"
                
                totalSaleVal = totalSaleVal + (sp * !Amount)
                totalPurchaseVal = totalPurchaseVal + pp * !Amount
                
                .MoveNext
        
'   0   SaleId
'   1   ItemId
'   2   BatchId
'   3   Issue Unit Id
'   4   No
'   5   Item
'   6   Batch
'   7   Quentity
'   8   Accepting
'   9   Returning
'   10  Purchase Rate
'   11   Retail Rate
'   12   Wholesale Rate
'   13   ReceivingValueAtSalePrice
'   14   AcceptingValueAtSalePrice
'   15   RturningValueAtSalePrice
'   16   ReceivingValueAtPurchasePrice
'   17   AcceptingValueAtPurchasePrice
'   18   ReturningValueAtPurchasePrice
        
        
                
            Wend
        End If
        .Close
    End With
    
    With GridItem
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 13
        .text = Format(totalSaleVal, "0.00")
        .Col = 16
        .text = Format(totalPurchaseVal, "0.00")
    
    End With
    
    
    Exit Sub
    
er:
    MsgBox "Error"
    Exit Sub
    Unload Me
    
'   0   SaleId
'   1   ItemId
'   2   BatchId
'   3   Issue Unit Id
'   4   No
'   5   Item
'   6   Batch
'   7   Quentity
'   8   Accepting
'   9   Purchase Rate
'   10   Retail Rate
'   11   Wholesale Rate
'   12   ReceivingValueAtPurchasePrice
'   13   ReceivingValueAtSalePrice


    
End Sub


Private Sub SetBillPaper()
    Dim TemResponce As Long
    Dim RetVal As Integer
    RetVal = SelectForm(BillPaperName, Me.hwnd)
    Select Case RetVal
        Case FORM_NOT_SELECTED   ' 0
            TemResponce = MsgBox("You have not selected a printer form to print, Please goto Preferances and Printing preferances to set a valid printer form.", vbExclamation, "Bill Not Printed")
        Case FORM_SELECTED   ' 1
            Call SelectPrint
        Case FORM_ADDED   ' 2
            TemResponce = MsgBox("New paper size added.", vbExclamation, "New Paper size")
    End Select
End Sub

Private Sub SelectPrint()
    If LCase(Left(Trim(HospitalName), 1)) = "m" Then
        
    ElseIf LCase(Left(Trim(HospitalName), 1)) = "r" Then
        RuhunaPrint
    ElseIf LCase(Left(Trim(HospitalName), 1)) = "c" Then
    
    Else
    
    End If
End Sub

Private Sub RuhunaPrint()

    On Error GoTo eh:

    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim Tab6 As Integer
    Dim Tab7 As Integer
    Dim Tab8 As Integer
    Dim Tab9 As Integer
    
    Tab1 = 4
    Tab2 = 15
    Tab3 = 36
    Tab4 = 20
    Tab5 = 50
    Tab6 = 55
    Tab7 = 70
    Tab8 = 23
    Tab9 = 65
    With Printer

'        .FontSize = 12
'        .Font = "Lucida Console"
'        Printer.Print
'        Printer.Print Tab(Tab8); UserStore & "   -  Cancellation Note"
'        Printer.Print
        .FontSize = 12
        .Font = "Lucida Console"
'        Printer.Print Tab(4); "             Co-Operative Hospital "
        Printer.Print Tab(4); "Cancellation"
        Printer.Print
        .FontSize = 10
        .Font = "Lucida Console"
'        Printer.Print Tab(Tab1); "Karapitiya, Galle." & "           Tel: 091-2234059-60, 091-5577113-14"
'        Printer.Print
        Dim TemString As String
        Printer.Print Tab(Tab1); "Issue No        - "; lblBillID.Caption & "-" & TemString; " Issued Date: "; Format(lblDate.Caption, "dd MM yy"); Tab(Tab6); "Issued Time : "; lblTime.Caption
        Printer.Print
        Printer.Print Tab(Tab1); "Cancel No - "; temReturnBillID & "-" & TemString; " Cancelled Date: "; Format(Date, "dd MM yy"); Tab(Tab6); "Cancelled Time : "; Time
        Printer.Print Tab(Tab1); "Patient : "; lblName.Caption; "       "
        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
        Printer.Print Tab(Tab1); "Category"; Tab(Tab2); "Item Name"; Tab(Tab3); "Quentity"; Tab(Tab5); Right(Space(12) & "Price", 9); Tab(Tab9); Right(Space(12) & "Value", 13)
        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
        Printer.Print
        .FontSize = 10
        .Font = "Lucida Console"
    End With
    Tab1 = 4
    Tab2 = 15
    Tab3 = 36
    Tab4 = 20
    Tab5 = 50
    Tab6 = 55
    Tab7 = 70
    Tab9 = 65
    With GridItem
        For i = 1 To .Rows - 1
        Printer.FontSize = 10
        Printer.Font = "Lucida Console"
'            Printer.Print Tab(Tab1); .TextMatrix(I, 11);
            Printer.Print Tab(Tab2); Left(.TextMatrix(i, 1), 20);
            Printer.Print Tab(Tab3); Left(.TextMatrix(i, 11), 24);
            Printer.Print Tab(Tab5); Right(Space(12) & .TextMatrix(i, 9), 9);
            Printer.Print Tab(Tab7); Right(Space(12) & .TextMatrix(i, 12), 8)
        Next i
    End With
    With Printer
        .Font = 10
        .Font = "Lucida Console"
        Printer.Print
        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
'        Printer.Print
        .FontSize = 10
        .Font = "Lucida Console"
        Printer.Print Tab(Tab1); "Gross Total"; Tab(Tab4); Right((Space(10)) & (txtReturnValue.text), 10)
        If Val(txtReturnDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab4); Right((Space(10)) & (txtReturnDiscount.text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab4); Right((Space(10)) & (txtNetReturn.text), 10)
        End If
'        Printer.Print
        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
        Printer.Print Tab(Tab1); "Operate by "; UserName; Tab(Tab5)
'        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
        Printer.Print Tab(Tab1); "Returns are acceptted only once"
'        Printer.Print Tab(Tab1); "--------------------------------------------------------------------------"
        Printer.Print
        Printer.Print
        .EndDoc
    End With
    
    
    
    


Exit Sub

eh:
    MsgBox "Printer Error"


End Sub

Private Sub GridItem_EnterCell()
    txtCell.Visible = False
    With GridItem
        If .Row < 1 Then Exit Sub
        If .Col <> 8 Then Exit Sub
        txtCell.Height = .CellHeight
        txtCell.Width = .CellWidth
        txtCell.Left = .Left + .CellLeft
        txtCell.Top = .CellTop + .Top
        cellToTxt
        txtCell.Visible = True
    End With
End Sub

Private Sub cellToTxt()
    txtCell.text = GridItem.TextMatrix(GridItem.Row, GridItem.Col)
End Sub

Private Function txtToCell() As Boolean
    With GridItem
        If .TextMatrix(.Row, 8) > .TextMatrix(.Row, 7) Then
            MsgBox "Accepting amount can not be larger than the "
            txtToCell = False
        End If
    End With
    GridItem.TextMatrix(GridItem.Row, GridItem.Col) = txtCell.text
End Function

'   0   SaleId
'   1   ItemId
'   2   BatchId
'   3   Issue Unit Id
'   4   No
'   5   Item
'   6   Batch
'   7   Quentity
'   8   Accepting
'   9   Returning
'   10  Purchase Rate
'   11   Retail Rate
'   12   Wholesale Rate
'   13   ReceivingValueAtSalePrice
'   14   AcceptingValueAtSalePrice
'   15   RturningValueAtSalePrice
'   16   ReceivingValueAtPurchasePrice
'   17   AcceptingValueAtPurchasePrice
'   18   ReturningValueAtPurchasePrice

Private Sub txtCell_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txtToCell
        txtCell.text = Empty
        If GridItem.Row < GridItem.Rows - 1 Then
            GridItem.Row = GridItem.Rows + 1
            GridItem.Col = GridItem.Col
            GridItem_EnterCell
        Else
            bttnUpdate.SetFocus
        End If
    ElseIf KeyCode = vbKeyEscape Then
        txtCell.text = GridItem.TextMatrix(GridItem.Row, GridItem.Col)
    ElseIf KeyCode = vbKeyUp Then
    
    ElseIf KeyCode = vbKeyDown Then
    
    End If
End Sub
