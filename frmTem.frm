VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Begin VB.Form frmTem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   10065
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10920
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10065
   ScaleWidth      =   10920
   Begin VB.Frame frameStaff 
      Caption         =   "Member Sale"
      Height          =   4455
      Left            =   4200
      TabIndex        =   3
      Top             =   5040
      Width           =   3015
      Begin VB.TextBox txtTemMemberCredit 
         Height          =   375
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   3720
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.TextBox txtMemberBalance 
         Alignment       =   1  'Right Justify
         Height          =   375
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   3000
         Width           =   2775
      End
      Begin VB.TextBox txtMember 
         Height          =   465
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   2775
      End
      Begin MSDataListLib.DataList lstMember 
         Height          =   1785
         Left            =   120
         TabIndex        =   4
         Top             =   840
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   2858
         _Version        =   393216
      End
      Begin MSDataListLib.DataCombo dtcStaffCustomer 
         Height          =   465
         Left            =   120
         TabIndex        =   7
         Top             =   2160
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label41 
         Caption         =   "Current Amount"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   2640
         Width           =   2775
      End
      Begin VB.Label Label44 
         Caption         =   "Staff"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "Credit Limit"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   3360
         Width           =   2775
      End
   End
   Begin VB.TextBox txtPrint 
      Height          =   1935
      Left            =   1080
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   720
      Width           =   4335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   4200
      TabIndex        =   1
      Top             =   2760
      Width           =   1215
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   315
      Left            =   1080
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   240
      Width           =   4335
   End
End
Attribute VB_Name = "frmTem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim CsetPrinter As New cSetDfltPrinter
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    
    
    
Private Sub Command1_Click()
   
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text)

    PrinterName = Printer.DeviceName
    
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    
    
    CsetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    
        
    Dim MyPrinter As VB.Printer
    For Each MyPrinter In VB.Printers
        If MyPrinter.DeviceName = cmbPrinter.text Then
            Set Printer = MyPrinter
        End If
    Next
    
    
    Printer.Print txtPrint.text
    Printer.EndDoc

End Sub

Private Sub Form_Load()
    Call FillPrinters
    txtPrint.text = "sfdpjsdfsd" & vbNewLine & "asd asdas asdasd" & "sfdpjsdfsd" & vbNewLine & "asd asdas asdasd" & "sfdpjsdfsd" & vbNewLine & "asd asdas asdasd" & "sfdpjsdfsd" & vbNewLine & "asd asdas asdasd" & "sfdpjsdfsd" & vbNewLine & "asd asdas asdasd"
End Sub

Private Sub FillPrinters()
    
    Dim MyPrinter As VB.Printer
    For Each MyPrinter In VB.Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub
