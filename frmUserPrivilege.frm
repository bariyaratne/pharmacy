VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmUserPrivilege 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "User Privileges"
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   7755
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      Height          =   495
      Left            =   6360
      TabIndex        =   7
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "Remove"
      Height          =   495
      Left            =   6360
      TabIndex        =   6
      Top             =   2640
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grid 
      Height          =   3855
      Left            =   1800
      TabIndex        =   5
      Top             =   2640
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   6800
      _Version        =   393216
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      Height          =   495
      Left            =   6240
      TabIndex        =   4
      Top             =   600
      Width           =   1215
   End
   Begin VB.ListBox lstPrivilege 
      Height          =   1740
      Left            =   1800
      TabIndex        =   3
      Top             =   600
      Width           =   4335
   End
   Begin MSDataListLib.DataCombo dtcStaff 
      Height          =   360
      Left            =   1800
      TabIndex        =   2
      Top             =   120
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label2 
      Caption         =   "Privilege"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "User"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmUserPrivilege"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsStaff As New ADODB.Recordset
    Dim temsql As String
    
Private Sub fillCombos()

    With rsStaff
        If .State = 1 Then .Close
        temsql = "SELECT * from tblstaff where IsAUser =1 order by name"
        .Open temsql, cnnStores, adOpenStatic, adLockReadOnly
    End With
    With dtcStaff
        Set .RowSource = rsStaff
        .ListField = "ListedName"
        .BoundColumn = "StaffID"
    End With
    
'    Purchase = 1
'    stockAdjust = 2
'    priceAdjust = 3
'    Edit = 4
'    BackOffice = 5
'    Staff = 6
    
    lstPrivilege.AddItem "Purchase"
    lstPrivilege.AddItem "Stock Adjustments"
    lstPrivilege.AddItem "Price Adjustments"
    lstPrivilege.AddItem "Edit"
    lstPrivilege.AddItem "Back Office"
    lstPrivilege.AddItem "Staff"
    
    lstPrivilege.ItemData(0) = 1
    lstPrivilege.ItemData(1) = 2
    lstPrivilege.ItemData(2) = 3
    lstPrivilege.ItemData(3) = 4
    lstPrivilege.ItemData(4) = 5
    lstPrivilege.ItemData(5) = 6
    
    
End Sub

Private Sub btnAdd_Click()
    If lstPrivilege.text = "" Then
        MsgBox "Select Privilege"
        lstPrivilege.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcStaff.BoundText) = False Then
        MsgBox "Staff ?"
        dtcStaff.SetFocus
        Exit Sub
    End If
    Dim sql As String
    Dim rsTem As New ADODB.Recordset
    sql = "select * from tbluserPrivilege where userid = " & Val(dtcStaff.BoundText) & " and Privilege = " & lstPrivilege.ItemData(lstPrivilege.ListIndex)
    With rsTem
        .Open sql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            MsgBox "Already there"
            .Close
            Exit Sub
        Else
            .AddNew
            !userid = Val(dtcStaff.BoundText)
            !Privilege = lstPrivilege.ItemData(lstPrivilege.ListIndex)
            .Update
            .Close
            fillList
        End If
    End With
End Sub


Public Sub fillList()
    Dim sql As String
    sql = "Select id,privilege from tblUserPrivilege where userid = " & Val(dtcStaff.BoundText) & " "
    FillAnyGrid sql, grid
    
    '    Purchase = 1
'    stockAdjust = 2
'    priceAdjust = 3
'    Edit = 4
'    BackOffice = 5
'    Staff = 6
    
    replaceGridString grid, 1, "1", "Purchase"
    replaceGridString grid, 1, "2", "stockAdjust"
    replaceGridString grid, 1, "3", "priceAdjust"
    replaceGridString grid, 1, "4", "Edit"
    replaceGridString grid, 1, "5", "BackOffice"
    replaceGridString grid, 1, "6", "Staff"
    
    grid.ColWidth(0) = 0
    grid.ColWidth(1) = grid.Width - 50
    
    
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnRemove_Click()
    Dim id As Long
    Dim sql As String
    Dim rsTem As New ADODB.Recordset
    id = Val(grid.TextMatrix(grid.Row, 0))
    If id = 0 Then
        MsgBox "Select Privilege"
        grid.SetFocus
        Exit Sub
    End If
    With rsTem
        sql = "Select * from tblUserPrivilege where id = " & id
        .Open sql, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            .Delete adAffectCurrent
            MsgBox "Deleted"
            fillList
        End If
    End With
End Sub

Private Sub dtcStaff_Change()
    fillList
End Sub

Private Sub Form_Load()
    Call fillCombos
    
End Sub
