Attribute VB_Name = "modGridFill"
Option Explicit
    Dim temSQL As String
    Dim FSys As New Scripting.FileSystemObject
    Dim i As Integer
    Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    
Public Sub FillAnyGrid(InputSql As String, InputGrid As MSFlexGrid, Optional doNotClear As Boolean)
    Dim rsTem As New ADODB.Recordset
    Dim i As Integer
    
    With rsTem
        If .State = 1 Then .Close
        temSQL = InputSql
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        
        If doNotClear = False Then
            InputGrid.Clear
            InputGrid.Rows = 1
            InputGrid.Row = 0
        End If
            
        InputGrid.Cols = .Fields.Count
        
        
                    
        For i = 0 To .Fields.Count - 1
            InputGrid.Col = i
            InputGrid.text = .Fields(i).Name
        Next i
        
        While .EOF = False
            InputGrid.Rows = InputGrid.Rows + 1
            InputGrid.Row = InputGrid.Rows - 1
            For i = 0 To .Fields.Count - 1
                InputGrid.Col = i
                If IsNull(.Fields(i).Value) = False Then
                    InputGrid.text = .Fields(i).Value
                End If
            Next i
            .MoveNext
        Wend
        .Close
    End With
End Sub

Public Function RepeatString(InputString As String, RepeatNo As Integer) As String
    Dim r As Integer
    For r = 1 To RepeatNo
        RepeatString = RepeatString & InputString
    Next r
End Function



Public Sub GridToExcel1(ExportGrid As MSFlexGrid, Optional Topic As String, Optional Subtopic As String)
    On Error Resume Next
    
    If ExportGrid.Rows <= 1 Then
        MsgBox "Noting to Export"
        Exit Sub
    End If
    
    Dim AppExcel As Excel.Application
    Dim myworkbook As Excel.Workbook
    Dim myWorkSheet1 As Excel.Worksheet
    Dim temRow As Integer
    Dim temCol As Integer
    
    Set AppExcel = CreateObject("Excel.Application")
    Set myworkbook = AppExcel.Workbooks.Add
    Set myWorkSheet1 = AppExcel.Worksheets(1)
    
    myWorkSheet1.Cells(1, 1) = Topic
    myWorkSheet1.Cells(2, 1) = Subtopic
    
    For temRow = 0 To ExportGrid.Rows - 1
        For temCol = 0 To ExportGrid.Cols - 1
            myWorkSheet1.Cells(temRow + 3, temCol + 1) = ExportGrid.TextMatrix(temRow, temCol)
            If ExportGrid.ColWidth(temCol) < 5 Then
                myWorkSheet1.Columns(, temCol + 1).Hidden = True
            End If
        Next
    Next temRow
    
    myworkbook.SaveAs (App.Path & "\" & Topic & ".xls")
    myworkbook.Save
    myworkbook.activate
    myworkbook.Close
    
    
    
    ShellExecute 0&, "open", App.Path & "\" & Topic & ".xls", "", "", vbMaximizedFocus
End Sub



Public Function rowTotal(Grid As MSFlexGrid, Col As Integer, startRow As Integer, endRow As Integer, DoNotIncludeHidden As Boolean) As Double
    Dim i As Integer
    Dim temDbl As Double
    With Grid
        For i = startRow To endRow
            If DoNotIncludeHidden = True And .RowHeight(i) = 0 Then
                            
            Else
                temDbl = temDbl + Val(.TextMatrix(i, Col))
            End If
        Next
    End With
    rowTotal = temDbl
End Function


Public Sub gridReset(Grid As MSFlexGrid)
    Dim Row As Integer
    With Grid
        
        For Row = 0 To .Rows - 1
            .RowHeight(Row) = 260
        Next
        For Row = 0 To .Cols - 1
            .ColWidth(Row) = 2000
        Next
    End With
End Sub

Public Sub hideRows(Grid As MSFlexGrid, contentCol As Integer, contentValue As String, excludeValue As String)
    Dim Row As Integer
    Dim Col As Integer
    With Grid
        .ColWidth(contentCol) = 0
        For Row = 0 To .Rows - 1
            If .TextMatrix(Row, contentCol) <> contentValue And .TextMatrix(Row, contentCol) <> "" And .TextMatrix(Row, contentCol) <> excludeValue Then
                .RowHeight(Row) = 0
            End If
        Next
    End With
End Sub



Public Sub GridToExcel(ExportGrid As MSFlexGrid, Optional Topic As String, Optional Subtopic As String, Optional DoNotIncludeHidden As Boolean)
    If ExportGrid.Rows <= 1 Then
        MsgBox "Noting to Export"
        Exit Sub
    End If
    
    Dim AppExcel As Excel.Application
    Dim myworkbook As Excel.Workbook
    Dim myWorkSheet1 As Excel.Worksheet
    Dim temRow As Integer
    Dim temCol As Integer
    
    Set AppExcel = CreateObject("Excel.Application")
    Set myworkbook = AppExcel.Workbooks.Add
    Set myWorkSheet1 = AppExcel.Worksheets(1)
    
    myWorkSheet1.Cells(1, 1) = Topic
    myWorkSheet1.Cells(2, 1) = Subtopic
    
    For temRow = 0 To ExportGrid.Rows - 1
        For temCol = 0 To ExportGrid.Cols - 1
            If DoNotIncludeHidden = True Then
                If ExportGrid.ColWidth(temCol) <> 0 And ExportGrid.RowHeight(temRow) <> 0 Then
                    myWorkSheet1.Cells(temRow + 4, temCol + 1) = ExportGrid.TextMatrix(temRow, temCol)
                End If
            Else
                myWorkSheet1.Cells(temRow + 4, temCol + 1) = ExportGrid.TextMatrix(temRow, temCol)
            End If
        Next
    Next temRow
    
    myWorkSheet1.Range("A1:" & GetColumnName(CDbl(temCol)) & temRow + 2).AutoFormat Format:=xlRangeAutoFormatClassic1
    
    myWorkSheet1.Range("A" & temRow + 3 & ":" & GetColumnName(CDbl(temCol)) & temRow + 3).AutoFormat Format:=xlRangeAutoFormat3DEffects1
    
    Topic = "Day End Summery " & Format(Date, "dd MMMM yyyy")
    myworkbook.SaveAs (App.Path & "\" & Topic & ".xls")
    myworkbook.Save
    myworkbook.Close
    
    ShellExecute 0&, "open", App.Path & "\" & Topic & ".xls", "", "", vbMaximizedFocus
End Sub

Private Function GetColumnName(ColumnNo As Long) As String
    Dim TemNum As Integer
    Dim temnum1 As Integer
    
    If ColumnNo < 27 Then
        GetColumnName = Chr(ColumnNo + 64)
    Else
        TemNum = ColumnNo \ 26
        temnum1 = ColumnNo Mod 26
        GetColumnName = Chr(TemNum + 64) & Chr(temnum1 + 64)
    End If
End Function



Public Function FillDetailGrid(InputSql As String, InputGrid As MSFlexGrid, TotalNameCol As Integer, TotalCols() As Integer, omitRepeatCols() As Integer, Optional AddBlankLine As Boolean, Optional ColToAddBlankLineWhenNew As Integer, Optional AddLineInsteadOfBlank As Boolean, Optional SubtotalColForBlankLine As Integer) As Double()
    Dim rsTem As New ADODB.Recordset
    Dim colTotal() As Double
    Dim previousValue() As String
    Dim previousValue2 As String
    Dim AddBlankThisTime As Boolean
    
    Dim i As Integer
    Dim Col As Integer
    Dim noRepeat As Boolean
    
    With rsTem
        If .State = 1 Then .Close
        temSQL = InputSql
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        
        InputGrid.Clear
        
        InputGrid.Rows = 1
        InputGrid.Cols = .Fields.Count
        
        ReDim colTotal(.Fields.Count)
        ReDim previousValue(.Fields.Count)
        
        InputGrid.Row = 0
                    
        For i = 0 To .Fields.Count - 1
            InputGrid.Col = i
            InputGrid.text = .Fields(i).Name
        Next i
        
        While .EOF = False
            InputGrid.Rows = InputGrid.Rows + 1
            InputGrid.Row = InputGrid.Rows - 1
                        
            AddBlankThisTime = False
                        
            For i = 0 To .Fields.Count - 1
                InputGrid.Col = i
                
                
                If i = ColToAddBlankLineWhenNew And AddBlankLine = True Then
                    If .AbsolutePosition = 1 Then
                        previousValue2 = .Fields(i).Value
                    End If
                    If previousValue2 <> .Fields(i).Value Then
                        InputGrid.Rows = InputGrid.Rows + 2
                        InputGrid.Row = InputGrid.Rows - 1
                        previousValue2 = .Fields(i).Value
                    End If
                End If
                
                If UBound(omitRepeatCols) > 0 Then
                    noRepeat = True
                    For Col = 0 To UBound(omitRepeatCols) - 1
                        If omitRepeatCols(Col) = i Then
                            noRepeat = False
                        End If
                    Next
                    If noRepeat = False Then
                        For Col = 0 To UBound(omitRepeatCols) - 1
                            If omitRepeatCols(Col) = i Then
                                If previousValue(i) <> .Fields(i).Value Then
                                    previousValue(i) = .Fields(i).Value
                                    If IsNull(.Fields(i).Value) = False Then
                                        InputGrid.text = .Fields(i).Value
                                    End If
                                
                                Else
                                    
                                End If
                                
                            Else
                            
                            End If
                        Next
                    Else
                        InputGrid.text = .Fields(i).Value
                    End If
                Else
                    If IsNull(.Fields(i).Value) = False Then
                        InputGrid.text = .Fields(i).Value
                    End If
                End If
                
                For Col = 0 To UBound(TotalCols) - 1
                    If TotalCols(Col) = i Then
                        If IsNull(.Fields(i).Value) = False Then
                            colTotal(i) = colTotal(i) + Val(.Fields(i).Value)
                        End If
                    End If
                Next
            
            Next i
            .MoveNext
        Wend
        .Close
    End With
    
    If UBound(TotalCols) > 0 Then
        InputGrid.Rows = InputGrid.Rows + 2
        InputGrid.Row = InputGrid.Rows - 1
        InputGrid.Col = TotalNameCol
        InputGrid.text = "Total"
        For i = 0 To InputGrid.Cols - 1
            InputGrid.Col = i
            For Col = 0 To UBound(TotalCols) - 1
                If TotalCols(Col) = i Then
                    InputGrid.text = colTotal(i)
                End If
            Next
        Next i
    End If
    
    Dim temCol As Integer
    Dim temRow As Integer
    Dim temColTextLength() As Integer
    Dim SubTotal As Double
    Dim AllColsOfTheRowIsBlank As Boolean
    Dim temBlankColCount As Integer
    
    ReDim temColTextLength(InputGrid.Cols - 1)
    
    
    If AddLineInsteadOfBlank = True Then
        
        With InputGrid
            
            For temRow = 1 To .Rows - 1
                
                AllColsOfTheRowIsBlank = True
                
                For temCol = 0 To .Cols - 1
                    If Trim(.TextMatrix(temRow, temCol)) <> "" And temCol <> SubtotalColForBlankLine Then
                        AllColsOfTheRowIsBlank = False
                        
                    End If
                    If temCol = SubtotalColForBlankLine Then
                            SubTotal = SubTotal + Val(.TextMatrix(temRow, temCol))
                    End If
                Next temCol
                
                If AllColsOfTheRowIsBlank = True Then
                    temBlankColCount = temBlankColCount + 1
                End If
                
                
                If temBlankColCount = 2 Then
                    For temCol = 0 To .Cols - 1
                        .TextMatrix(temRow, temCol) = RepeatString("-", temColTextLength(temCol))
                    Next temCol
                    temBlankColCount = 0
                End If
                
                If temBlankColCount = 1 Then
                    For temCol = 0 To .Cols - 1
                        If temCol = SubtotalColForBlankLine Then
                            .TextMatrix(temRow, temCol) = SubTotal
                            SubTotal = 0
                        End If
                    Next temCol
                End If
                
                If AllColsOfTheRowIsBlank = False Then
                    For temCol = 0 To .Cols - 1
                        temColTextLength(temCol) = Len(.TextMatrix(temRow, temCol))
                    Next temCol
                End If
                
            Next temRow
        End With
    
    End If
    
    Dim temDbl As Double
    FillDetailGrid = colTotal

End Function
    
    
Public Sub replaceGridString(Grid As MSFlexGrid, column As Integer, inputStr As String, outputStr As String)
    On Error Resume Next
    With Grid
        Dim i As Integer
        For i = 1 To .Rows - 1
            If .TextMatrix(i, column) = inputStr Then .TextMatrix(i, column) = outputStr
        Next
    End With
End Sub

Public Sub joinGridColString(Grid As MSFlexGrid, col1 As Integer, col2 As Integer)
    On Error Resume Next
    With Grid
        Dim i As Integer
        For i = 1 To .Rows - 1
            .TextMatrix(i, col1) = .TextMatrix(i, col1) & .TextMatrix(i, col2)
            .TextMatrix(i, col2) = ""
        Next
    End With
End Sub

Public Sub joinGridColDouble(Grid As MSFlexGrid, col1 As Integer, col2 As Integer)
    On Error Resume Next
    With Grid
        Dim i As Integer
        For i = 1 To .Rows - 1
            .TextMatrix(i, col1) = Val(.TextMatrix(i, col1)) + Val(.TextMatrix(i, col2))
            .TextMatrix(i, col2) = ""
        Next
    End With
End Sub

Public Sub replaceBhtId(Grid As MSFlexGrid, column As Integer)
    On Error Resume Next
    Dim temBht As New BHT
    
    With Grid
        Dim i As Integer
        For i = 1 To .Rows - 1
            temBht.BHTID = Val(.TextMatrix(i, column))
            .TextMatrix(i, column) = temBht.BHT
        Next
    End With
End Sub

Public Sub concatLastCols(Grid As MSFlexGrid, numberOfCols As Integer)
'    On Error Resume Next
'    Dim temBht As New BHT
'    Dim temStr As String
'    With Grid
'        Dim i As Integer
'        Dim c As Integer
'        For i = 1 To .Rows - 1
'            temSQL = ""
'            For c = numberOfCols To .Cols - 1
'                temStr = temStr & .TextMatrix(i, c)
'            Next c
'            .TextMatrix(i, column) = temBht.BHT
'        Next i
'    End With
End Sub

Public Sub formatGridString(Grid As MSFlexGrid, column As Integer, formatStr As String)
    On Error Resume Next
    With Grid
        Dim i As Integer
        For i = 1 To .Rows - 1
            'If IsNumeric(.TextMatrix(i, column)) = True Then
                .TextMatrix(i, column) = Format(.TextMatrix(i, column), formatStr)
           ' End If
        Next
    End With
End Sub

Public Sub colourGridByRow(Grid As MSFlexGrid, headerColour, oddColour, evenColour)
    With Grid
        
        Dim i As Integer
        Dim n As Integer
        For i = 0 To .Rows - 1
            
            For n = 0 To .Cols - 1
                .Col = n
                .Row = i
                If i = 0 Then
                    .CellBackColor = headerColour
                ElseIf (i Mod 2) = 1 Then
                    .CellBackColor = oddColour
                Else
                    .CellBackColor = evenColour
                End If
            Next
        Next
        
    End With
End Sub
