Attribute VB_Name = "modPharmacy"
Option Explicit

    Public lastDailyStockDate As Date


Public Function userCanLogToStore(checkUserId As Long, checkStoreId As Long) As Boolean
    Dim temUs As New ADODB.Recordset
    Dim temSQL As String
    With temUs
        temSQL = "SELECT dbo.tblStore.Store, dbo.tblUserStore.UserStoreId, dbo.tblUserStore.UserId " & _
                    "FROM dbo.tblStore RIGHT OUTER JOIN dbo.tblUserStore ON dbo.tblStore.StoreID = dbo.tblUserStore.StoreId " & _
                    "WHERE (dbo.tblUserStore.deleted = 0) AND (dbo.tblUserStore.UserId = " & checkUserId & ") AND (dbo.tblUserStore.StoreId = " & checkStoreId & ") "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            userCanLogToStore = True
        Else
            userCanLogToStore = False
        End If
    End With
End Function

Public Function getDeptSaleBillId(SaleStoreId As Long, CategoryId As Long) As String
    Dim rsTem As New ADODB.Recordset
    Dim temSQL As String
    With rsTem
        temSQL = "Select count (SaleBillID) as billCount from tblSaleBill where Date >= '" & DateSerial(Year(Date), 1, 1) & "'  and SaleCategoryID = " & CategoryId & " and StoreID = " & SaleStoreId
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Dim myStore As New clsStore
        myStore.storeId = SaleStoreId
        Dim mySc As New SaleCategory
        mySc.SaleCategoryID = CategoryId
        
        If .RecordCount > 0 Then
            If IsNull(!billCount) = False Then
                getDeptSaleBillId = myStore.Code & "/" & Left(mySc.SaleCategory, 4) & "/" & (!billCount + 1)
            End If
        End If
        .Close
    End With
End Function


Public Function getDeptPurchaseBillId(PurchaseStoreId As Long) As String
    Dim rsTem As New ADODB.Recordset
    Dim temSQL As String
    With rsTem
        temSQL = "Select count (RefillBillID) as billCount from tblRefillBill where  Date >= '" & DateSerial(Year(Date), 1, 1) & "'  and Purchase = 1 and StoreID = " & PurchaseStoreId
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Dim myStore As New clsStore
        myStore.storeId = PurchaseStoreId
        
        If .RecordCount > 0 Then
            If IsNull(!billCount) = False Then
                getDeptPurchaseBillId = myStore.Code & "/" & (!billCount + 1)
            End If
        End If
        .Close
    End With
End Function


Public Function getDeptTransferBillId(FromStoreId As Long, ToStoreId As Long) As String

    Dim myRb As New RefillBill
    

    Dim rsTem As New ADODB.Recordset
    Dim temSQL As String
    With rsTem
        temSQL = "Select count (RefillBillID) as billCount from tblRefillBill where Purchase = 1 and StoreID = " & FromStoreId
        temSQL = "SELECT COUNT(dbo.tblRefillBill.RefillBillID) AS billCount " & _
                    "FROM dbo.tblSaleBill LEFT OUTER JOIN " & _
                        "dbo.tblRefillBill ON dbo.tblSaleBill.SaleBillID = dbo.tblRefillBill.OrderBillID " & _
                        "Where  Date >= '" & DateSerial(Year(Date), 1, 1) & "'  and (dbo.tblSaleBill.StoreId = " & FromStoreId & ") And (dbo.tblRefillBill.StoreId = " & ToStoreId & ")"
        
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Dim myStore As New clsStore
        Dim toStore As New clsStore
        
        toStore.storeId = ToStoreId
        myStore.storeId = FromStoreId
        
        If .RecordCount > 0 Then
            If IsNull(!billCount) = False Then
                getDeptTransferBillId = myStore.Code & "/" & toStore.Code & "/" & (!billCount + 1)
            End If
        End If
        .Close
    End With
End Function


Public Sub recordDailyStock()
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    If Date = lastDailyStockDate Then Exit Sub
    Dim rsSv As New ADODB.Recordset
    With rsSv
        If .State = 1 Then .Close
        temSQL = "select * from tblStockValueHx where valueDate = '" & Format(Date - 1, "dd MMMM yyyy") & "'"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            lastDailyStockDate = Date
        Else
            temSQL = "select * from tblStore"
            If .State = 1 Then .Close
            .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
            While .EOF = False
                recordStoreDailyStock !storeId
                .MoveNext
            Wend
            .Close
            lastDailyStockDate = Date
        End If
    End With
End Sub

Public Sub recordStoreDailyStock(storeId As Long)
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim rsHx As New ADODB.Recordset
    temSQL = "select * from tblStockValueHx where StoreId = " & storeId & " and valueDate = '" & Format(Date - 1, "dd MMMM yyyy") & "'"
    With rsHx
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !StockValue = findStockValue(storeId, 0)
            !StockPurchaseValue = findStockPurchaseValue(storeId, 0)
        Else
            .AddNew
            !storeId = storeId
            !valueDate = Date - 1
            !StockValue = findStockValue(storeId, 0)
            !StockPurchaseValue = findStockPurchaseValue(storeId, 0)
        End If
        .Update
        .Close
    End With
End Sub

Public Sub fillSaleCats(combo As DataCombo)
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim rsCat As New ADODB.Recordset
    With rsCat
        If .State = 1 Then .Close
        temSQL = "Select * From tblSaleCategory Order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set combo.RowSource = rsCat
        combo.BoundColumn = "SaleCategoryID"
        combo.ListField = "SaleCategory"
    End With
End Sub

Public Sub fillBhts(combo As DataCombo)
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim rsCat As New ADODB.Recordset
    With rsCat
        If .State = 1 Then .Close
        temSQL = "Select * From tblBht Order by Bht"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        Set combo.RowSource = rsCat
        combo.BoundColumn = "BhtId"
        combo.ListField = "Bht"
    End With
End Sub

Public Sub FillItems(combo As DataCombo)
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim rsDept As New ADODB.Recordset
    With rsDept
        If .State = 1 Then .Close
        .Open "Select tblItem.* From tblItem Order By Display", cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Set combo.RowSource = rsDept
        combo.ListField = "Display"
        combo.BoundColumn = "ItemID"
    End With
End Sub

Public Sub fillDepts(combo As DataCombo)
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim rsDept As New ADODB.Recordset
 With rsDept
        If .State = 1 Then .Close
        .Open "Select tblStore.* From tblStore Order By Store", cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Set combo.RowSource = rsDept
        combo.ListField = "Store"
        combo.BoundColumn = "StoreID"
    End With
End Sub

Public Function findSalePrice(ByVal itemId As Long, Optional BatchID As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        findSalePrice = 0
        If .State = 1 Then .Close
        
        
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & itemId & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentSalePrice.SPrice FROM tblCurrentSalePrice WHERE tblCurrentSalePrice.ItemID=" & itemId & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        
        
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findSalePrice = !sprice
        End If
        If findSalePrice = 0 And BatchID <> 0 Then
            findSalePrice = findSalePrice(itemId)
        End If
        End With
End Function

Public Function findPurchasePrice(ByVal itemId As Long, Optional BatchID As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        findPurchasePrice = 0
        If .State = 1 Then .Close
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & itemId & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentPurchasePrice.PPrice FROM tblCurrentPurchasePrice WHERE tblCurrentPurchasePrice.ItemID=" & itemId & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findPurchasePrice = !pprice
        End If
        If findPurchasePrice = 0 And BatchID <> 0 Then
            findPurchasePrice = findPurchasePrice(itemId)
        End If
    End With
End Function

Public Function findWholeSalePrice(ByVal itemId As Long, Optional BatchID As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        findWholeSalePrice = 0
        If .State = 1 Then .Close
        If BatchID = 0 Then
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & itemId & " Order By SetDate Desc, SetTime DESC"
        Else
            temSQL = "SELECT tblCurrentWholeSalePrice.WPrice FROM tblCurrentWholeSalePrice WHERE tblCurrentWholeSalePrice.ItemID=" & itemId & " And BatchId = " & BatchID & "Order By SetDate Desc, SetTime DESC"
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            findWholeSalePrice = !wprice
        End If
        If findWholeSalePrice = 0 And BatchID <> 0 Then
            findWholeSalePrice = findWholeSalePrice(itemId)
        End If
        End With
End Function

Public Function findAllSaleCategories() As Collection
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim cats As New Collection
    Dim temCat As SaleCategory
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblSaleCategory order by SaleCategory"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set temCat = New SaleCategory
            temCat.SaleCategoryID = !SaleCategoryID
            cats.Add temCat
            .MoveNext
        Wend
        .Close
    End With
    Set findAllSaleCategories = cats
End Function

Public Function findAllStores() As Collection
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    Dim cats As New Collection
    Dim temCat As clsStore
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblStore order by Store"
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        While .EOF = False
            Set temCat = New clsStore
            temCat.storeId = !storeId
            cats.Add temCat
            .MoveNext
        Wend
        .Close
    End With
    Set findAllStores = cats
End Function




















Public Function findNetIssue(fromDate As Date, toDate As Date, fromDeptId As Long, toDeptId As Long) As Double
    findNetIssue = findGrossIssue(fromDate, toDate, fromDeptId, toDeptId) - findIssueCancellAndReturn(fromDate, toDate, fromDeptId, toDeptId)
End Function

Public Function findGrossIssue(fromDate As Date, toDate As Date, fromDeptId As Long, toDeptId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT sum(tblSaleBill.NetPrice) as sumNetTotal " & _
                    "FROM tblSaleBill " & _
                    "WHERE  "
        If fromDeptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & fromDeptId & " AND "
        End If
        If toDeptId = 0 Then
            temSQL = temSQL & " tblSaleBill.BilledUnitId <> 0 AND "
        Else
            temSQL = temSQL & " tblSaleBill.BilledUnitId = " & toDeptId & " AND "
        End If
        temSQL = temSQL & "  tblSaleBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumNetTotal) = False Then
            findGrossIssue = !sumNetTotal
        Else
            findGrossIssue = 0
        End If
    End With
End Function

Public Function findIssueCancellAndReturn(fromDate As Date, toDate As Date, fromDeptId As Long, toDeptId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
         temSQL = "SELECT sum(tblReturnBill.NetPrice) AS sumReturn " & _
                    "FROM dbo.tblReturnBill LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblReturnBill.SaleBillID = dbo.tblSaleBill.SaleBillID "
        
        
       
        If fromDeptId <> 0 Then
            temSQL = temSQL & " where tblSaleBill.StoreId = " & fromDeptId & " AND "
        Else
            temSQL = temSQL & " where "
        End If
        If toDeptId = 0 Then
            temSQL = temSQL & " tblSaleBill.BilledUnitId <> 0 AND "
        Else
            temSQL = temSQL & " tblSaleBill.BilledUnitId = " & toDeptId & " AND "
        End If
        
        temSQL = temSQL & " ( tblReturnBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' ) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumReturn) = False Then
            findIssueCancellAndReturn = !sumReturn
        Else
            findIssueCancellAndReturn = 0
        End If
    End With
End Function



































Public Function findNetSale(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    findNetSale = findGrossSale(fromDate, toDate, CatId, deptId) - findCancellAndReturn(fromDate, toDate, CatId, deptId)
End Function

Public Function findGrossSale(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT sum(tblSaleBill.NetPrice) as sumNetTotal " & _
                    "FROM tblSaleBill " & _
                    "WHERE  "
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & "  tblSaleBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumNetTotal) = False Then
            findGrossSale = !sumNetTotal
        Else
            findGrossSale = 0
        End If
    End With
End Function

Public Function findCancellAndReturn(fromDate As Date, toDate As Date, CatId As Long, deptId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
         temSQL = "SELECT sum(tblReturnBill.NetPrice) AS sumReturn " & _
                    "FROM dbo.tblReturnBill LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblReturnBill.SaleBillID = dbo.tblSaleBill.SaleBillID "
        If CatId = 0 Then
            temSQL = temSQL & "WHERE tblsalebill.salecategoryid = " & CatId & " AND "
        Else
            temSQL = temSQL & "WHERE "
        End If
        If CatId <> 0 Then
            temSQL = temSQL & " tblSaleBill.salecategoryid = " & CatId & " AND "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " tblSaleBill.StoreId = " & deptId & " AND "
        End If
        
        temSQL = temSQL & " ( tblReturnBill.Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' ) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!sumReturn) = False Then
            findCancellAndReturn = !sumReturn
        Else
            findCancellAndReturn = 0
        End If
    End With
End Function

Public Function findNetPurchase(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    findNetPurchase = findGrossPurchase(fromDate, toDate, deptId, Purchase, unitReceive) - findPurchaseReturnCancellation(fromDate, toDate, deptId, Purchase, unitReceive)
End Function

Public Function findGrossPurchase(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(NetPrice) AS SumOfPurchases " & _
                    "FROM dbo.tblRefillBill " & _
                    "WHERE  "
        If Purchase = True And unitReceive = True Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =1) AND "
        ElseIf Purchase = False And unitReceive = True Then
            temSQL = temSQL & " (purchase = 0 or ReceivedIssue =1) AND "
        ElseIf Purchase = True And unitReceive = False Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =0) AND "
        Else
            temSQL = temSQL & " "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & " Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!SumOfPurchases) = False Then
            findGrossPurchase = !SumOfPurchases
        Else
            findGrossPurchase = 0
        End If
    End With
End Function

Public Function findPurchaseReturnCancellation(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(ReturnedValue) AS SumOfPurchases " & _
                    "FROM dbo.tblRefillBill " & _
                    "WHERE  "
        If Purchase = True And unitReceive = True Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =1) AND "
        ElseIf Purchase = False And unitReceive = True Then
            temSQL = temSQL & " (purchase = 0 or ReceivedIssue =1) AND "
        ElseIf Purchase = True And unitReceive = False Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =0) AND "
        Else
            temSQL = temSQL & " "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & " ((ReturnedDate between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "') or (CancelledDate between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "')) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!SumOfPurchases) = False Then
            findPurchaseReturnCancellation = !SumOfPurchases
        Else
            findPurchaseReturnCancellation = 0
        End If
    End With
End Function















Public Function findNetPurchaseS(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    findNetPurchaseS = findGrossPurchaseS(fromDate, toDate, deptId, Purchase, unitReceive) - findPurchaseReturnCancellationS(fromDate, toDate, deptId, Purchase, unitReceive)
End Function

Public Function findGrossPurchaseS(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(NetSaleValue) AS SumOfPurchases " & _
                    "FROM dbo.tblRefillBill " & _
                    "WHERE  "
        If Purchase = True And unitReceive = True Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =1) AND "
        ElseIf Purchase = False And unitReceive = True Then
            temSQL = temSQL & " (purchase = 0 or ReceivedIssue =1) AND "
        ElseIf Purchase = True And unitReceive = False Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =0) AND "
        Else
            temSQL = temSQL & " "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & " Date between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "' "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!SumOfPurchases) = False Then
            findGrossPurchaseS = !SumOfPurchases
        Else
            findGrossPurchaseS = 0
        End If
    End With
End Function

Public Function findPurchaseReturnCancellationS(fromDate As Date, toDate As Date, deptId As Long, Purchase As Boolean, unitReceive As Boolean) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(ReturnedSaleValue) AS SumOfPurchases " & _
                    "FROM dbo.tblRefillBill " & _
                    "WHERE  "
        If Purchase = True And unitReceive = True Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =1) AND "
        ElseIf Purchase = False And unitReceive = True Then
            temSQL = temSQL & " (purchase = 0 or ReceivedIssue =1) AND "
        ElseIf Purchase = True And unitReceive = False Then
            temSQL = temSQL & " (purchase = 1 or ReceivedIssue =0) AND "
        Else
            temSQL = temSQL & " "
        End If
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & " AND "
        End If
        temSQL = temSQL & " ((ReturnedDate between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "') or (CancelledDate between '" & Format(fromDate, "dd MMMM yyyy") & "' And '" & Format(toDate, "dd MMMM yyyy") & "')) "
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!SumOfPurchases) = False Then
            findPurchaseReturnCancellationS = !SumOfPurchases
        Else
            findPurchaseReturnCancellationS = 0
        End If
         
    End With
End Function















Public Function findStockValue(deptId As Long, itemId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(dbo.tblBatchStock.sprice * dbo.tblBatchStock.Stock)AS valueOfStock FROM dbo.tblBatch RIGHT OUTER JOIN dbo.tblBatchStock ON dbo.tblBatch.BatchID = dbo.tblBatchStock.BatchID " & _
                    "WHERE  "
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & "  "
        End If
        If itemId <> 0 And deptId <> 0 Then
            temSQL = temSQL & " and "
        End If
        If itemId <> 0 Then
            temSQL = temSQL & " ItemId = " & itemId & " "
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!valueOfStock) = False Then
            findStockValue = !valueOfStock
        Else
            findStockValue = 0
        End If
    End With
End Function


Public Function findStockPurchaseValue(deptId As Long, itemId As Long) As Double
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT SUM(dbo.tblBatchStock.pprice * dbo.tblBatchStock.Stock)AS valueOfStock FROM dbo.tblBatch RIGHT OUTER JOIN dbo.tblBatchStock ON dbo.tblBatch.BatchID = dbo.tblBatchStock.BatchID " & _
                    "WHERE  "
        If deptId <> 0 Then
            temSQL = temSQL & " StoreId = " & deptId & "  "
        End If
        If itemId <> 0 And deptId <> 0 Then
            temSQL = temSQL & " and "
        End If
        If itemId <> 0 Then
            temSQL = temSQL & " ItemId = " & itemId & " "
        End If
        .Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
        If IsNull(!valueOfStock) = False Then
            findStockPurchaseValue = !valueOfStock
        Else
            findStockPurchaseValue = 0
        End If
    End With
End Function

Public Sub addBatchPrice()
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    On Error Resume Next
    Dim temBatch As New Batch
    Dim temBs As BatchStock
    Dim rsTem1 As New ADODB.Recordset
    
    With rsTem1
        If .State = 1 Then .Close
        temSQL = "SELECT tblBatchStock.* FROM tblBatchStock"
        .Open temSQL, cnnStores, adOpenStatic, adLockOptimistic
        .MoveLast
        .MoveFirst
        Dim i As Integer
        
        For i = 0 To .RecordCount - 1
            Set temBs = New BatchStock
            temBs.BatchStockID = !BatchStockID
            temBatch.BatchID = !BatchID
            temBs.sprice = findSalePrice(temBatch.itemId, temBatch.BatchID)
            temBs.pprice = findPurchasePrice(temBatch.itemId, temBatch.BatchID)
            temBs.wprice = findWholeSalePrice(temBatch.itemId, temBatch.BatchID)
            temBs.saveData
            .MoveNext
        Next
        .Close
'        While .EOF = False
'            temBs.BatchStockID = !BatchStockID
'            temBatch.BatchID = !BatchID
'            temBs.sprice = findSalePrice(temBatch.ItemID, temBatch.BatchID)
'            temBs.pprice = findPurchasePrice(temBatch.ItemID, temBatch.BatchID)
'            temBs.wprice = findWholeSalePrice(temBatch.ItemID, temBatch.BatchID)
'            .MoveNext
'        Wend
'        .Close
    End With
End Sub



