Attribute VB_Name = "modPrinterSettings"
Option Explicit
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private CsetPrinter As New cSetDfltPrinter

    Dim myForm As Form

Private Sub PopulatePrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        myForm.cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub

Public Sub getPrinterSettings(ByRef pForm As Form)
    On Error Resume Next
    Set myForm = pForm
    PopulatePrinters
    Dim temPrinter As String
    temPrinter = GetSetting(App.EXEName, myForm.Name, "printer", "")
    If Trim(temPrinter) = "" Then
        myForm.cmbPrinter.text = getDefaultPrinterName
    Else
        myForm.cmbPrinter.text = temPrinter
    End If
    PopulatePapers
    Dim temPaper As String
    temPaper = GetSetting(App.EXEName, myForm.Name, "paper", "")
    If Trim(temPaper) = "" Then temPaper = "A4"
    myForm.cmbPaper.text = temPaper
    savePrinterSettings
End Sub

Public Sub setPrinterSettings(ByRef pForm As Form)
    On Error Resume Next
    Set myForm = pForm
    savePrinterSettings
End Sub

Public Function getDefaultPrinterName() As String
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        If Printer.DeviceName = MyPrinter.DeviceName Then
            getDefaultPrinterName = Printer.DeviceName
        End If
    Next
End Function

Private Sub PopulatePapers()
    SetPrinter = False
    If Trim(myForm.cmbPrinter.text) = "" Then
        CsetPrinter.SetPrinterAsDefault (myForm.cmbPrinter.text)
        PrinterName = myForm.cmbPrinter.text
    Else
        PrinterName = getDefaultPrinterName
        CsetPrinter.SetPrinterAsDefault (PrinterName)
    End If
    
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
'        With FormSize
'            .cx = BillPaperHeight
'            .cy = BillPaperWidth
'        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                'FormItem = PtrCtoVbString(.pName) & " - " & .Size.cx / 1000 & " mm X " & .Size.cy / 1000 & " mm   (" & i + 1 & ")"
                'ComboBillPrinterPapers.AddItem FormItem
                myForm.cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

Private Sub savePrinterSettings()
    SaveSetting App.EXEName, myForm.Name, "printer", myForm.cmbPrinter.text
    SaveSetting App.EXEName, myForm.Name, "paper", myForm.cmbPaper.text
End Sub

