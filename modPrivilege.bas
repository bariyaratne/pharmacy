Attribute VB_Name = "modPrivilege"
Option Explicit

Public Enum Privilege
    Purchase = 1
    stockAdjust = 2
    priceAdjust = 3
    Edit = 4
    BackOffice = 5
    Staff = 6
    
End Enum

Public Function userIsPrivileged(p As Privilege) As Boolean
    Dim sql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        sql = "select * from tblUserPrivilege where userid = " & userid & " and privilege = " & p
        .Open sql, cnnStores, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            userIsPrivileged = True
        Else
            userIsPrivileged = False
        End If
        .Close
    End With
End Function

