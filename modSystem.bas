Attribute VB_Name = "modSystem"
Option Explicit
    Dim CsetPrinter As New cSetDfltPrinter
    Dim PrinterHandle As Long
    
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    
    Dim temSQL As String

    Dim LeftMargin As Double
    Dim TopMargin As Double
    Dim myY As Double
    Dim myLabelX As Double
    Dim myValX As Double
    Dim myNuX As Double
    Dim myColan As Double
    Dim centre As Double
    
    Dim CenterX As Long
    Dim FieldX As Long
    Dim NoX As Long
    Dim ValueX As Long
    Dim AllLines() As String
    
    Dim printLower As String
    Dim printUpper As String
    Dim longLine As String
    Dim longLine1 As String
    
    Dim itemTab As Double
    Dim qtyTab As Double
    Dim priceTab As Double
    
    
    Dim footerStart As Double
    Dim paperCut As Double

    Dim temY As Long
    Dim n As Long
    
    Public Type MyBillPoints
        DX As Double
        DY As Double
        VX As Double
        VY As Double
        cx As Double
        cy As Double
        CenterX As Double
    End Type
    
Private Sub setMargins()
        LeftMargin = 0
        TopMargin = 1440 * 0.7
        centre = Printer.Width / 2
        myLabelX = 1440 * 0.6
        myValX = 1440 * 2.01
        myNuX = Printer.Width - 1440 * 0.7
        myColan = 1440 * 1.7
        footerStart = Printer.Height - 1440 * 1.2
        paperCut = Printer.Height - 1440 * 0.65
    
    

    
    itemTab = 0.5 * 1440
    qtyTab = 3.1 * 1440
    priceTab = 4.2 * 1440
    
        printUpper = "_____________"
        printLower = "============="
        longLine = " -------------------------------------------------------------------------------------"
        longLine1 = "____________________________________________________________________________________"
End Sub



Public Sub printSaleBillA5(billId As Long, formHwnd)
    Dim PrinterName As String
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    If SelectForm(BillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim temBillPoints As MyBillPoints
    Dim MyFOnt As ReportFont
    
    
    Dim pBill As New SaleBill
    Dim pBillItem As SaleItem
    Dim pBht As New BHT
    Dim pItem As clsItem
    Dim pDept As New clsStore
    Dim pPt As New Patient
    Dim pbStaff As New clsStaff
    Dim pSaleCat As New SaleCategory
    
    pBill.SaleBillID = billId
    
    pDept.StoreID = pBill.StoreID
    
    pBht.BHTID = pBill.BilledBHTID
    
    pSaleCat.SaleCategoryID = pBill.SaleCategoryID
    
    If pBht.BHTID <> 0 Then
        pPt.PatientID = pBht.PatientID
    Else
        pPt.PatientID = pBill.BilledOutPatientID
    End If
    
    pbStaff.StaffID = pBill.BilledStaffID
    
    
    
    
    
    Call setMargins
     
     
    ' ******************** printHeading
    
        Printer.FontBold = True
        Printer.Font = "Arial"
        Printer.FontSize = "5"
       ' Printer.Print
        
        Printer.FontSize = "14"
        Printer.CurrentY = TopMargin
        Printer.CurrentX = centre - (Printer.TextWidth(pDept.Store) / 2)
        Printer.Print pDept.Store
        
        Printer.FontSize = "5"
        Printer.Print
        
        Printer.Font = "Arial"
        Printer.FontSize = "10"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Name."
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        
        If pPt.FirstName <> "" And pBht.BHT = "" Then
            Printer.Print pPt.FirstName
        ElseIf pBht.BHT <> "" Then
            Printer.Print pPt.FirstName & "(BHT No. " & pBht.BHT & ")"
        ElseIf pbStaff.Name <> "" Then
            Printer.Print pbStaff.Name & "(" & pbStaff.Code & ")"
        End If
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Serial"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print pBill.SaleBillID
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Bill No"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print pBill.DisplayBillId
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Sale"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print pSaleCat.SaleCategory
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Date / Time"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print Format(pBill.BillDate, "dd MMM yyyy") & " - " & Format(pBill.Time, "hh:mm AMPM")


    Printer.FontSize = "5"
    Printer.Print
'    Printer.Print

    Printer.FontSize = "10"
    Printer.CurrentX = itemTab
    Printer.Print longLine
    
    myY = Printer.CurrentY
    
    Printer.CurrentX = itemTab
    Printer.CurrentY = myY
    Printer.Print "Item"
    
    Printer.CurrentX = qtyTab - Printer.TextWidth("Quantity")
    Printer.CurrentY = myY
    Printer.Print "Quantity"

    Printer.CurrentX = priceTab - Printer.TextWidth("Value")
    Printer.CurrentY = myY
    Printer.Print Format("Value")
    
    Printer.CurrentX = itemTab
    Printer.Print longLine
    
    ' ******************* End Print Headings
    
    Dim strTem  As String
    Dim rsTem1 As New ADODB.Recordset
    Dim rsTem2 As New ADODB.Recordset
    Dim dblNo1 As Double
    
    strTem = ""
    dblNo1 = 0
    
    If rsTem1.State = 1 Then rsTem1.Close
    temSQL = "SELECT * FROM tblSale where SaleBillID = " & pBill.SaleBillID
    rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    

    
    While rsTem1.EOF = False
        Set pItem = New clsItem
        Set pBillItem = New SaleItem
        
        pBillItem.SaleId = rsTem1!SaleId
        pItem.ItemID = pBillItem.ItemID
        
        myY = Printer.CurrentY
        
        Printer.CurrentX = itemTab
        Printer.CurrentY = myY
        Printer.Print pItem.Display
        
        Printer.CurrentX = qtyTab - Printer.TextWidth(pBillItem.Amount)
        Printer.CurrentY = myY
        Printer.Print pBillItem.Amount

        Printer.CurrentX = priceTab - Printer.TextWidth(Format(pBillItem.GrossPrice, "#,##0.00"))
        Printer.CurrentY = myY
        Printer.Print Format(pBillItem.GrossPrice, "#,##0.00")
        
        rsTem1.MoveNext
    Wend
    rsTem1.Close
        
    Printer.Print
    Printer.Print
    
    
    myY = paperCut - 1440 * 0.75

    Printer.FontSize = "10"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printUpper
    
    Printer.FontSize = "12"
    Printer.FontBold = False

    myY = Printer.CurrentY
    Printer.CurrentX = myLabelX
    Printer.CurrentY = myY
    Printer.Print "Total"
    
    Printer.CurrentX = myNuX - Printer.TextWidth(Format(pBill.Price, "#,##0.00"))
    Printer.CurrentY = myY
    Printer.Print Format(pBill.Price, "#,##0.00")
    
    myY = Printer.CurrentY
    Printer.CurrentX = myLabelX
    Printer.CurrentY = myY
    Printer.Print "Discount"
    
    Printer.CurrentX = myNuX - Printer.TextWidth(Format(pBill.Discount, "#,##0.00"))
    Printer.CurrentY = myY
    Printer.Print Format(pBill.Discount, "#,##0.00")
    
    myY = Printer.CurrentY
    Printer.CurrentX = myLabelX
    Printer.CurrentY = myY
    Printer.Print "Net Total"

    
    Printer.CurrentX = myNuX - Printer.TextWidth(Format(pBill.NetPrice, "#,##0.00"))
    Printer.CurrentY = myY
    Printer.Print Format(pBill.NetPrice, "#,##0.00")

    myY = Printer.CurrentY
    Printer.FontSize = "10"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printLower

    pbStaff.StaffID = pBill.StaffID

    myY = Printer.CurrentY
    Printer.FontSize = "8"
    
    Printer.CurrentY = myY
    Printer.CurrentX = myNuX - Printer.TextWidth(pbStaff.Name)
    Printer.Print "User : " & DecreptedWord(pbStaff.UserName)
    
    Printer.EndDoc
End Sub

Public Sub printBhtItems(BHTID As Long, formHwnd, fromDate As Date, toDate As Date, PrinterName As String, paperName As String)
    CsetPrinter.SetPrinterAsDefault (PrinterName)
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (PrinterName)
    If SelectForm(paperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim temBillPoints As MyBillPoints
    Dim MyFOnt As ReportFont
    
    
    Dim pBill As New SaleBill
    Dim pBillItem As SaleItem
    Dim pBht As New BHT
    Dim pItem As clsItem
    Dim pDept As New clsStore
    Dim pPt As New Patient
    Dim pbStaff As New clsStaff
    
    Dim Total As Double
    
    pBht.BHTID = BHTID
    
   
    pPt.PatientID = pBht.PatientID
   
    
   
    
    
    
    Call setMargins
     
     
    ' ******************** printHeading
    
        Printer.FontBold = True
        Printer.Font = "Arial"
        Printer.FontSize = "5"
       ' Printer.Print
        
        Printer.FontSize = "14"
        Printer.CurrentY = TopMargin
        Printer.CurrentX = centre - (Printer.TextWidth("BHT Issues") / 2)
        Printer.Print "BHT Issues"
        
        Printer.FontSize = "5"
        Printer.Print
        
        Printer.Font = "Arial"
        Printer.FontSize = "10"
        Printer.FontBold = False
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "Name."
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print pPt.FirstName
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "BHT No"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print pBht.BHT
        
        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "From"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print Format(fromDate, "dd MMM yyyy")

        myY = Printer.CurrentY
        Printer.CurrentX = myLabelX
        Printer.CurrentY = myY
        Printer.Print "To"
        Printer.CurrentX = myColan
        Printer.CurrentY = myY
        Printer.Print ":"
        Printer.CurrentX = myValX
        Printer.CurrentY = myY
        Printer.Print Format(toDate, "dd MMM yyyy")


    Printer.FontSize = "5"
    Printer.Print
'    Printer.Print

    Printer.FontSize = "10"
    Printer.CurrentX = itemTab
    Printer.Print longLine
    
    myY = Printer.CurrentY
    
    Printer.CurrentX = itemTab
    Printer.CurrentY = myY
    Printer.Print "Item"
    
    Printer.CurrentX = qtyTab - Printer.TextWidth("Quantity")
    Printer.CurrentY = myY
    Printer.Print "Quantity"

    Printer.CurrentX = priceTab - Printer.TextWidth("Value")
    Printer.CurrentY = myY
    Printer.Print Format("Value")
    
    Printer.CurrentX = itemTab
    Printer.Print longLine
    
    ' ******************* End Print Headings
    
    Dim strTem  As String
    Dim rsTem1 As New ADODB.Recordset
    Dim rsTem2 As New ADODB.Recordset
    Dim dblNo1 As Double
    
    strTem = ""
    dblNo1 = 0
    
    If rsTem1.State = 1 Then rsTem1.Close
    temSQL = "SELECT dbo.tblItem.Display, dbo.tblSale.Price, dbo.tblSale.Amount " & _
    "FROM  dbo.tblItem RIGHT OUTER JOIN dbo.tblSale LEFT OUTER JOIN dbo.tblSaleBill ON dbo.tblSale.SaleBillID = dbo.tblSaleBill.SaleBillID LEFT OUTER JOIN  dbo.tblStaff Cashier ON dbo.tblSaleBill.StaffID = Cashier.StaffID ON dbo.tblItem.ItemID = dbo.tblSale.ItemID " & _
    "Where dbo.tblSaleBill.BilledBHTID = " & BHTID & " AND (dbo.tblSaleBill.Date BETWEEN CONVERT(DATETIME, '" & Format(fromDate, "dd MMMM yyyy") & "', 102) AND CONVERT(DATETIME, '" & Format(toDate, "dd MMMM yyyy") & "', 102)) "
    rsTem1.Open temSQL, cnnStores, adOpenStatic, adLockReadOnly
    

    
    While rsTem1.EOF = False
       
        
        myY = Printer.CurrentY
        
        Printer.CurrentX = itemTab
        Printer.CurrentY = myY
        Printer.Print rsTem1!Display
        
        Printer.CurrentX = qtyTab - Printer.TextWidth(rsTem1!Amount)
        Printer.CurrentY = myY
        Printer.Print rsTem1!Amount

        Printer.CurrentX = priceTab - Printer.TextWidth(Format(rsTem1!Price, "#,##0.00"))
        Printer.CurrentY = myY
        Printer.Print Format(rsTem1!Price, "#,##0.00")
        
        Total = Total + rsTem1!Price
        
        rsTem1.MoveNext
    Wend
    rsTem1.Close
        
    Printer.Print
    Printer.Print
    
    
    myY = paperCut - 1440 * 0.75

    Printer.FontSize = "10"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printUpper
    
    Printer.FontSize = "10"
    Printer.FontBold = False

    myY = Printer.CurrentY
    Printer.CurrentX = myLabelX
    Printer.CurrentY = myY
    Printer.Print "Total"
    
    Printer.CurrentX = myNuX - Printer.TextWidth(Format(Total, "#,##0.00"))
    Printer.CurrentY = myY
    Printer.Print Format(Total, "#,##0.00")
    
    myY = Printer.CurrentY
    Printer.FontSize = "10"
    Printer.FontBold = False

    Printer.CurrentX = myNuX - Printer.TextWidth(printUpper)
    Printer.CurrentY = myY
    Printer.Print printLower
    Printer.EndDoc
End Sub


Public Sub printSaleBillA4(billId As Long, formHwnd)
    Dim PrinterName As String
    Unload drSaleBillAsReport
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
        .sqlSaleBill CDbl(billId)
    End With
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    If SelectForm(ReportPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    DoEvents
    Dim A As Integer
    For A = 1 To 1000
        DoEvents
    Next
    drSaleBillAsReport.Refresh
    drSaleBillAsReport.PrintReport
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
    End With
End Sub


Public Sub printSaleBillA5Old(billId As Long, formHwnd)
    
    Dim PrinterName As String
    
    
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    If SelectForm(BillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim A As Integer
    
    For A = 1 To 1000
        DoEvents
    Next
    
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
        .sqlSaleBill CDbl(billId)
    End With
    
    For A = 1 To 1000
        DoEvents
    Next
    drSaleBillAsBill.Refresh
    'drSaleBillAsBill.PrintReport
    
    
    Printer.Print "aaaa"
    Printer.Print "bbbbb"
    Printer.EndDoc
    
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
    End With
End Sub


Public Sub previewSaleBillA4(billId As Long, formHwnd)
    Dim PrinterName As String
    Unload drSaleBillAsReport
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
        .sqlSaleBill CDbl(billId)
    End With
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (ReportPrinterName)
    If SelectForm(ReportPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    DoEvents
    Dim A As Integer
    For A = 1 To 1000
        DoEvents
    Next
    drSaleBillAsReport.Refresh
    drSaleBillAsReport.Show
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
    End With
End Sub

Public Sub previewSaleBillA5(billId As Long, formHwnd)
    Dim PrinterName As String
    Unload drSaleBillAsBill
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
        .sqlSaleBill CDbl(billId)
    End With
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    CsetPrinter.SetPrinterAsDefault (BillPrinterName)
    If SelectForm(BillPaperName, formHwnd) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    DoEvents
    Dim A As Integer
    For A = 1 To 1000
        DoEvents
    Next
    drSaleBillAsBill.Refresh
    drSaleBillAsBill.Show
    With Dataenvironment1
        If .rssqlSaleBill.State = 1 Then .rssqlSaleBill.Close
    End With
End Sub


